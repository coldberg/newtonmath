#ifndef __NEWTON_MATH_VECTOR_HPP__ 
#define __NEWTON_MATH_VECTOR_HPP__ 

#include <cstddef>
#include <cstdint>
#include "detail/tvec2.hpp"
#include "detail/tvec3.hpp"
#include "detail/tvec4.hpp"

namespace Newton {
	namespace Math {
	
	
		template <typename T> using tvec2 = tvec<2, T>;
		template <typename T> using tvec3 = tvec<3, T>;
		template <typename T> using tvec4 = tvec<4, T>;
		template <std::size_t N> using u8vec = tvec<N, std::uint8_t>;
		template <std::size_t N> using u16vec = tvec<N, std::uint16_t>;
		template <std::size_t N> using u32vec = tvec<N, std::uint32_t>;
		template <std::size_t N> using u64vec = tvec<N, std::uint64_t>;
		template <std::size_t N> using i8vec = tvec<N, std::int8_t>;
		template <std::size_t N> using i16vec = tvec<N, std::int16_t>;
		template <std::size_t N> using i32vec = tvec<N, std::int32_t>;
		template <std::size_t N> using i64vec = tvec<N, std::int64_t>;
		template <std::size_t N> using f32vec = tvec<N, float>;
		template <std::size_t N> using f64vec = tvec<N, double>;
		template <std::size_t N> using llvec = tvec<N, long long>;
		template <std::size_t N> using lvec = tvec<N, long>;
		template <std::size_t N> using lluvec = tvec<N, unsigned long long>;
		template <std::size_t N> using uvec = tvec<N, unsigned>;
		template <std::size_t N> using ivec = tvec<N, int>;
		template <std::size_t N> using bvec = tvec<N, bool>;
		template <std::size_t N> using dvec = tvec<N, double>;
		template <std::size_t N> using fvec = tvec<N, float>;
		template <std::size_t N> using vec = tvec<N, float>;

		typedef tvec<2, std::uint8_t> u8vec2;
		typedef tvec<2, std::uint16_t> u16vec2;
		typedef tvec<2, std::uint32_t> u32vec2;
		typedef tvec<2, std::uint64_t> u64vec2;
		typedef tvec<2, std::int8_t> i8vec2;
		typedef tvec<2, std::int16_t> i16vec2;
		typedef tvec<2, std::int32_t> i32vec2;
		typedef tvec<2, std::int64_t> i64vec2;
		typedef tvec<2, float> f32vec2;
		typedef tvec<2, double> f64vec2;
		typedef tvec<2, long long> llvec2;
		typedef tvec<2, long> lvec2;
		typedef tvec<2, unsigned long long> lluvec2;
		typedef tvec<2, unsigned> uvec2;
		typedef tvec<2, int> ivec2;
		typedef tvec<2, bool> bvec2;
		typedef tvec<2, double> dvec2;
		typedef tvec<2, float> fvec2;
		typedef tvec<2, float> vec2;
		typedef tvec<3, std::uint8_t> u8vec3;
		typedef tvec<3, std::uint16_t> u16vec3;
		typedef tvec<3, std::uint32_t> u32vec3;
		typedef tvec<3, std::uint64_t> u64vec3;
		typedef tvec<3, std::int8_t> i8vec3;
		typedef tvec<3, std::int16_t> i16vec3;
		typedef tvec<3, std::int32_t> i32vec3;
		typedef tvec<3, std::int64_t> i64vec3;
		typedef tvec<3, float> f32vec3;
		typedef tvec<3, double> f64vec3;
		typedef tvec<3, long long> llvec3;
		typedef tvec<3, long> lvec3;
		typedef tvec<3, unsigned long long> lluvec3;
		typedef tvec<3, unsigned> uvec3;
		typedef tvec<3, int> ivec3;
		typedef tvec<3, bool> bvec3;
		typedef tvec<3, double> dvec3;
		typedef tvec<3, float> fvec3;
		typedef tvec<3, float> vec3;
		typedef tvec<4, std::uint8_t> u8vec4;
		typedef tvec<4, std::uint16_t> u16vec4;
		typedef tvec<4, std::uint32_t> u32vec4;
		typedef tvec<4, std::uint64_t> u64vec4;
		typedef tvec<4, std::int8_t> i8vec4;
		typedef tvec<4, std::int16_t> i16vec4;
		typedef tvec<4, std::int32_t> i32vec4;
		typedef tvec<4, std::int64_t> i64vec4;
		typedef tvec<4, float> f32vec4;
		typedef tvec<4, double> f64vec4;
		typedef tvec<4, long long> llvec4;
		typedef tvec<4, long> lvec4;
		typedef tvec<4, unsigned long long> lluvec4;
		typedef tvec<4, unsigned> uvec4;
		typedef tvec<4, int> ivec4;
		typedef tvec<4, bool> bvec4;
		typedef tvec<4, double> dvec4;
		typedef tvec<4, float> fvec4;
		typedef tvec<4, float> vec4;

	}
}

#endif // __NEWTON_MATH_VECTOR_HPP__