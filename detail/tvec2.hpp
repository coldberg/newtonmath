#ifndef __NEWTON_MATH_DETAIL_TVEC2_HPP__ 
#define __NEWTON_MATH_DETAIL_TVEC2_HPP__ 

#include <cstddef>
#include <cstdint>

#include "basic_tvec.hpp"

#include <istream>
#include <ostream>
#include <cmath>
#include <type_traits>

#pragma pack(push, 1)
namespace Newton {
	namespace Math {
		namespace detail {
			template<typename T>
			struct tvec_landing<2, T> {				
				tvec_landing (T& x, T& y) : x (x), y (y) {}
				
				tvec_landing<2, T>& operator = (const tvec<2, T>& in)  {
					x = in.x;
					y = in.y;
					return *this;
				}
				
			private:
				T& x;
				T& y;
			};
		}
				
		template <typename T>
		struct tvec<2, T> {
			typedef T value_type;
			static const std::size_t size = 2;			

			union {				
				struct { value_type x, y; };
				struct { value_type r, g; };
				struct { value_type s, t; };
				struct { value_type u, v; };

			#if !_NEWTON_MATH_DISABLE_SWIZZLE			
				detail::tvec_swizzle<T, 0, 0> xx;
				detail::tvec_swizzle<T, 0, 1> xy;
				detail::tvec_swizzle<T, 1, 0> yx;
				detail::tvec_swizzle<T, 1, 1> yy;
				detail::tvec_swizzle<T, 0, 0, 0> xxx;
				detail::tvec_swizzle<T, 0, 0, 1> xxy;
				detail::tvec_swizzle<T, 0, 1, 0> xyx;
				detail::tvec_swizzle<T, 0, 1, 1> xyy;
				detail::tvec_swizzle<T, 1, 0, 0> yxx;
				detail::tvec_swizzle<T, 1, 0, 1> yxy;
				detail::tvec_swizzle<T, 1, 1, 0> yyx;
				detail::tvec_swizzle<T, 1, 1, 1> yyy;
				detail::tvec_swizzle<T, 0, 0, 0, 0> xxxx;
				detail::tvec_swizzle<T, 0, 0, 0, 1> xxxy;
				detail::tvec_swizzle<T, 0, 0, 1, 0> xxyx;
				detail::tvec_swizzle<T, 0, 0, 1, 1> xxyy;
				detail::tvec_swizzle<T, 0, 1, 0, 0> xyxx;
				detail::tvec_swizzle<T, 0, 1, 0, 1> xyxy;
				detail::tvec_swizzle<T, 0, 1, 1, 0> xyyx;
				detail::tvec_swizzle<T, 0, 1, 1, 1> xyyy;
				detail::tvec_swizzle<T, 1, 0, 0, 0> yxxx;
				detail::tvec_swizzle<T, 1, 0, 0, 1> yxxy;
				detail::tvec_swizzle<T, 1, 0, 1, 0> yxyx;
				detail::tvec_swizzle<T, 1, 0, 1, 1> yxyy;
				detail::tvec_swizzle<T, 1, 1, 0, 0> yyxx;
				detail::tvec_swizzle<T, 1, 1, 0, 1> yyxy;
				detail::tvec_swizzle<T, 1, 1, 1, 0> yyyx;
				detail::tvec_swizzle<T, 1, 1, 1, 1> yyyy;
				detail::tvec_swizzle<T, 0, 0> rr;
				detail::tvec_swizzle<T, 0, 1> rg;
				detail::tvec_swizzle<T, 1, 0> gr;
				detail::tvec_swizzle<T, 1, 1> gg;
				detail::tvec_swizzle<T, 0, 0, 0> rrr;
				detail::tvec_swizzle<T, 0, 0, 1> rrg;
				detail::tvec_swizzle<T, 0, 1, 0> rgr;
				detail::tvec_swizzle<T, 0, 1, 1> rgg;
				detail::tvec_swizzle<T, 1, 0, 0> grr;
				detail::tvec_swizzle<T, 1, 0, 1> grg;
				detail::tvec_swizzle<T, 1, 1, 0> ggr;
				detail::tvec_swizzle<T, 1, 1, 1> ggg;
				detail::tvec_swizzle<T, 0, 0, 0, 0> rrrr;
				detail::tvec_swizzle<T, 0, 0, 0, 1> rrrg;
				detail::tvec_swizzle<T, 0, 0, 1, 0> rrgr;
				detail::tvec_swizzle<T, 0, 0, 1, 1> rrgg;
				detail::tvec_swizzle<T, 0, 1, 0, 0> rgrr;
				detail::tvec_swizzle<T, 0, 1, 0, 1> rgrg;
				detail::tvec_swizzle<T, 0, 1, 1, 0> rggr;
				detail::tvec_swizzle<T, 0, 1, 1, 1> rggg;
				detail::tvec_swizzle<T, 1, 0, 0, 0> grrr;
				detail::tvec_swizzle<T, 1, 0, 0, 1> grrg;
				detail::tvec_swizzle<T, 1, 0, 1, 0> grgr;
				detail::tvec_swizzle<T, 1, 0, 1, 1> grgg;
				detail::tvec_swizzle<T, 1, 1, 0, 0> ggrr;
				detail::tvec_swizzle<T, 1, 1, 0, 1> ggrg;
				detail::tvec_swizzle<T, 1, 1, 1, 0> gggr;
				detail::tvec_swizzle<T, 1, 1, 1, 1> gggg;
				detail::tvec_swizzle<T, 0, 0> ss;
				detail::tvec_swizzle<T, 0, 1> st;
				detail::tvec_swizzle<T, 1, 0> ts;
				detail::tvec_swizzle<T, 1, 1> tt;
				detail::tvec_swizzle<T, 0, 0, 0> sss;
				detail::tvec_swizzle<T, 0, 0, 1> sst;
				detail::tvec_swizzle<T, 0, 1, 0> sts;
				detail::tvec_swizzle<T, 0, 1, 1> stt;
				detail::tvec_swizzle<T, 1, 0, 0> tss;
				detail::tvec_swizzle<T, 1, 0, 1> tst;
				detail::tvec_swizzle<T, 1, 1, 0> tts;
				detail::tvec_swizzle<T, 1, 1, 1> ttt;
				detail::tvec_swizzle<T, 0, 0, 0, 0> ssss;
				detail::tvec_swizzle<T, 0, 0, 0, 1> ssst;
				detail::tvec_swizzle<T, 0, 0, 1, 0> ssts;
				detail::tvec_swizzle<T, 0, 0, 1, 1> sstt;
				detail::tvec_swizzle<T, 0, 1, 0, 0> stss;
				detail::tvec_swizzle<T, 0, 1, 0, 1> stst;
				detail::tvec_swizzle<T, 0, 1, 1, 0> stts;
				detail::tvec_swizzle<T, 0, 1, 1, 1> sttt;
				detail::tvec_swizzle<T, 1, 0, 0, 0> tsss;
				detail::tvec_swizzle<T, 1, 0, 0, 1> tsst;
				detail::tvec_swizzle<T, 1, 0, 1, 0> tsts;
				detail::tvec_swizzle<T, 1, 0, 1, 1> tstt;
				detail::tvec_swizzle<T, 1, 1, 0, 0> ttss;
				detail::tvec_swizzle<T, 1, 1, 0, 1> ttst;
				detail::tvec_swizzle<T, 1, 1, 1, 0> ttts;
				detail::tvec_swizzle<T, 1, 1, 1, 1> tttt;
				detail::tvec_swizzle<T, 0, 0> uu;
				detail::tvec_swizzle<T, 0, 1> uv;
				detail::tvec_swizzle<T, 1, 0> vu;
				detail::tvec_swizzle<T, 1, 1> vv;
				detail::tvec_swizzle<T, 0, 0, 0> uuu;
				detail::tvec_swizzle<T, 0, 0, 1> uuv;
				detail::tvec_swizzle<T, 0, 1, 0> uvu;
				detail::tvec_swizzle<T, 0, 1, 1> uvv;
				detail::tvec_swizzle<T, 1, 0, 0> vuu;
				detail::tvec_swizzle<T, 1, 0, 1> vuv;
				detail::tvec_swizzle<T, 1, 1, 0> vvu;
				detail::tvec_swizzle<T, 1, 1, 1> vvv;
				detail::tvec_swizzle<T, 0, 0, 0, 0> uuuu;
				detail::tvec_swizzle<T, 0, 0, 0, 1> uuuv;
				detail::tvec_swizzle<T, 0, 0, 1, 0> uuvu;
				detail::tvec_swizzle<T, 0, 0, 1, 1> uuvv;
				detail::tvec_swizzle<T, 0, 1, 0, 0> uvuu;
				detail::tvec_swizzle<T, 0, 1, 0, 1> uvuv;
				detail::tvec_swizzle<T, 0, 1, 1, 0> uvvu;
				detail::tvec_swizzle<T, 0, 1, 1, 1> uvvv;
				detail::tvec_swizzle<T, 1, 0, 0, 0> vuuu;
				detail::tvec_swizzle<T, 1, 0, 0, 1> vuuv;
				detail::tvec_swizzle<T, 1, 0, 1, 0> vuvu;
				detail::tvec_swizzle<T, 1, 0, 1, 1> vuvv;
				detail::tvec_swizzle<T, 1, 1, 0, 0> vvuu;
				detail::tvec_swizzle<T, 1, 1, 0, 1> vvuv;
				detail::tvec_swizzle<T, 1, 1, 1, 0> vvvu;
				detail::tvec_swizzle<T, 1, 1, 1, 1> vvvv;
			#endif
			};
			const T& operator [] (std::size_t i)const {
			#if _NEWTON_MATH_RANGE_CHECK			
				if (i >= size) throw std::out_of_range ("Vector subscript out of range.");
			#endif
				return reinterpret_cast<const T (&) [size]> (*this) [i];
			}
			 T& operator [] (std::size_t i) {
			#if _NEWTON_MATH_RANGE_CHECK			
				if (i >= size) throw std::out_of_range ("Vector subscript out of range.");
			#endif
				return reinterpret_cast< T (&) [size]> (*this) [i];
			}
			
			constexpr tvec (T v0, T v1) : 
				x (v0), y (v1)				 
			{}
			
			constexpr tvec (T in) : 
				tvec (in, in) 
			{}
			
			constexpr tvec ()  : tvec (T ()) {}
			
			//template <typename U, std::size_t... I>
			//tvec (const detail::tvec_swizzle<U, I...>& a) : tvec (a ()) {}
			
			template <typename I, std::enable_if_t<!std::is_same<T, I>::value, int> = 0> 
			explicit tvec (const tvec<2, I>& in) : 
				tvec (in.x, in.y) 
			{}
			
			template <typename I> 
			explicit tvec (const tvec<3, I>& in) : 
				tvec (in.x, in.y) 
			{}
			
			template <typename I> 
			explicit tvec (const tvec<4, I>& in) : 
				tvec (in.x, in.y) 
			{}
			
			tvec (const tvec<2, T>& v0) : tvec (v0.x, v0.y) {}
			tvec<2, T>& operator = (const tvec<2, T>& in) {
				(*this).x = in.x;
				(*this).y = in.y;
				return *this;
			};
		};
////////////////////////////////
// I/O Operators
////////////////////////////////
		template <typename T>
		std::istream& operator >> (std::istream& iss, tvec<2, T>& in) {			
			return iss >> in.x >> in.y;
		}		
		template <typename T>
		std::ostream& operator << (std::ostream& oss, const tvec<2, T>& in) {
			return oss << "{ " << "x: " << in.x << ", "<< "y: " << in.y << ", " << " }";
		}

////////////////////////////////
// Binary operations
////////////////////////////////
		template <typename A, typename B>
		inline auto operator + (const tvec<2, A>& a, const tvec<2, B>& b) {
			return tvec_make (a.x + b.x, a.y + b.y);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator + (const tvec<2, A>& a, const B& b) {
			return a + tvec<2, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator + (const A& a, const tvec<2, B>& b) {
			return tvec<2, A> (a) + b;
		}
		
		template <typename A, typename B>
		inline auto operator - (const tvec<2, A>& a, const tvec<2, B>& b) {
			return tvec_make (a.x - b.x, a.y - b.y);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator - (const tvec<2, A>& a, const B& b) {
			return a - tvec<2, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator - (const A& a, const tvec<2, B>& b) {
			return tvec<2, A> (a) - b;
		}
		
		template <typename A, typename B>
		inline auto operator * (const tvec<2, A>& a, const tvec<2, B>& b) {
			return tvec_make (a.x * b.x, a.y * b.y);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator * (const tvec<2, A>& a, const B& b) {
			return a * tvec<2, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator * (const A& a, const tvec<2, B>& b) {
			return tvec<2, A> (a) * b;
		}
		
		template <typename A, typename B>
		inline auto operator / (const tvec<2, A>& a, const tvec<2, B>& b) {
			return tvec_make (a.x / b.x, a.y / b.y);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator / (const tvec<2, A>& a, const B& b) {
			return a / tvec<2, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator / (const A& a, const tvec<2, B>& b) {
			return tvec<2, A> (a) / b;
		}
		
		template <typename A, typename B>
		inline auto operator % (const tvec<2, A>& a, const tvec<2, B>& b) {
			return tvec_make (modulus (a.x, b.x), modulus (a.y, b.y));			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator % (const tvec<2, A>& a, const B& b) {
			return a % tvec<2, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator % (const A& a, const tvec<2, B>& b) {
			return tvec<2, A> (a) % b;
		}
		
		template <typename A, typename B>
		inline auto operator & (const tvec<2, A>& a, const tvec<2, B>& b) {
			return tvec_make (a.x & b.x, a.y & b.y);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator & (const tvec<2, A>& a, const B& b) {
			return a & tvec<2, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator & (const A& a, const tvec<2, B>& b) {
			return tvec<2, A> (a) & b;
		}
		
		template <typename A, typename B>
		inline auto operator | (const tvec<2, A>& a, const tvec<2, B>& b) {
			return tvec_make (a.x | b.x, a.y | b.y);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator | (const tvec<2, A>& a, const B& b) {
			return a | tvec<2, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator | (const A& a, const tvec<2, B>& b) {
			return tvec<2, A> (a) | b;
		}
		
		template <typename A, typename B>
		inline auto operator ^ (const tvec<2, A>& a, const tvec<2, B>& b) {
			return tvec_make (a.x ^ b.x, a.y ^ b.y);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator ^ (const tvec<2, A>& a, const B& b) {
			return a ^ tvec<2, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator ^ (const A& a, const tvec<2, B>& b) {
			return tvec<2, A> (a) ^ b;
		}
		
		template <typename A, typename B>
		inline auto operator && (const tvec<2, A>& a, const tvec<2, B>& b) {
			return tvec_make (a.x && b.x, a.y && b.y);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator && (const tvec<2, A>& a, const B& b) {
			return a && tvec<2, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator && (const A& a, const tvec<2, B>& b) {
			return tvec<2, A> (a) && b;
		}
		
		template <typename A, typename B>
		inline auto operator || (const tvec<2, A>& a, const tvec<2, B>& b) {
			return tvec_make (a.x || b.x, a.y || b.y);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator || (const tvec<2, A>& a, const B& b) {
			return a || tvec<2, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator || (const A& a, const tvec<2, B>& b) {
			return tvec<2, A> (a) || b;
		}
		
////////////////////////////////
// Boolean operations
////////////////////////////////
		template <typename A, typename B>
		inline auto operator == (const tvec<2, A>& a, const tvec<2, B>& b) {
			return (a.x == b.x) && (a.y == b.y);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator == (const tvec<2, A>& a, const B& b) {
			return a == tvec<2, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator == (const A& a, const tvec<2, B>& b) {
			return tvec<2, A> (a) == b;
		}		
		
		template <typename A, typename B>
		inline auto operator != (const tvec<2, A>& a, const tvec<2, B>& b) {
			return (a.x != b.x) || (a.y != b.y);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator != (const tvec<2, A>& a, const B& b) {
			return a != tvec<2, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator != (const A& a, const tvec<2, B>& b) {
			return tvec<2, A> (a) != b;
		}		
		
		


////////////////////////////////
// Unary operations
////////////////////////////////
		template <typename T>
		inline auto operator + (const tvec<2, T>& a) {
			return tvec_make (+a.x, +a.y);			
		}
		
		template <typename T>
		inline auto operator - (const tvec<2, T>& a) {
			return tvec_make (-a.x, -a.y);			
		}
		
		template <typename T>
		inline auto operator ~ (const tvec<2, T>& a) {
			return tvec_make (~a.x, ~a.y);			
		}
		
		template <typename T>
		inline auto operator ! (const tvec<2, T>& a) {
			return tvec_make (!a.x, !a.y);			
		}
		
////////////////////////////////
// Misc operations
////////////////////////////////
		template <typename A, typename B>
		inline auto dot (const tvec<2, A>& a, const tvec<2, B>& b)  {			
			return a.x*b.x + a.y*b.y;
		}
		
		template <typename T>
		inline auto hypot2 (const tvec<2, T>& a)  {
			return dot (a, a);
		}
		
		template <typename T>
		inline auto hypot (const tvec<2, T>& a)  {
			return std::sqrt (hypot2 (a));
		}

		template <typename T>
		inline auto length2 (const tvec<2, T>& a)  {
			return hypot2 (a);
		}
		
		template <typename T>
		inline auto length (const tvec<2, T>& a)  {
			return hypot (a);
		}
		
		template <typename T>
		inline auto normalize (const tvec<2, T>& a)  {
			return a / hypot (a);
		}	
		
		template <typename T>
		inline auto distance (const tvec<2, T>& a, const tvec<2, T>& b)  {
			return hypot (b - a);
		}

		template <typename T>
		inline auto distance2 (const tvec<2, T>& a, const tvec<2, T>& b)  {
			return hypot2 (b - a);
		}
		

		template <typename T>
		auto abs (const tvec<2, T>& x)  {
			return tvec_make (std::abs (x.x), std::abs (x.y));
		}		
		
		template <typename T>
		auto min (const tvec<2, T>& x, const tvec<2, T>& y)  {
			return tvec_make (std::min (x.x, y.x), std::min (x.y, y.y));
		}		
		
		template <typename T>
		auto max (const tvec<2, T>& x, const tvec<2, T>& y)  {
			return tvec_make (std::max (x.x, y.x), std::max (x.y, y.y));
		}		
		
		template <typename T>
		auto sin (const tvec<2, T>& x)  {
			return tvec_make (std::sin (x.x), std::sin (x.y));
		}		
		
		template <typename T>
		auto cos (const tvec<2, T>& x)  {
			return tvec_make (std::cos (x.x), std::cos (x.y));
		}		
		
		template <typename T>
		auto tan (const tvec<2, T>& x)  {
			return tvec_make (std::tan (x.x), std::tan (x.y));
		}		
		
		template <typename T>
		auto sinh (const tvec<2, T>& x)  {
			return tvec_make (std::sinh (x.x), std::sinh (x.y));
		}		
		
		template <typename T>
		auto cosh (const tvec<2, T>& x)  {
			return tvec_make (std::cosh (x.x), std::cosh (x.y));
		}		
		
		template <typename T>
		auto tanh (const tvec<2, T>& x)  {
			return tvec_make (std::tanh (x.x), std::tanh (x.y));
		}		
		
		template <typename T>
		auto asin (const tvec<2, T>& x)  {
			return tvec_make (std::asin (x.x), std::asin (x.y));
		}		
		
		template <typename T>
		auto acos (const tvec<2, T>& x)  {
			return tvec_make (std::acos (x.x), std::acos (x.y));
		}		
		
		template <typename T>
		auto atan (const tvec<2, T>& x)  {
			return tvec_make (std::atan (x.x), std::atan (x.y));
		}		
		
		template <typename T>
		auto atan2 (const tvec<2, T>& x, const tvec<2, T>& y)  {
			return tvec_make (std::atan2 (x.x, y.x), std::atan2 (x.y, y.y));
		}		
		
		template <typename T>
		auto log (const tvec<2, T>& x)  {
			return tvec_make (std::log (x.x), std::log (x.y));
		}		
		
		template <typename T>
		auto log2 (const tvec<2, T>& x)  {
			return tvec_make (std::log2 (x.x), std::log2 (x.y));
		}		
		
		template <typename T>
		auto log10 (const tvec<2, T>& x)  {
			return tvec_make (std::log10 (x.x), std::log10 (x.y));
		}		
		
		template <typename T>
		auto log1p (const tvec<2, T>& x)  {
			return tvec_make (std::log1p (x.x), std::log1p (x.y));
		}		
		
		template <typename T>
		auto exp (const tvec<2, T>& x)  {
			return tvec_make (std::exp (x.x), std::exp (x.y));
		}		
		
		template <typename T>
		auto exp2 (const tvec<2, T>& x)  {
			return tvec_make (std::exp2 (x.x), std::exp2 (x.y));
		}		
		
		template <typename T>
		auto exp1m (const tvec<2, T>& x)  {
			return tvec_make (std::exp1m (x.x), std::exp1m (x.y));
		}		
		
		template <typename T>
		auto pow (const tvec<2, T>& x, const tvec<2, T>& n)  {
			return tvec_make (std::pow (x.x, n.x), std::pow (x.y, n.y));
		}		
		
		template <typename T>
		auto sqrt (const tvec<2, T>& x)  {
			return tvec_make (std::sqrt (x.x), std::sqrt (x.y));
		}		
		
		template <typename T>
		auto cbrt (const tvec<2, T>& x)  {
			return tvec_make (std::cbrt (x.x), std::cbrt (x.y));
		}		
		
		template <typename T>
		auto floor (const tvec<2, T>& x)  {
			return tvec_make (std::floor (x.x), std::floor (x.y));
		}		
		
		template <typename T>
		auto ceil (const tvec<2, T>& x)  {
			return tvec_make (std::ceil (x.x), std::ceil (x.y));
		}		
		
		template <typename T>
		auto round (const tvec<2, T>& x)  {
			return tvec_make (std::round (x.x), std::round (x.y));
		}		
		
		template <typename T>
		auto trunc (const tvec<2, T>& x)  {
			return tvec_make (std::trunc (x.x), std::trunc (x.y));
		}		
		
		template <typename T>
		auto copysign (const tvec<2, T>& x, const tvec<2, T>& y)  {
			return tvec_make (std::copysign (x.x, y.x), std::copysign (x.y, y.y));
		}		
		
		
	}
}
#pragma pack(pop)
#endif // __NEWTON_MATH_DETAIL_TVEC2_HPP__