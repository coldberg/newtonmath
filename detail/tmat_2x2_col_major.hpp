#ifndef __NEWTON_MATH_DETAIL_TMAT_2X2_COL_MAJOR_HPP__ 
#define __NEWTON_MATH_DETAIL_TMAT_2X2_COL_MAJOR_HPP__ 

#include <cstddef>
#include <cstdint>

#include "basic_tmat.hpp"
#include "tvec2.hpp"
#pragma pack(push, 1)
namespace Newton {
	namespace Math {
		namespace col_major {			
			template <typename T>
			struct tmat<2, 2, T> {
				static const std::size_t cols = 2u;
				static const std::size_t rows = 2u;
				typedef T value_type;
				typedef tvec<2, T> row_type;
				typedef tvec<2, T> col_type;
				
				explicit constexpr tmat (const col_type& v0, const col_type& v1) : m_data {v0, v1} {}
				
				explicit constexpr tmat (value_type v00, value_type v10, value_type v01, value_type v11) : m_data {col_type {v00, v10}, col_type {v01, v11}} {}			
				
				explicit constexpr tmat (value_type w) : 
					m_data {col_type (tvec<2, T> (value_type (w), value_type (0))), col_type (tvec<2, T> (value_type (0), value_type (w)))}
				{}
				
				constexpr tmat () : tmat (value_type (0)) {}
			
				template <std::size_t K, typename I>
				explicit constexpr tmat (const tmat<2, K, I>& in) :
					tmat (col_type (in [0]), col_type (in [1])) {}
			
				template <std::size_t K, typename I>
				explicit constexpr tmat (const tmat<3, K, I>& in) :
					tmat (col_type (in [0]), col_type (in [1])) {}
			
				template <std::size_t K, typename I>
				explicit constexpr tmat (const tmat<4, K, I>& in) :
					tmat (col_type (in [0]), col_type (in [1])) {}
					
				
				const auto& operator [] (std::size_t i) const {
					#if _NEWTON_MATH_RANGE_CHECK					
					if (i >= std::size (m_data)) throw std::out_of_range ("Matrix subscript out of range");				
					#endif
					return m_data [i];
				}
				
				auto& operator [] (std::size_t i) {
					#if _NEWTON_MATH_RANGE_CHECK 				
					if (i >= std::size (m_data)) throw std::out_of_range ("Matrix subscript out of range");				
					#endif
					return m_data [i];
				}
				
				auto& operator () (std::size_t col, std::size_t row) {
					return (*this) [col][row];
	
				}
				const auto& operator () (std::size_t col, std::size_t row) const {
					return (*this) [col][row];
	
				}
				
			private:
				col_type m_data [2];
			};

			template <typename T>
			inline const auto& col (const tmat<2, 2, T>& in, std::size_t i) {
				return in [i];
			}
			
			template <typename T>
			inline auto row (const tmat<2, 2, T>& in, std::size_t i) {
				return tvec<2, T> (in [0][i], in [1][i]);
			}
			
			template <typename T>
			inline auto transpose (const tmat<2, 2, T>& in) {
				return tmat_make (row (in, 0), row (in, 1));
			} 
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator * (const tmat<2, 2, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] * rhs, lhs [1] * rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator * (const _Lhs& lhs, const tmat<2, 2, _Rhs>& rhs) {
				return tmat_make (lhs * rhs [0], lhs * rhs [1]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator / (const tmat<2, 2, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] / rhs, lhs [1] / rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator / (const _Lhs& lhs, const tmat<2, 2, _Rhs>& rhs) {
				return tmat_make (lhs / rhs [0], lhs / rhs [1]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator + (const tmat<2, 2, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] + rhs, lhs [1] + rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator + (const _Lhs& lhs, const tmat<2, 2, _Rhs>& rhs) {
				return tmat_make (lhs + rhs [0], lhs + rhs [1]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator - (const tmat<2, 2, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] - rhs, lhs [1] - rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator - (const _Lhs& lhs, const tmat<2, 2, _Rhs>& rhs) {
				return tmat_make (lhs - rhs [0], lhs - rhs [1]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator % (const tmat<2, 2, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] % rhs, lhs [1] % rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator % (const _Lhs& lhs, const tmat<2, 2, _Rhs>& rhs) {
				return tmat_make (lhs % rhs [0], lhs % rhs [1]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator & (const tmat<2, 2, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] & rhs, lhs [1] & rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator & (const _Lhs& lhs, const tmat<2, 2, _Rhs>& rhs) {
				return tmat_make (lhs & rhs [0], lhs & rhs [1]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator | (const tmat<2, 2, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] | rhs, lhs [1] | rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator | (const _Lhs& lhs, const tmat<2, 2, _Rhs>& rhs) {
				return tmat_make (lhs | rhs [0], lhs | rhs [1]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator ^ (const tmat<2, 2, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] ^ rhs, lhs [1] ^ rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator ^ (const _Lhs& lhs, const tmat<2, 2, _Rhs>& rhs) {
				return tmat_make (lhs ^ rhs [0], lhs ^ rhs [1]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator || (const tmat<2, 2, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] || rhs, lhs [1] || rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator || (const _Lhs& lhs, const tmat<2, 2, _Rhs>& rhs) {
				return tmat_make (lhs || rhs [0], lhs || rhs [1]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator && (const tmat<2, 2, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] && rhs, lhs [1] && rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator && (const _Lhs& lhs, const tmat<2, 2, _Rhs>& rhs) {
				return tmat_make (lhs && rhs [0], lhs && rhs [1]);
			}
			
			template <typename _Lhs, typename _Rhs>
			inline auto operator + (const tmat<2, 2, _Lhs>& lhs, const tmat<2, 2, _Rhs>& rhs) {
				return tmat_make (lhs [0] + rhs [0], lhs [1] + rhs [1]);				
			}

			template <typename _Lhs, typename _Rhs>
			inline auto operator - (const tmat<2, 2, _Lhs>& lhs, const tmat<2, 2, _Rhs>& rhs) {
				return tmat_make (lhs [0] - rhs [0], lhs [1] - rhs [1]);				
			}

			
			template <typename _Lhs, typename _Rhs>
			inline auto operator == (const tmat<2, 2, _Lhs>& lhs, const tmat<2, 2, _Rhs>& rhs) {
				return lhs [0] == rhs [0] && lhs [1] == rhs [1];				
			}

			template <typename _Lhs, typename _Rhs>
			inline auto operator != (const tmat<2, 2, _Lhs>& lhs, const tmat<2, 2, _Rhs>& rhs) {
				return lhs [0] != rhs [0] || lhs [1] != rhs [1];				
			}

			template <typename T> 
			inline std::ostream& operator << (std::ostream& oss, const tmat<2, 2, T>& in) {
				oss << std::setw(5) << in (0, 0) << ", ";
				oss << std::setw(5) << in (1, 0) << ", ";
				oss << std::endl ;
				oss << std::setw(5) << in (0, 1) << ", ";
				oss << std::setw(5) << in (1, 1) << ", ";
				oss << std::endl ;
				return oss;
			}
			
			template <typename T> 
			inline std::istream& operator >> (std::istream& iss, tmat<2, 2, T>& in) {
				iss >> in (0, 0);
				iss >> in (1, 0);
				iss >> in (0, 1);
				iss >> in (1, 1);
				return iss;
			}	
			
			template <typename _Lhs, typename _Rhs, typename _Ret = std::common_type_t<_Lhs, _Rhs>>
			inline auto operator * (const tmat<2, 2, _Lhs>& lhs, const tmat<2, 2, _Rhs>& rhs) {
				tmat<2, 2, _Ret> result;				
				result (0, 0) = dot (row (lhs, 0), col (rhs, 0));					
				result (1, 0) = dot (row (lhs, 0), col (rhs, 1));					
				result (0, 1) = dot (row (lhs, 1), col (rhs, 0));					
				result (1, 1) = dot (row (lhs, 1), col (rhs, 1));					
				return result;
			}
			
			template <typename _Lhs, typename _Rhs, typename _Ret = std::common_type_t<_Lhs, _Rhs>>
			inline auto operator * (const tmat<2, 2, _Lhs>& lhs, const tvec<2, _Rhs>& rhs) {
				tvec<2, _Ret> result;
				result [0] = dot (row (lhs, 0), rhs);
				result [1] = dot (row (lhs, 1), rhs);
				return result;
			}

			template <typename _Lhs, typename _Rhs, typename _Ret = std::common_type_t<_Lhs, _Rhs>>
			inline auto operator * (const tvec<2, _Lhs>& lhs, const tmat<2, 2, _Rhs>& rhs) {
				tvec<2, _Ret> result;
				result [0] = dot (lhs, col (rhs, 0));
				result [1] = dot (lhs, col (rhs, 1));
				return result;
			}			
			
			

		}
	}
}
#pragma pack(pop)


 #endif // __NEWTON_MATH_DETAIL_TMAT_2X2_COL_MAJOR_HPP__