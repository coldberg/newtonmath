#ifndef __NEWTON_MATH_DETAIL_TMAT_3X3_ROW_MAJOR_HPP__ 
#define __NEWTON_MATH_DETAIL_TMAT_3X3_ROW_MAJOR_HPP__ 

#include <cstddef>
#include <cstdint>

#include "basic_tmat.hpp"
#include "tvec3.hpp"
#pragma pack(push, 1)
namespace Newton {
	namespace Math {
		namespace row_major {			
			template <typename T>
			struct tmat<3, 3, T> {
				static const std::size_t cols = 3u;
				static const std::size_t rows = 3u;
				typedef T value_type;
				typedef tvec<3, T> row_type;
				typedef tvec<3, T> col_type;
				
				explicit constexpr tmat (const row_type& v0, const row_type& v1, const row_type& v2) : m_data {v0, v1, v2} {}
				
				explicit constexpr tmat (value_type v00, value_type v10, value_type v20, value_type v01, value_type v11, value_type v21, value_type v02, value_type v12, value_type v22) : m_data {row_type {v00, v10, v20}, row_type {v01, v11, v21}, row_type {v02, v12, v22}} {}			
				
				explicit constexpr tmat (value_type w) : 
					m_data {row_type (tvec<3, T> (value_type (w), value_type (0), value_type (0))), row_type (tvec<3, T> (value_type (0), value_type (w), value_type (0))), row_type (tvec<3, T> (value_type (0), value_type (0), value_type (w)))}
				{}
				
				constexpr tmat () : tmat (value_type (0)) {}
			
				template <std::size_t K, typename I>
				explicit constexpr tmat (const tmat<K, 2, I>& in) :
					tmat (row_type (in [0]), row_type (in [1]), row_type (value_type (0))) {}
			
				template <std::size_t K, typename I>
				explicit constexpr tmat (const tmat<K, 3, I>& in) :
					tmat (row_type (in [0]), row_type (in [1]), row_type (in [2])) {}
			
				template <std::size_t K, typename I>
				explicit constexpr tmat (const tmat<K, 4, I>& in) :
					tmat (row_type (in [0]), row_type (in [1]), row_type (in [2])) {}
					
				
				const auto& operator [] (std::size_t i) const {
					#if _NEWTON_MATH_RANGE_CHECK					
					if (i >= std::size (m_data)) throw std::out_of_range ("Matrix subscript out of range");				
					#endif
					return m_data [i];
				}
				
				auto& operator [] (std::size_t i) {
					#if _NEWTON_MATH_RANGE_CHECK 				
					if (i >= std::size (m_data)) throw std::out_of_range ("Matrix subscript out of range");				
					#endif
					return m_data [i];
				}
				
				auto& operator () (std::size_t col, std::size_t row) {
					return (*this) [row][col];
	
				}
				const auto& operator () (std::size_t col, std::size_t row) const {
					return (*this) [row][col];
	
				}
				
			private:
				row_type m_data [3];
			};

			template <typename T>
			inline const auto& row (const tmat<3, 3, T>& in, std::size_t i) {
				return in [i];
			}
			
			template <typename T>
			inline auto col (const tmat<3, 3, T>& in, std::size_t i) {
				return tvec<3, T> (in [0][i], in [1][i], in [2][i]);
			}
			
			template <typename T>
			inline auto transpose (const tmat<3, 3, T>& in) {
				return tmat_make (col (in, 0), col (in, 1), col (in, 2));
			} 
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator * (const tmat<3, 3, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] * rhs, lhs [1] * rhs, lhs [2] * rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator * (const _Lhs& lhs, const tmat<3, 3, _Rhs>& rhs) {
				return tmat_make (lhs * rhs [0], lhs * rhs [1], lhs * rhs [2]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator / (const tmat<3, 3, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] / rhs, lhs [1] / rhs, lhs [2] / rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator / (const _Lhs& lhs, const tmat<3, 3, _Rhs>& rhs) {
				return tmat_make (lhs / rhs [0], lhs / rhs [1], lhs / rhs [2]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator + (const tmat<3, 3, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] + rhs, lhs [1] + rhs, lhs [2] + rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator + (const _Lhs& lhs, const tmat<3, 3, _Rhs>& rhs) {
				return tmat_make (lhs + rhs [0], lhs + rhs [1], lhs + rhs [2]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator - (const tmat<3, 3, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] - rhs, lhs [1] - rhs, lhs [2] - rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator - (const _Lhs& lhs, const tmat<3, 3, _Rhs>& rhs) {
				return tmat_make (lhs - rhs [0], lhs - rhs [1], lhs - rhs [2]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator % (const tmat<3, 3, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] % rhs, lhs [1] % rhs, lhs [2] % rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator % (const _Lhs& lhs, const tmat<3, 3, _Rhs>& rhs) {
				return tmat_make (lhs % rhs [0], lhs % rhs [1], lhs % rhs [2]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator & (const tmat<3, 3, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] & rhs, lhs [1] & rhs, lhs [2] & rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator & (const _Lhs& lhs, const tmat<3, 3, _Rhs>& rhs) {
				return tmat_make (lhs & rhs [0], lhs & rhs [1], lhs & rhs [2]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator | (const tmat<3, 3, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] | rhs, lhs [1] | rhs, lhs [2] | rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator | (const _Lhs& lhs, const tmat<3, 3, _Rhs>& rhs) {
				return tmat_make (lhs | rhs [0], lhs | rhs [1], lhs | rhs [2]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator ^ (const tmat<3, 3, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] ^ rhs, lhs [1] ^ rhs, lhs [2] ^ rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator ^ (const _Lhs& lhs, const tmat<3, 3, _Rhs>& rhs) {
				return tmat_make (lhs ^ rhs [0], lhs ^ rhs [1], lhs ^ rhs [2]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator || (const tmat<3, 3, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] || rhs, lhs [1] || rhs, lhs [2] || rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator || (const _Lhs& lhs, const tmat<3, 3, _Rhs>& rhs) {
				return tmat_make (lhs || rhs [0], lhs || rhs [1], lhs || rhs [2]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator && (const tmat<3, 3, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] && rhs, lhs [1] && rhs, lhs [2] && rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator && (const _Lhs& lhs, const tmat<3, 3, _Rhs>& rhs) {
				return tmat_make (lhs && rhs [0], lhs && rhs [1], lhs && rhs [2]);
			}
			
			template <typename _Lhs, typename _Rhs>
			inline auto operator + (const tmat<3, 3, _Lhs>& lhs, const tmat<3, 3, _Rhs>& rhs) {
				return tmat_make (lhs [0] + rhs [0], lhs [1] + rhs [1], lhs [2] + rhs [2]);				
			}

			template <typename _Lhs, typename _Rhs>
			inline auto operator - (const tmat<3, 3, _Lhs>& lhs, const tmat<3, 3, _Rhs>& rhs) {
				return tmat_make (lhs [0] - rhs [0], lhs [1] - rhs [1], lhs [2] - rhs [2]);				
			}

			
			template <typename _Lhs, typename _Rhs>
			inline auto operator == (const tmat<3, 3, _Lhs>& lhs, const tmat<3, 3, _Rhs>& rhs) {
				return lhs [0] == rhs [0] && lhs [1] == rhs [1] && lhs [2] == rhs [2];				
			}

			template <typename _Lhs, typename _Rhs>
			inline auto operator != (const tmat<3, 3, _Lhs>& lhs, const tmat<3, 3, _Rhs>& rhs) {
				return lhs [0] != rhs [0] || lhs [1] != rhs [1] || lhs [2] != rhs [2];				
			}

			template <typename T> 
			inline std::ostream& operator << (std::ostream& oss, const tmat<3, 3, T>& in) {
				oss << std::setw(5) << in (0, 0) << ", ";
				oss << std::setw(5) << in (1, 0) << ", ";
				oss << std::setw(5) << in (2, 0) << ", ";
				oss << std::endl ;
				oss << std::setw(5) << in (0, 1) << ", ";
				oss << std::setw(5) << in (1, 1) << ", ";
				oss << std::setw(5) << in (2, 1) << ", ";
				oss << std::endl ;
				oss << std::setw(5) << in (0, 2) << ", ";
				oss << std::setw(5) << in (1, 2) << ", ";
				oss << std::setw(5) << in (2, 2) << ", ";
				oss << std::endl ;
				return oss;
			}
			
			template <typename T> 
			inline std::istream& operator >> (std::istream& iss, tmat<3, 3, T>& in) {
				iss >> in (0, 0);
				iss >> in (1, 0);
				iss >> in (2, 0);
				iss >> in (0, 1);
				iss >> in (1, 1);
				iss >> in (2, 1);
				iss >> in (0, 2);
				iss >> in (1, 2);
				iss >> in (2, 2);
				return iss;
			}	
			
			template <typename _Lhs, typename _Rhs, typename _Ret = std::common_type_t<_Lhs, _Rhs>>
			inline auto operator * (const tmat<3, 3, _Lhs>& lhs, const tmat<3, 3, _Rhs>& rhs) {
				tmat<3, 3, _Ret> result;				
				result (0, 0) = dot (row (lhs, 0), col (rhs, 0));					
				result (1, 0) = dot (row (lhs, 0), col (rhs, 1));					
				result (2, 0) = dot (row (lhs, 0), col (rhs, 2));					
				result (0, 1) = dot (row (lhs, 1), col (rhs, 0));					
				result (1, 1) = dot (row (lhs, 1), col (rhs, 1));					
				result (2, 1) = dot (row (lhs, 1), col (rhs, 2));					
				result (0, 2) = dot (row (lhs, 2), col (rhs, 0));					
				result (1, 2) = dot (row (lhs, 2), col (rhs, 1));					
				result (2, 2) = dot (row (lhs, 2), col (rhs, 2));					
				return result;
			}
			
			template <typename _Lhs, typename _Rhs, typename _Ret = std::common_type_t<_Lhs, _Rhs>>
			inline auto operator * (const tmat<3, 3, _Lhs>& lhs, const tvec<3, _Rhs>& rhs) {
				tvec<3, _Ret> result;
				result [0] = dot (row (lhs, 0), rhs);
				result [1] = dot (row (lhs, 1), rhs);
				result [2] = dot (row (lhs, 2), rhs);
				return result;
			}

			template <typename _Lhs, typename _Rhs, typename _Ret = std::common_type_t<_Lhs, _Rhs>>
			inline auto operator * (const tvec<3, _Lhs>& lhs, const tmat<3, 3, _Rhs>& rhs) {
				tvec<3, _Ret> result;
				result [0] = dot (lhs, col (rhs, 0));
				result [1] = dot (lhs, col (rhs, 1));
				result [2] = dot (lhs, col (rhs, 2));
				return result;
			}			
			
			

		}
	}
}
#pragma pack(pop)


 #endif // __NEWTON_MATH_DETAIL_TMAT_3X3_ROW_MAJOR_HPP__