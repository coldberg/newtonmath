#ifndef __NEWTON_MATH_DETAIL_TMAT_4X4_ROW_MAJOR_HPP__ 
#define __NEWTON_MATH_DETAIL_TMAT_4X4_ROW_MAJOR_HPP__ 

#include <cstddef>
#include <cstdint>

#include "basic_tmat.hpp"
#include "tvec4.hpp"
#pragma pack(push, 1)
namespace Newton {
	namespace Math {
		namespace row_major {			
			template <typename T>
			struct tmat<4, 4, T> {
				static const std::size_t cols = 4u;
				static const std::size_t rows = 4u;
				typedef T value_type;
				typedef tvec<4, T> row_type;
				typedef tvec<4, T> col_type;
				
				explicit constexpr tmat (const row_type& v0, const row_type& v1, const row_type& v2, const row_type& v3) : m_data {v0, v1, v2, v3} {}
				
				explicit constexpr tmat (value_type v00, value_type v10, value_type v20, value_type v30, value_type v01, value_type v11, value_type v21, value_type v31, value_type v02, value_type v12, value_type v22, value_type v32, value_type v03, value_type v13, value_type v23, value_type v33) : m_data {row_type {v00, v10, v20, v30}, row_type {v01, v11, v21, v31}, row_type {v02, v12, v22, v32}, row_type {v03, v13, v23, v33}} {}			
				
				explicit constexpr tmat (value_type w) : 
					m_data {row_type (tvec<4, T> (value_type (w), value_type (0), value_type (0), value_type (0))), row_type (tvec<4, T> (value_type (0), value_type (w), value_type (0), value_type (0))), row_type (tvec<4, T> (value_type (0), value_type (0), value_type (w), value_type (0))), row_type (tvec<4, T> (value_type (0), value_type (0), value_type (0), value_type (w)))}
				{}
				
				constexpr tmat () : tmat (value_type (0)) {}
			
				template <std::size_t K, typename I>
				explicit constexpr tmat (const tmat<K, 2, I>& in) :
					tmat (row_type (in [0]), row_type (in [1]), row_type (value_type (0)), row_type (value_type (0))) {}
			
				template <std::size_t K, typename I>
				explicit constexpr tmat (const tmat<K, 3, I>& in) :
					tmat (row_type (in [0]), row_type (in [1]), row_type (in [2]), row_type (value_type (0))) {}
			
				template <std::size_t K, typename I>
				explicit constexpr tmat (const tmat<K, 4, I>& in) :
					tmat (row_type (in [0]), row_type (in [1]), row_type (in [2]), row_type (in [3])) {}
					
				
				const auto& operator [] (std::size_t i) const {
					#if _NEWTON_MATH_RANGE_CHECK					
					if (i >= std::size (m_data)) throw std::out_of_range ("Matrix subscript out of range");				
					#endif
					return m_data [i];
				}
				
				auto& operator [] (std::size_t i) {
					#if _NEWTON_MATH_RANGE_CHECK 				
					if (i >= std::size (m_data)) throw std::out_of_range ("Matrix subscript out of range");				
					#endif
					return m_data [i];
				}
				
				auto& operator () (std::size_t col, std::size_t row) {
					return (*this) [row][col];
	
				}
				const auto& operator () (std::size_t col, std::size_t row) const {
					return (*this) [row][col];
	
				}
				
			private:
				row_type m_data [4];
			};

			template <typename T>
			inline const auto& row (const tmat<4, 4, T>& in, std::size_t i) {
				return in [i];
			}
			
			template <typename T>
			inline auto col (const tmat<4, 4, T>& in, std::size_t i) {
				return tvec<4, T> (in [0][i], in [1][i], in [2][i], in [3][i]);
			}
			
			template <typename T>
			inline auto transpose (const tmat<4, 4, T>& in) {
				return tmat_make (col (in, 0), col (in, 1), col (in, 2), col (in, 3));
			} 
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator * (const tmat<4, 4, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] * rhs, lhs [1] * rhs, lhs [2] * rhs, lhs [3] * rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator * (const _Lhs& lhs, const tmat<4, 4, _Rhs>& rhs) {
				return tmat_make (lhs * rhs [0], lhs * rhs [1], lhs * rhs [2], lhs * rhs [3]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator / (const tmat<4, 4, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] / rhs, lhs [1] / rhs, lhs [2] / rhs, lhs [3] / rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator / (const _Lhs& lhs, const tmat<4, 4, _Rhs>& rhs) {
				return tmat_make (lhs / rhs [0], lhs / rhs [1], lhs / rhs [2], lhs / rhs [3]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator + (const tmat<4, 4, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] + rhs, lhs [1] + rhs, lhs [2] + rhs, lhs [3] + rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator + (const _Lhs& lhs, const tmat<4, 4, _Rhs>& rhs) {
				return tmat_make (lhs + rhs [0], lhs + rhs [1], lhs + rhs [2], lhs + rhs [3]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator - (const tmat<4, 4, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] - rhs, lhs [1] - rhs, lhs [2] - rhs, lhs [3] - rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator - (const _Lhs& lhs, const tmat<4, 4, _Rhs>& rhs) {
				return tmat_make (lhs - rhs [0], lhs - rhs [1], lhs - rhs [2], lhs - rhs [3]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator % (const tmat<4, 4, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] % rhs, lhs [1] % rhs, lhs [2] % rhs, lhs [3] % rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator % (const _Lhs& lhs, const tmat<4, 4, _Rhs>& rhs) {
				return tmat_make (lhs % rhs [0], lhs % rhs [1], lhs % rhs [2], lhs % rhs [3]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator & (const tmat<4, 4, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] & rhs, lhs [1] & rhs, lhs [2] & rhs, lhs [3] & rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator & (const _Lhs& lhs, const tmat<4, 4, _Rhs>& rhs) {
				return tmat_make (lhs & rhs [0], lhs & rhs [1], lhs & rhs [2], lhs & rhs [3]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator | (const tmat<4, 4, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] | rhs, lhs [1] | rhs, lhs [2] | rhs, lhs [3] | rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator | (const _Lhs& lhs, const tmat<4, 4, _Rhs>& rhs) {
				return tmat_make (lhs | rhs [0], lhs | rhs [1], lhs | rhs [2], lhs | rhs [3]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator ^ (const tmat<4, 4, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] ^ rhs, lhs [1] ^ rhs, lhs [2] ^ rhs, lhs [3] ^ rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator ^ (const _Lhs& lhs, const tmat<4, 4, _Rhs>& rhs) {
				return tmat_make (lhs ^ rhs [0], lhs ^ rhs [1], lhs ^ rhs [2], lhs ^ rhs [3]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator || (const tmat<4, 4, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] || rhs, lhs [1] || rhs, lhs [2] || rhs, lhs [3] || rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator || (const _Lhs& lhs, const tmat<4, 4, _Rhs>& rhs) {
				return tmat_make (lhs || rhs [0], lhs || rhs [1], lhs || rhs [2], lhs || rhs [3]);
			}
			
			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Rhs>::value, int> = 0>
			inline auto operator && (const tmat<4, 4, _Lhs>& lhs, const _Rhs& rhs) {
				return tmat_make (lhs [0] && rhs, lhs [1] && rhs, lhs [2] && rhs, lhs [3] && rhs);
			}

			template <typename _Lhs, typename _Rhs, std::enable_if_t<std::is_scalar<_Lhs>::value, int> = 0>
			inline auto operator && (const _Lhs& lhs, const tmat<4, 4, _Rhs>& rhs) {
				return tmat_make (lhs && rhs [0], lhs && rhs [1], lhs && rhs [2], lhs && rhs [3]);
			}
			
			template <typename _Lhs, typename _Rhs>
			inline auto operator + (const tmat<4, 4, _Lhs>& lhs, const tmat<4, 4, _Rhs>& rhs) {
				return tmat_make (lhs [0] + rhs [0], lhs [1] + rhs [1], lhs [2] + rhs [2], lhs [3] + rhs [3]);				
			}

			template <typename _Lhs, typename _Rhs>
			inline auto operator - (const tmat<4, 4, _Lhs>& lhs, const tmat<4, 4, _Rhs>& rhs) {
				return tmat_make (lhs [0] - rhs [0], lhs [1] - rhs [1], lhs [2] - rhs [2], lhs [3] - rhs [3]);				
			}

			
			template <typename _Lhs, typename _Rhs>
			inline auto operator == (const tmat<4, 4, _Lhs>& lhs, const tmat<4, 4, _Rhs>& rhs) {
				return lhs [0] == rhs [0] && lhs [1] == rhs [1] && lhs [2] == rhs [2] && lhs [3] == rhs [3];				
			}

			template <typename _Lhs, typename _Rhs>
			inline auto operator != (const tmat<4, 4, _Lhs>& lhs, const tmat<4, 4, _Rhs>& rhs) {
				return lhs [0] != rhs [0] || lhs [1] != rhs [1] || lhs [2] != rhs [2] || lhs [3] != rhs [3];				
			}

			template <typename T> 
			inline std::ostream& operator << (std::ostream& oss, const tmat<4, 4, T>& in) {
				oss << std::setw(5) << in (0, 0) << ", ";
				oss << std::setw(5) << in (1, 0) << ", ";
				oss << std::setw(5) << in (2, 0) << ", ";
				oss << std::setw(5) << in (3, 0) << ", ";
				oss << std::endl ;
				oss << std::setw(5) << in (0, 1) << ", ";
				oss << std::setw(5) << in (1, 1) << ", ";
				oss << std::setw(5) << in (2, 1) << ", ";
				oss << std::setw(5) << in (3, 1) << ", ";
				oss << std::endl ;
				oss << std::setw(5) << in (0, 2) << ", ";
				oss << std::setw(5) << in (1, 2) << ", ";
				oss << std::setw(5) << in (2, 2) << ", ";
				oss << std::setw(5) << in (3, 2) << ", ";
				oss << std::endl ;
				oss << std::setw(5) << in (0, 3) << ", ";
				oss << std::setw(5) << in (1, 3) << ", ";
				oss << std::setw(5) << in (2, 3) << ", ";
				oss << std::setw(5) << in (3, 3) << ", ";
				oss << std::endl ;
				return oss;
			}
			
			template <typename T> 
			inline std::istream& operator >> (std::istream& iss, tmat<4, 4, T>& in) {
				iss >> in (0, 0);
				iss >> in (1, 0);
				iss >> in (2, 0);
				iss >> in (3, 0);
				iss >> in (0, 1);
				iss >> in (1, 1);
				iss >> in (2, 1);
				iss >> in (3, 1);
				iss >> in (0, 2);
				iss >> in (1, 2);
				iss >> in (2, 2);
				iss >> in (3, 2);
				iss >> in (0, 3);
				iss >> in (1, 3);
				iss >> in (2, 3);
				iss >> in (3, 3);
				return iss;
			}	
			
			template <typename _Lhs, typename _Rhs, typename _Ret = std::common_type_t<_Lhs, _Rhs>>
			inline auto operator * (const tmat<4, 4, _Lhs>& lhs, const tmat<4, 4, _Rhs>& rhs) {
				tmat<4, 4, _Ret> result;				
				result (0, 0) = dot (row (lhs, 0), col (rhs, 0));					
				result (1, 0) = dot (row (lhs, 0), col (rhs, 1));					
				result (2, 0) = dot (row (lhs, 0), col (rhs, 2));					
				result (3, 0) = dot (row (lhs, 0), col (rhs, 3));					
				result (0, 1) = dot (row (lhs, 1), col (rhs, 0));					
				result (1, 1) = dot (row (lhs, 1), col (rhs, 1));					
				result (2, 1) = dot (row (lhs, 1), col (rhs, 2));					
				result (3, 1) = dot (row (lhs, 1), col (rhs, 3));					
				result (0, 2) = dot (row (lhs, 2), col (rhs, 0));					
				result (1, 2) = dot (row (lhs, 2), col (rhs, 1));					
				result (2, 2) = dot (row (lhs, 2), col (rhs, 2));					
				result (3, 2) = dot (row (lhs, 2), col (rhs, 3));					
				result (0, 3) = dot (row (lhs, 3), col (rhs, 0));					
				result (1, 3) = dot (row (lhs, 3), col (rhs, 1));					
				result (2, 3) = dot (row (lhs, 3), col (rhs, 2));					
				result (3, 3) = dot (row (lhs, 3), col (rhs, 3));					
				return result;
			}
			
			template <typename _Lhs, typename _Rhs, typename _Ret = std::common_type_t<_Lhs, _Rhs>>
			inline auto operator * (const tmat<4, 4, _Lhs>& lhs, const tvec<4, _Rhs>& rhs) {
				tvec<4, _Ret> result;
				result [0] = dot (row (lhs, 0), rhs);
				result [1] = dot (row (lhs, 1), rhs);
				result [2] = dot (row (lhs, 2), rhs);
				result [3] = dot (row (lhs, 3), rhs);
				return result;
			}

			template <typename _Lhs, typename _Rhs, typename _Ret = std::common_type_t<_Lhs, _Rhs>>
			inline auto operator * (const tvec<4, _Lhs>& lhs, const tmat<4, 4, _Rhs>& rhs) {
				tvec<4, _Ret> result;
				result [0] = dot (lhs, col (rhs, 0));
				result [1] = dot (lhs, col (rhs, 1));
				result [2] = dot (lhs, col (rhs, 2));
				result [3] = dot (lhs, col (rhs, 3));
				return result;
			}			
			
			

		}
	}
}
#pragma pack(pop)


 #endif // __NEWTON_MATH_DETAIL_TMAT_4X4_ROW_MAJOR_HPP__