#ifndef __NEWTON_MATH_DETAIL_TVEC4_HPP__ 
#define __NEWTON_MATH_DETAIL_TVEC4_HPP__ 

#include <cstddef>
#include <cstdint>

#include "basic_tvec.hpp"

#include <istream>
#include <ostream>
#include <cmath>
#include <type_traits>

#pragma pack(push, 1)
namespace Newton {
	namespace Math {
		namespace detail {
			template<typename T>
			struct tvec_landing<4, T> {				
				tvec_landing (T& x, T& y, T& z, T& w) : x (x), y (y), z (z), w (w) {}
				
				tvec_landing<4, T>& operator = (const tvec<4, T>& in)  {
					x = in.x;
					y = in.y;
					z = in.z;
					w = in.w;
					return *this;
				}
				
			private:
				T& x;
				T& y;
				T& z;
				T& w;
			};
		}
				
		template <typename T>
		struct tvec<4, T> {
			typedef T value_type;
			static const std::size_t size = 4;			

			union {				
				struct { value_type x, y, z, w; };
				struct { value_type r, g, b, a; };
				struct { value_type s, t, p, q; };
				struct { value_type u, v, i, j; };

			#if !_NEWTON_MATH_DISABLE_SWIZZLE			
				detail::tvec_swizzle<T, 0, 0> xx;
				detail::tvec_swizzle<T, 0, 1> xy;
				detail::tvec_swizzle<T, 0, 2> xz;
				detail::tvec_swizzle<T, 0, 3> xw;
				detail::tvec_swizzle<T, 1, 0> yx;
				detail::tvec_swizzle<T, 1, 1> yy;
				detail::tvec_swizzle<T, 1, 2> yz;
				detail::tvec_swizzle<T, 1, 3> yw;
				detail::tvec_swizzle<T, 2, 0> zx;
				detail::tvec_swizzle<T, 2, 1> zy;
				detail::tvec_swizzle<T, 2, 2> zz;
				detail::tvec_swizzle<T, 2, 3> zw;
				detail::tvec_swizzle<T, 3, 0> wx;
				detail::tvec_swizzle<T, 3, 1> wy;
				detail::tvec_swizzle<T, 3, 2> wz;
				detail::tvec_swizzle<T, 3, 3> ww;
				detail::tvec_swizzle<T, 0, 0, 0> xxx;
				detail::tvec_swizzle<T, 0, 0, 1> xxy;
				detail::tvec_swizzle<T, 0, 0, 2> xxz;
				detail::tvec_swizzle<T, 0, 0, 3> xxw;
				detail::tvec_swizzle<T, 0, 1, 0> xyx;
				detail::tvec_swizzle<T, 0, 1, 1> xyy;
				detail::tvec_swizzle<T, 0, 1, 2> xyz;
				detail::tvec_swizzle<T, 0, 1, 3> xyw;
				detail::tvec_swizzle<T, 0, 2, 0> xzx;
				detail::tvec_swizzle<T, 0, 2, 1> xzy;
				detail::tvec_swizzle<T, 0, 2, 2> xzz;
				detail::tvec_swizzle<T, 0, 2, 3> xzw;
				detail::tvec_swizzle<T, 0, 3, 0> xwx;
				detail::tvec_swizzle<T, 0, 3, 1> xwy;
				detail::tvec_swizzle<T, 0, 3, 2> xwz;
				detail::tvec_swizzle<T, 0, 3, 3> xww;
				detail::tvec_swizzle<T, 1, 0, 0> yxx;
				detail::tvec_swizzle<T, 1, 0, 1> yxy;
				detail::tvec_swizzle<T, 1, 0, 2> yxz;
				detail::tvec_swizzle<T, 1, 0, 3> yxw;
				detail::tvec_swizzle<T, 1, 1, 0> yyx;
				detail::tvec_swizzle<T, 1, 1, 1> yyy;
				detail::tvec_swizzle<T, 1, 1, 2> yyz;
				detail::tvec_swizzle<T, 1, 1, 3> yyw;
				detail::tvec_swizzle<T, 1, 2, 0> yzx;
				detail::tvec_swizzle<T, 1, 2, 1> yzy;
				detail::tvec_swizzle<T, 1, 2, 2> yzz;
				detail::tvec_swizzle<T, 1, 2, 3> yzw;
				detail::tvec_swizzle<T, 1, 3, 0> ywx;
				detail::tvec_swizzle<T, 1, 3, 1> ywy;
				detail::tvec_swizzle<T, 1, 3, 2> ywz;
				detail::tvec_swizzle<T, 1, 3, 3> yww;
				detail::tvec_swizzle<T, 2, 0, 0> zxx;
				detail::tvec_swizzle<T, 2, 0, 1> zxy;
				detail::tvec_swizzle<T, 2, 0, 2> zxz;
				detail::tvec_swizzle<T, 2, 0, 3> zxw;
				detail::tvec_swizzle<T, 2, 1, 0> zyx;
				detail::tvec_swizzle<T, 2, 1, 1> zyy;
				detail::tvec_swizzle<T, 2, 1, 2> zyz;
				detail::tvec_swizzle<T, 2, 1, 3> zyw;
				detail::tvec_swizzle<T, 2, 2, 0> zzx;
				detail::tvec_swizzle<T, 2, 2, 1> zzy;
				detail::tvec_swizzle<T, 2, 2, 2> zzz;
				detail::tvec_swizzle<T, 2, 2, 3> zzw;
				detail::tvec_swizzle<T, 2, 3, 0> zwx;
				detail::tvec_swizzle<T, 2, 3, 1> zwy;
				detail::tvec_swizzle<T, 2, 3, 2> zwz;
				detail::tvec_swizzle<T, 2, 3, 3> zww;
				detail::tvec_swizzle<T, 3, 0, 0> wxx;
				detail::tvec_swizzle<T, 3, 0, 1> wxy;
				detail::tvec_swizzle<T, 3, 0, 2> wxz;
				detail::tvec_swizzle<T, 3, 0, 3> wxw;
				detail::tvec_swizzle<T, 3, 1, 0> wyx;
				detail::tvec_swizzle<T, 3, 1, 1> wyy;
				detail::tvec_swizzle<T, 3, 1, 2> wyz;
				detail::tvec_swizzle<T, 3, 1, 3> wyw;
				detail::tvec_swizzle<T, 3, 2, 0> wzx;
				detail::tvec_swizzle<T, 3, 2, 1> wzy;
				detail::tvec_swizzle<T, 3, 2, 2> wzz;
				detail::tvec_swizzle<T, 3, 2, 3> wzw;
				detail::tvec_swizzle<T, 3, 3, 0> wwx;
				detail::tvec_swizzle<T, 3, 3, 1> wwy;
				detail::tvec_swizzle<T, 3, 3, 2> wwz;
				detail::tvec_swizzle<T, 3, 3, 3> www;
				detail::tvec_swizzle<T, 0, 0, 0, 0> xxxx;
				detail::tvec_swizzle<T, 0, 0, 0, 1> xxxy;
				detail::tvec_swizzle<T, 0, 0, 0, 2> xxxz;
				detail::tvec_swizzle<T, 0, 0, 0, 3> xxxw;
				detail::tvec_swizzle<T, 0, 0, 1, 0> xxyx;
				detail::tvec_swizzle<T, 0, 0, 1, 1> xxyy;
				detail::tvec_swizzle<T, 0, 0, 1, 2> xxyz;
				detail::tvec_swizzle<T, 0, 0, 1, 3> xxyw;
				detail::tvec_swizzle<T, 0, 0, 2, 0> xxzx;
				detail::tvec_swizzle<T, 0, 0, 2, 1> xxzy;
				detail::tvec_swizzle<T, 0, 0, 2, 2> xxzz;
				detail::tvec_swizzle<T, 0, 0, 2, 3> xxzw;
				detail::tvec_swizzle<T, 0, 0, 3, 0> xxwx;
				detail::tvec_swizzle<T, 0, 0, 3, 1> xxwy;
				detail::tvec_swizzle<T, 0, 0, 3, 2> xxwz;
				detail::tvec_swizzle<T, 0, 0, 3, 3> xxww;
				detail::tvec_swizzle<T, 0, 1, 0, 0> xyxx;
				detail::tvec_swizzle<T, 0, 1, 0, 1> xyxy;
				detail::tvec_swizzle<T, 0, 1, 0, 2> xyxz;
				detail::tvec_swizzle<T, 0, 1, 0, 3> xyxw;
				detail::tvec_swizzle<T, 0, 1, 1, 0> xyyx;
				detail::tvec_swizzle<T, 0, 1, 1, 1> xyyy;
				detail::tvec_swizzle<T, 0, 1, 1, 2> xyyz;
				detail::tvec_swizzle<T, 0, 1, 1, 3> xyyw;
				detail::tvec_swizzle<T, 0, 1, 2, 0> xyzx;
				detail::tvec_swizzle<T, 0, 1, 2, 1> xyzy;
				detail::tvec_swizzle<T, 0, 1, 2, 2> xyzz;
				detail::tvec_swizzle<T, 0, 1, 2, 3> xyzw;
				detail::tvec_swizzle<T, 0, 1, 3, 0> xywx;
				detail::tvec_swizzle<T, 0, 1, 3, 1> xywy;
				detail::tvec_swizzle<T, 0, 1, 3, 2> xywz;
				detail::tvec_swizzle<T, 0, 1, 3, 3> xyww;
				detail::tvec_swizzle<T, 0, 2, 0, 0> xzxx;
				detail::tvec_swizzle<T, 0, 2, 0, 1> xzxy;
				detail::tvec_swizzle<T, 0, 2, 0, 2> xzxz;
				detail::tvec_swizzle<T, 0, 2, 0, 3> xzxw;
				detail::tvec_swizzle<T, 0, 2, 1, 0> xzyx;
				detail::tvec_swizzle<T, 0, 2, 1, 1> xzyy;
				detail::tvec_swizzle<T, 0, 2, 1, 2> xzyz;
				detail::tvec_swizzle<T, 0, 2, 1, 3> xzyw;
				detail::tvec_swizzle<T, 0, 2, 2, 0> xzzx;
				detail::tvec_swizzle<T, 0, 2, 2, 1> xzzy;
				detail::tvec_swizzle<T, 0, 2, 2, 2> xzzz;
				detail::tvec_swizzle<T, 0, 2, 2, 3> xzzw;
				detail::tvec_swizzle<T, 0, 2, 3, 0> xzwx;
				detail::tvec_swizzle<T, 0, 2, 3, 1> xzwy;
				detail::tvec_swizzle<T, 0, 2, 3, 2> xzwz;
				detail::tvec_swizzle<T, 0, 2, 3, 3> xzww;
				detail::tvec_swizzle<T, 0, 3, 0, 0> xwxx;
				detail::tvec_swizzle<T, 0, 3, 0, 1> xwxy;
				detail::tvec_swizzle<T, 0, 3, 0, 2> xwxz;
				detail::tvec_swizzle<T, 0, 3, 0, 3> xwxw;
				detail::tvec_swizzle<T, 0, 3, 1, 0> xwyx;
				detail::tvec_swizzle<T, 0, 3, 1, 1> xwyy;
				detail::tvec_swizzle<T, 0, 3, 1, 2> xwyz;
				detail::tvec_swizzle<T, 0, 3, 1, 3> xwyw;
				detail::tvec_swizzle<T, 0, 3, 2, 0> xwzx;
				detail::tvec_swizzle<T, 0, 3, 2, 1> xwzy;
				detail::tvec_swizzle<T, 0, 3, 2, 2> xwzz;
				detail::tvec_swizzle<T, 0, 3, 2, 3> xwzw;
				detail::tvec_swizzle<T, 0, 3, 3, 0> xwwx;
				detail::tvec_swizzle<T, 0, 3, 3, 1> xwwy;
				detail::tvec_swizzle<T, 0, 3, 3, 2> xwwz;
				detail::tvec_swizzle<T, 0, 3, 3, 3> xwww;
				detail::tvec_swizzle<T, 1, 0, 0, 0> yxxx;
				detail::tvec_swizzle<T, 1, 0, 0, 1> yxxy;
				detail::tvec_swizzle<T, 1, 0, 0, 2> yxxz;
				detail::tvec_swizzle<T, 1, 0, 0, 3> yxxw;
				detail::tvec_swizzle<T, 1, 0, 1, 0> yxyx;
				detail::tvec_swizzle<T, 1, 0, 1, 1> yxyy;
				detail::tvec_swizzle<T, 1, 0, 1, 2> yxyz;
				detail::tvec_swizzle<T, 1, 0, 1, 3> yxyw;
				detail::tvec_swizzle<T, 1, 0, 2, 0> yxzx;
				detail::tvec_swizzle<T, 1, 0, 2, 1> yxzy;
				detail::tvec_swizzle<T, 1, 0, 2, 2> yxzz;
				detail::tvec_swizzle<T, 1, 0, 2, 3> yxzw;
				detail::tvec_swizzle<T, 1, 0, 3, 0> yxwx;
				detail::tvec_swizzle<T, 1, 0, 3, 1> yxwy;
				detail::tvec_swizzle<T, 1, 0, 3, 2> yxwz;
				detail::tvec_swizzle<T, 1, 0, 3, 3> yxww;
				detail::tvec_swizzle<T, 1, 1, 0, 0> yyxx;
				detail::tvec_swizzle<T, 1, 1, 0, 1> yyxy;
				detail::tvec_swizzle<T, 1, 1, 0, 2> yyxz;
				detail::tvec_swizzle<T, 1, 1, 0, 3> yyxw;
				detail::tvec_swizzle<T, 1, 1, 1, 0> yyyx;
				detail::tvec_swizzle<T, 1, 1, 1, 1> yyyy;
				detail::tvec_swizzle<T, 1, 1, 1, 2> yyyz;
				detail::tvec_swizzle<T, 1, 1, 1, 3> yyyw;
				detail::tvec_swizzle<T, 1, 1, 2, 0> yyzx;
				detail::tvec_swizzle<T, 1, 1, 2, 1> yyzy;
				detail::tvec_swizzle<T, 1, 1, 2, 2> yyzz;
				detail::tvec_swizzle<T, 1, 1, 2, 3> yyzw;
				detail::tvec_swizzle<T, 1, 1, 3, 0> yywx;
				detail::tvec_swizzle<T, 1, 1, 3, 1> yywy;
				detail::tvec_swizzle<T, 1, 1, 3, 2> yywz;
				detail::tvec_swizzle<T, 1, 1, 3, 3> yyww;
				detail::tvec_swizzle<T, 1, 2, 0, 0> yzxx;
				detail::tvec_swizzle<T, 1, 2, 0, 1> yzxy;
				detail::tvec_swizzle<T, 1, 2, 0, 2> yzxz;
				detail::tvec_swizzle<T, 1, 2, 0, 3> yzxw;
				detail::tvec_swizzle<T, 1, 2, 1, 0> yzyx;
				detail::tvec_swizzle<T, 1, 2, 1, 1> yzyy;
				detail::tvec_swizzle<T, 1, 2, 1, 2> yzyz;
				detail::tvec_swizzle<T, 1, 2, 1, 3> yzyw;
				detail::tvec_swizzle<T, 1, 2, 2, 0> yzzx;
				detail::tvec_swizzle<T, 1, 2, 2, 1> yzzy;
				detail::tvec_swizzle<T, 1, 2, 2, 2> yzzz;
				detail::tvec_swizzle<T, 1, 2, 2, 3> yzzw;
				detail::tvec_swizzle<T, 1, 2, 3, 0> yzwx;
				detail::tvec_swizzle<T, 1, 2, 3, 1> yzwy;
				detail::tvec_swizzle<T, 1, 2, 3, 2> yzwz;
				detail::tvec_swizzle<T, 1, 2, 3, 3> yzww;
				detail::tvec_swizzle<T, 1, 3, 0, 0> ywxx;
				detail::tvec_swizzle<T, 1, 3, 0, 1> ywxy;
				detail::tvec_swizzle<T, 1, 3, 0, 2> ywxz;
				detail::tvec_swizzle<T, 1, 3, 0, 3> ywxw;
				detail::tvec_swizzle<T, 1, 3, 1, 0> ywyx;
				detail::tvec_swizzle<T, 1, 3, 1, 1> ywyy;
				detail::tvec_swizzle<T, 1, 3, 1, 2> ywyz;
				detail::tvec_swizzle<T, 1, 3, 1, 3> ywyw;
				detail::tvec_swizzle<T, 1, 3, 2, 0> ywzx;
				detail::tvec_swizzle<T, 1, 3, 2, 1> ywzy;
				detail::tvec_swizzle<T, 1, 3, 2, 2> ywzz;
				detail::tvec_swizzle<T, 1, 3, 2, 3> ywzw;
				detail::tvec_swizzle<T, 1, 3, 3, 0> ywwx;
				detail::tvec_swizzle<T, 1, 3, 3, 1> ywwy;
				detail::tvec_swizzle<T, 1, 3, 3, 2> ywwz;
				detail::tvec_swizzle<T, 1, 3, 3, 3> ywww;
				detail::tvec_swizzle<T, 2, 0, 0, 0> zxxx;
				detail::tvec_swizzle<T, 2, 0, 0, 1> zxxy;
				detail::tvec_swizzle<T, 2, 0, 0, 2> zxxz;
				detail::tvec_swizzle<T, 2, 0, 0, 3> zxxw;
				detail::tvec_swizzle<T, 2, 0, 1, 0> zxyx;
				detail::tvec_swizzle<T, 2, 0, 1, 1> zxyy;
				detail::tvec_swizzle<T, 2, 0, 1, 2> zxyz;
				detail::tvec_swizzle<T, 2, 0, 1, 3> zxyw;
				detail::tvec_swizzle<T, 2, 0, 2, 0> zxzx;
				detail::tvec_swizzle<T, 2, 0, 2, 1> zxzy;
				detail::tvec_swizzle<T, 2, 0, 2, 2> zxzz;
				detail::tvec_swizzle<T, 2, 0, 2, 3> zxzw;
				detail::tvec_swizzle<T, 2, 0, 3, 0> zxwx;
				detail::tvec_swizzle<T, 2, 0, 3, 1> zxwy;
				detail::tvec_swizzle<T, 2, 0, 3, 2> zxwz;
				detail::tvec_swizzle<T, 2, 0, 3, 3> zxww;
				detail::tvec_swizzle<T, 2, 1, 0, 0> zyxx;
				detail::tvec_swizzle<T, 2, 1, 0, 1> zyxy;
				detail::tvec_swizzle<T, 2, 1, 0, 2> zyxz;
				detail::tvec_swizzle<T, 2, 1, 0, 3> zyxw;
				detail::tvec_swizzle<T, 2, 1, 1, 0> zyyx;
				detail::tvec_swizzle<T, 2, 1, 1, 1> zyyy;
				detail::tvec_swizzle<T, 2, 1, 1, 2> zyyz;
				detail::tvec_swizzle<T, 2, 1, 1, 3> zyyw;
				detail::tvec_swizzle<T, 2, 1, 2, 0> zyzx;
				detail::tvec_swizzle<T, 2, 1, 2, 1> zyzy;
				detail::tvec_swizzle<T, 2, 1, 2, 2> zyzz;
				detail::tvec_swizzle<T, 2, 1, 2, 3> zyzw;
				detail::tvec_swizzle<T, 2, 1, 3, 0> zywx;
				detail::tvec_swizzle<T, 2, 1, 3, 1> zywy;
				detail::tvec_swizzle<T, 2, 1, 3, 2> zywz;
				detail::tvec_swizzle<T, 2, 1, 3, 3> zyww;
				detail::tvec_swizzle<T, 2, 2, 0, 0> zzxx;
				detail::tvec_swizzle<T, 2, 2, 0, 1> zzxy;
				detail::tvec_swizzle<T, 2, 2, 0, 2> zzxz;
				detail::tvec_swizzle<T, 2, 2, 0, 3> zzxw;
				detail::tvec_swizzle<T, 2, 2, 1, 0> zzyx;
				detail::tvec_swizzle<T, 2, 2, 1, 1> zzyy;
				detail::tvec_swizzle<T, 2, 2, 1, 2> zzyz;
				detail::tvec_swizzle<T, 2, 2, 1, 3> zzyw;
				detail::tvec_swizzle<T, 2, 2, 2, 0> zzzx;
				detail::tvec_swizzle<T, 2, 2, 2, 1> zzzy;
				detail::tvec_swizzle<T, 2, 2, 2, 2> zzzz;
				detail::tvec_swizzle<T, 2, 2, 2, 3> zzzw;
				detail::tvec_swizzle<T, 2, 2, 3, 0> zzwx;
				detail::tvec_swizzle<T, 2, 2, 3, 1> zzwy;
				detail::tvec_swizzle<T, 2, 2, 3, 2> zzwz;
				detail::tvec_swizzle<T, 2, 2, 3, 3> zzww;
				detail::tvec_swizzle<T, 2, 3, 0, 0> zwxx;
				detail::tvec_swizzle<T, 2, 3, 0, 1> zwxy;
				detail::tvec_swizzle<T, 2, 3, 0, 2> zwxz;
				detail::tvec_swizzle<T, 2, 3, 0, 3> zwxw;
				detail::tvec_swizzle<T, 2, 3, 1, 0> zwyx;
				detail::tvec_swizzle<T, 2, 3, 1, 1> zwyy;
				detail::tvec_swizzle<T, 2, 3, 1, 2> zwyz;
				detail::tvec_swizzle<T, 2, 3, 1, 3> zwyw;
				detail::tvec_swizzle<T, 2, 3, 2, 0> zwzx;
				detail::tvec_swizzle<T, 2, 3, 2, 1> zwzy;
				detail::tvec_swizzle<T, 2, 3, 2, 2> zwzz;
				detail::tvec_swizzle<T, 2, 3, 2, 3> zwzw;
				detail::tvec_swizzle<T, 2, 3, 3, 0> zwwx;
				detail::tvec_swizzle<T, 2, 3, 3, 1> zwwy;
				detail::tvec_swizzle<T, 2, 3, 3, 2> zwwz;
				detail::tvec_swizzle<T, 2, 3, 3, 3> zwww;
				detail::tvec_swizzle<T, 3, 0, 0, 0> wxxx;
				detail::tvec_swizzle<T, 3, 0, 0, 1> wxxy;
				detail::tvec_swizzle<T, 3, 0, 0, 2> wxxz;
				detail::tvec_swizzle<T, 3, 0, 0, 3> wxxw;
				detail::tvec_swizzle<T, 3, 0, 1, 0> wxyx;
				detail::tvec_swizzle<T, 3, 0, 1, 1> wxyy;
				detail::tvec_swizzle<T, 3, 0, 1, 2> wxyz;
				detail::tvec_swizzle<T, 3, 0, 1, 3> wxyw;
				detail::tvec_swizzle<T, 3, 0, 2, 0> wxzx;
				detail::tvec_swizzle<T, 3, 0, 2, 1> wxzy;
				detail::tvec_swizzle<T, 3, 0, 2, 2> wxzz;
				detail::tvec_swizzle<T, 3, 0, 2, 3> wxzw;
				detail::tvec_swizzle<T, 3, 0, 3, 0> wxwx;
				detail::tvec_swizzle<T, 3, 0, 3, 1> wxwy;
				detail::tvec_swizzle<T, 3, 0, 3, 2> wxwz;
				detail::tvec_swizzle<T, 3, 0, 3, 3> wxww;
				detail::tvec_swizzle<T, 3, 1, 0, 0> wyxx;
				detail::tvec_swizzle<T, 3, 1, 0, 1> wyxy;
				detail::tvec_swizzle<T, 3, 1, 0, 2> wyxz;
				detail::tvec_swizzle<T, 3, 1, 0, 3> wyxw;
				detail::tvec_swizzle<T, 3, 1, 1, 0> wyyx;
				detail::tvec_swizzle<T, 3, 1, 1, 1> wyyy;
				detail::tvec_swizzle<T, 3, 1, 1, 2> wyyz;
				detail::tvec_swizzle<T, 3, 1, 1, 3> wyyw;
				detail::tvec_swizzle<T, 3, 1, 2, 0> wyzx;
				detail::tvec_swizzle<T, 3, 1, 2, 1> wyzy;
				detail::tvec_swizzle<T, 3, 1, 2, 2> wyzz;
				detail::tvec_swizzle<T, 3, 1, 2, 3> wyzw;
				detail::tvec_swizzle<T, 3, 1, 3, 0> wywx;
				detail::tvec_swizzle<T, 3, 1, 3, 1> wywy;
				detail::tvec_swizzle<T, 3, 1, 3, 2> wywz;
				detail::tvec_swizzle<T, 3, 1, 3, 3> wyww;
				detail::tvec_swizzle<T, 3, 2, 0, 0> wzxx;
				detail::tvec_swizzle<T, 3, 2, 0, 1> wzxy;
				detail::tvec_swizzle<T, 3, 2, 0, 2> wzxz;
				detail::tvec_swizzle<T, 3, 2, 0, 3> wzxw;
				detail::tvec_swizzle<T, 3, 2, 1, 0> wzyx;
				detail::tvec_swizzle<T, 3, 2, 1, 1> wzyy;
				detail::tvec_swizzle<T, 3, 2, 1, 2> wzyz;
				detail::tvec_swizzle<T, 3, 2, 1, 3> wzyw;
				detail::tvec_swizzle<T, 3, 2, 2, 0> wzzx;
				detail::tvec_swizzle<T, 3, 2, 2, 1> wzzy;
				detail::tvec_swizzle<T, 3, 2, 2, 2> wzzz;
				detail::tvec_swizzle<T, 3, 2, 2, 3> wzzw;
				detail::tvec_swizzle<T, 3, 2, 3, 0> wzwx;
				detail::tvec_swizzle<T, 3, 2, 3, 1> wzwy;
				detail::tvec_swizzle<T, 3, 2, 3, 2> wzwz;
				detail::tvec_swizzle<T, 3, 2, 3, 3> wzww;
				detail::tvec_swizzle<T, 3, 3, 0, 0> wwxx;
				detail::tvec_swizzle<T, 3, 3, 0, 1> wwxy;
				detail::tvec_swizzle<T, 3, 3, 0, 2> wwxz;
				detail::tvec_swizzle<T, 3, 3, 0, 3> wwxw;
				detail::tvec_swizzle<T, 3, 3, 1, 0> wwyx;
				detail::tvec_swizzle<T, 3, 3, 1, 1> wwyy;
				detail::tvec_swizzle<T, 3, 3, 1, 2> wwyz;
				detail::tvec_swizzle<T, 3, 3, 1, 3> wwyw;
				detail::tvec_swizzle<T, 3, 3, 2, 0> wwzx;
				detail::tvec_swizzle<T, 3, 3, 2, 1> wwzy;
				detail::tvec_swizzle<T, 3, 3, 2, 2> wwzz;
				detail::tvec_swizzle<T, 3, 3, 2, 3> wwzw;
				detail::tvec_swizzle<T, 3, 3, 3, 0> wwwx;
				detail::tvec_swizzle<T, 3, 3, 3, 1> wwwy;
				detail::tvec_swizzle<T, 3, 3, 3, 2> wwwz;
				detail::tvec_swizzle<T, 3, 3, 3, 3> wwww;
				detail::tvec_swizzle<T, 0, 0> rr;
				detail::tvec_swizzle<T, 0, 1> rg;
				detail::tvec_swizzle<T, 0, 2> rb;
				detail::tvec_swizzle<T, 0, 3> ra;
				detail::tvec_swizzle<T, 1, 0> gr;
				detail::tvec_swizzle<T, 1, 1> gg;
				detail::tvec_swizzle<T, 1, 2> gb;
				detail::tvec_swizzle<T, 1, 3> ga;
				detail::tvec_swizzle<T, 2, 0> br;
				detail::tvec_swizzle<T, 2, 1> bg;
				detail::tvec_swizzle<T, 2, 2> bb;
				detail::tvec_swizzle<T, 2, 3> ba;
				detail::tvec_swizzle<T, 3, 0> ar;
				detail::tvec_swizzle<T, 3, 1> ag;
				detail::tvec_swizzle<T, 3, 2> ab;
				detail::tvec_swizzle<T, 3, 3> aa;
				detail::tvec_swizzle<T, 0, 0, 0> rrr;
				detail::tvec_swizzle<T, 0, 0, 1> rrg;
				detail::tvec_swizzle<T, 0, 0, 2> rrb;
				detail::tvec_swizzle<T, 0, 0, 3> rra;
				detail::tvec_swizzle<T, 0, 1, 0> rgr;
				detail::tvec_swizzle<T, 0, 1, 1> rgg;
				detail::tvec_swizzle<T, 0, 1, 2> rgb;
				detail::tvec_swizzle<T, 0, 1, 3> rga;
				detail::tvec_swizzle<T, 0, 2, 0> rbr;
				detail::tvec_swizzle<T, 0, 2, 1> rbg;
				detail::tvec_swizzle<T, 0, 2, 2> rbb;
				detail::tvec_swizzle<T, 0, 2, 3> rba;
				detail::tvec_swizzle<T, 0, 3, 0> rar;
				detail::tvec_swizzle<T, 0, 3, 1> rag;
				detail::tvec_swizzle<T, 0, 3, 2> rab;
				detail::tvec_swizzle<T, 0, 3, 3> raa;
				detail::tvec_swizzle<T, 1, 0, 0> grr;
				detail::tvec_swizzle<T, 1, 0, 1> grg;
				detail::tvec_swizzle<T, 1, 0, 2> grb;
				detail::tvec_swizzle<T, 1, 0, 3> gra;
				detail::tvec_swizzle<T, 1, 1, 0> ggr;
				detail::tvec_swizzle<T, 1, 1, 1> ggg;
				detail::tvec_swizzle<T, 1, 1, 2> ggb;
				detail::tvec_swizzle<T, 1, 1, 3> gga;
				detail::tvec_swizzle<T, 1, 2, 0> gbr;
				detail::tvec_swizzle<T, 1, 2, 1> gbg;
				detail::tvec_swizzle<T, 1, 2, 2> gbb;
				detail::tvec_swizzle<T, 1, 2, 3> gba;
				detail::tvec_swizzle<T, 1, 3, 0> gar;
				detail::tvec_swizzle<T, 1, 3, 1> gag;
				detail::tvec_swizzle<T, 1, 3, 2> gab;
				detail::tvec_swizzle<T, 1, 3, 3> gaa;
				detail::tvec_swizzle<T, 2, 0, 0> brr;
				detail::tvec_swizzle<T, 2, 0, 1> brg;
				detail::tvec_swizzle<T, 2, 0, 2> brb;
				detail::tvec_swizzle<T, 2, 0, 3> bra;
				detail::tvec_swizzle<T, 2, 1, 0> bgr;
				detail::tvec_swizzle<T, 2, 1, 1> bgg;
				detail::tvec_swizzle<T, 2, 1, 2> bgb;
				detail::tvec_swizzle<T, 2, 1, 3> bga;
				detail::tvec_swizzle<T, 2, 2, 0> bbr;
				detail::tvec_swizzle<T, 2, 2, 1> bbg;
				detail::tvec_swizzle<T, 2, 2, 2> bbb;
				detail::tvec_swizzle<T, 2, 2, 3> bba;
				detail::tvec_swizzle<T, 2, 3, 0> bar;
				detail::tvec_swizzle<T, 2, 3, 1> bag;
				detail::tvec_swizzle<T, 2, 3, 2> bab;
				detail::tvec_swizzle<T, 2, 3, 3> baa;
				detail::tvec_swizzle<T, 3, 0, 0> arr;
				detail::tvec_swizzle<T, 3, 0, 1> arg;
				detail::tvec_swizzle<T, 3, 0, 2> arb;
				detail::tvec_swizzle<T, 3, 0, 3> ara;
				detail::tvec_swizzle<T, 3, 1, 0> agr;
				detail::tvec_swizzle<T, 3, 1, 1> agg;
				detail::tvec_swizzle<T, 3, 1, 2> agb;
				detail::tvec_swizzle<T, 3, 1, 3> aga;
				detail::tvec_swizzle<T, 3, 2, 0> abr;
				detail::tvec_swizzle<T, 3, 2, 1> abg;
				detail::tvec_swizzle<T, 3, 2, 2> abb;
				detail::tvec_swizzle<T, 3, 2, 3> aba;
				detail::tvec_swizzle<T, 3, 3, 0> aar;
				detail::tvec_swizzle<T, 3, 3, 1> aag;
				detail::tvec_swizzle<T, 3, 3, 2> aab;
				detail::tvec_swizzle<T, 3, 3, 3> aaa;
				detail::tvec_swizzle<T, 0, 0, 0, 0> rrrr;
				detail::tvec_swizzle<T, 0, 0, 0, 1> rrrg;
				detail::tvec_swizzle<T, 0, 0, 0, 2> rrrb;
				detail::tvec_swizzle<T, 0, 0, 0, 3> rrra;
				detail::tvec_swizzle<T, 0, 0, 1, 0> rrgr;
				detail::tvec_swizzle<T, 0, 0, 1, 1> rrgg;
				detail::tvec_swizzle<T, 0, 0, 1, 2> rrgb;
				detail::tvec_swizzle<T, 0, 0, 1, 3> rrga;
				detail::tvec_swizzle<T, 0, 0, 2, 0> rrbr;
				detail::tvec_swizzle<T, 0, 0, 2, 1> rrbg;
				detail::tvec_swizzle<T, 0, 0, 2, 2> rrbb;
				detail::tvec_swizzle<T, 0, 0, 2, 3> rrba;
				detail::tvec_swizzle<T, 0, 0, 3, 0> rrar;
				detail::tvec_swizzle<T, 0, 0, 3, 1> rrag;
				detail::tvec_swizzle<T, 0, 0, 3, 2> rrab;
				detail::tvec_swizzle<T, 0, 0, 3, 3> rraa;
				detail::tvec_swizzle<T, 0, 1, 0, 0> rgrr;
				detail::tvec_swizzle<T, 0, 1, 0, 1> rgrg;
				detail::tvec_swizzle<T, 0, 1, 0, 2> rgrb;
				detail::tvec_swizzle<T, 0, 1, 0, 3> rgra;
				detail::tvec_swizzle<T, 0, 1, 1, 0> rggr;
				detail::tvec_swizzle<T, 0, 1, 1, 1> rggg;
				detail::tvec_swizzle<T, 0, 1, 1, 2> rggb;
				detail::tvec_swizzle<T, 0, 1, 1, 3> rgga;
				detail::tvec_swizzle<T, 0, 1, 2, 0> rgbr;
				detail::tvec_swizzle<T, 0, 1, 2, 1> rgbg;
				detail::tvec_swizzle<T, 0, 1, 2, 2> rgbb;
				detail::tvec_swizzle<T, 0, 1, 2, 3> rgba;
				detail::tvec_swizzle<T, 0, 1, 3, 0> rgar;
				detail::tvec_swizzle<T, 0, 1, 3, 1> rgag;
				detail::tvec_swizzle<T, 0, 1, 3, 2> rgab;
				detail::tvec_swizzle<T, 0, 1, 3, 3> rgaa;
				detail::tvec_swizzle<T, 0, 2, 0, 0> rbrr;
				detail::tvec_swizzle<T, 0, 2, 0, 1> rbrg;
				detail::tvec_swizzle<T, 0, 2, 0, 2> rbrb;
				detail::tvec_swizzle<T, 0, 2, 0, 3> rbra;
				detail::tvec_swizzle<T, 0, 2, 1, 0> rbgr;
				detail::tvec_swizzle<T, 0, 2, 1, 1> rbgg;
				detail::tvec_swizzle<T, 0, 2, 1, 2> rbgb;
				detail::tvec_swizzle<T, 0, 2, 1, 3> rbga;
				detail::tvec_swizzle<T, 0, 2, 2, 0> rbbr;
				detail::tvec_swizzle<T, 0, 2, 2, 1> rbbg;
				detail::tvec_swizzle<T, 0, 2, 2, 2> rbbb;
				detail::tvec_swizzle<T, 0, 2, 2, 3> rbba;
				detail::tvec_swizzle<T, 0, 2, 3, 0> rbar;
				detail::tvec_swizzle<T, 0, 2, 3, 1> rbag;
				detail::tvec_swizzle<T, 0, 2, 3, 2> rbab;
				detail::tvec_swizzle<T, 0, 2, 3, 3> rbaa;
				detail::tvec_swizzle<T, 0, 3, 0, 0> rarr;
				detail::tvec_swizzle<T, 0, 3, 0, 1> rarg;
				detail::tvec_swizzle<T, 0, 3, 0, 2> rarb;
				detail::tvec_swizzle<T, 0, 3, 0, 3> rara;
				detail::tvec_swizzle<T, 0, 3, 1, 0> ragr;
				detail::tvec_swizzle<T, 0, 3, 1, 1> ragg;
				detail::tvec_swizzle<T, 0, 3, 1, 2> ragb;
				detail::tvec_swizzle<T, 0, 3, 1, 3> raga;
				detail::tvec_swizzle<T, 0, 3, 2, 0> rabr;
				detail::tvec_swizzle<T, 0, 3, 2, 1> rabg;
				detail::tvec_swizzle<T, 0, 3, 2, 2> rabb;
				detail::tvec_swizzle<T, 0, 3, 2, 3> raba;
				detail::tvec_swizzle<T, 0, 3, 3, 0> raar;
				detail::tvec_swizzle<T, 0, 3, 3, 1> raag;
				detail::tvec_swizzle<T, 0, 3, 3, 2> raab;
				detail::tvec_swizzle<T, 0, 3, 3, 3> raaa;
				detail::tvec_swizzle<T, 1, 0, 0, 0> grrr;
				detail::tvec_swizzle<T, 1, 0, 0, 1> grrg;
				detail::tvec_swizzle<T, 1, 0, 0, 2> grrb;
				detail::tvec_swizzle<T, 1, 0, 0, 3> grra;
				detail::tvec_swizzle<T, 1, 0, 1, 0> grgr;
				detail::tvec_swizzle<T, 1, 0, 1, 1> grgg;
				detail::tvec_swizzle<T, 1, 0, 1, 2> grgb;
				detail::tvec_swizzle<T, 1, 0, 1, 3> grga;
				detail::tvec_swizzle<T, 1, 0, 2, 0> grbr;
				detail::tvec_swizzle<T, 1, 0, 2, 1> grbg;
				detail::tvec_swizzle<T, 1, 0, 2, 2> grbb;
				detail::tvec_swizzle<T, 1, 0, 2, 3> grba;
				detail::tvec_swizzle<T, 1, 0, 3, 0> grar;
				detail::tvec_swizzle<T, 1, 0, 3, 1> grag;
				detail::tvec_swizzle<T, 1, 0, 3, 2> grab;
				detail::tvec_swizzle<T, 1, 0, 3, 3> graa;
				detail::tvec_swizzle<T, 1, 1, 0, 0> ggrr;
				detail::tvec_swizzle<T, 1, 1, 0, 1> ggrg;
				detail::tvec_swizzle<T, 1, 1, 0, 2> ggrb;
				detail::tvec_swizzle<T, 1, 1, 0, 3> ggra;
				detail::tvec_swizzle<T, 1, 1, 1, 0> gggr;
				detail::tvec_swizzle<T, 1, 1, 1, 1> gggg;
				detail::tvec_swizzle<T, 1, 1, 1, 2> gggb;
				detail::tvec_swizzle<T, 1, 1, 1, 3> ggga;
				detail::tvec_swizzle<T, 1, 1, 2, 0> ggbr;
				detail::tvec_swizzle<T, 1, 1, 2, 1> ggbg;
				detail::tvec_swizzle<T, 1, 1, 2, 2> ggbb;
				detail::tvec_swizzle<T, 1, 1, 2, 3> ggba;
				detail::tvec_swizzle<T, 1, 1, 3, 0> ggar;
				detail::tvec_swizzle<T, 1, 1, 3, 1> ggag;
				detail::tvec_swizzle<T, 1, 1, 3, 2> ggab;
				detail::tvec_swizzle<T, 1, 1, 3, 3> ggaa;
				detail::tvec_swizzle<T, 1, 2, 0, 0> gbrr;
				detail::tvec_swizzle<T, 1, 2, 0, 1> gbrg;
				detail::tvec_swizzle<T, 1, 2, 0, 2> gbrb;
				detail::tvec_swizzle<T, 1, 2, 0, 3> gbra;
				detail::tvec_swizzle<T, 1, 2, 1, 0> gbgr;
				detail::tvec_swizzle<T, 1, 2, 1, 1> gbgg;
				detail::tvec_swizzle<T, 1, 2, 1, 2> gbgb;
				detail::tvec_swizzle<T, 1, 2, 1, 3> gbga;
				detail::tvec_swizzle<T, 1, 2, 2, 0> gbbr;
				detail::tvec_swizzle<T, 1, 2, 2, 1> gbbg;
				detail::tvec_swizzle<T, 1, 2, 2, 2> gbbb;
				detail::tvec_swizzle<T, 1, 2, 2, 3> gbba;
				detail::tvec_swizzle<T, 1, 2, 3, 0> gbar;
				detail::tvec_swizzle<T, 1, 2, 3, 1> gbag;
				detail::tvec_swizzle<T, 1, 2, 3, 2> gbab;
				detail::tvec_swizzle<T, 1, 2, 3, 3> gbaa;
				detail::tvec_swizzle<T, 1, 3, 0, 0> garr;
				detail::tvec_swizzle<T, 1, 3, 0, 1> garg;
				detail::tvec_swizzle<T, 1, 3, 0, 2> garb;
				detail::tvec_swizzle<T, 1, 3, 0, 3> gara;
				detail::tvec_swizzle<T, 1, 3, 1, 0> gagr;
				detail::tvec_swizzle<T, 1, 3, 1, 1> gagg;
				detail::tvec_swizzle<T, 1, 3, 1, 2> gagb;
				detail::tvec_swizzle<T, 1, 3, 1, 3> gaga;
				detail::tvec_swizzle<T, 1, 3, 2, 0> gabr;
				detail::tvec_swizzle<T, 1, 3, 2, 1> gabg;
				detail::tvec_swizzle<T, 1, 3, 2, 2> gabb;
				detail::tvec_swizzle<T, 1, 3, 2, 3> gaba;
				detail::tvec_swizzle<T, 1, 3, 3, 0> gaar;
				detail::tvec_swizzle<T, 1, 3, 3, 1> gaag;
				detail::tvec_swizzle<T, 1, 3, 3, 2> gaab;
				detail::tvec_swizzle<T, 1, 3, 3, 3> gaaa;
				detail::tvec_swizzle<T, 2, 0, 0, 0> brrr;
				detail::tvec_swizzle<T, 2, 0, 0, 1> brrg;
				detail::tvec_swizzle<T, 2, 0, 0, 2> brrb;
				detail::tvec_swizzle<T, 2, 0, 0, 3> brra;
				detail::tvec_swizzle<T, 2, 0, 1, 0> brgr;
				detail::tvec_swizzle<T, 2, 0, 1, 1> brgg;
				detail::tvec_swizzle<T, 2, 0, 1, 2> brgb;
				detail::tvec_swizzle<T, 2, 0, 1, 3> brga;
				detail::tvec_swizzle<T, 2, 0, 2, 0> brbr;
				detail::tvec_swizzle<T, 2, 0, 2, 1> brbg;
				detail::tvec_swizzle<T, 2, 0, 2, 2> brbb;
				detail::tvec_swizzle<T, 2, 0, 2, 3> brba;
				detail::tvec_swizzle<T, 2, 0, 3, 0> brar;
				detail::tvec_swizzle<T, 2, 0, 3, 1> brag;
				detail::tvec_swizzle<T, 2, 0, 3, 2> brab;
				detail::tvec_swizzle<T, 2, 0, 3, 3> braa;
				detail::tvec_swizzle<T, 2, 1, 0, 0> bgrr;
				detail::tvec_swizzle<T, 2, 1, 0, 1> bgrg;
				detail::tvec_swizzle<T, 2, 1, 0, 2> bgrb;
				detail::tvec_swizzle<T, 2, 1, 0, 3> bgra;
				detail::tvec_swizzle<T, 2, 1, 1, 0> bggr;
				detail::tvec_swizzle<T, 2, 1, 1, 1> bggg;
				detail::tvec_swizzle<T, 2, 1, 1, 2> bggb;
				detail::tvec_swizzle<T, 2, 1, 1, 3> bgga;
				detail::tvec_swizzle<T, 2, 1, 2, 0> bgbr;
				detail::tvec_swizzle<T, 2, 1, 2, 1> bgbg;
				detail::tvec_swizzle<T, 2, 1, 2, 2> bgbb;
				detail::tvec_swizzle<T, 2, 1, 2, 3> bgba;
				detail::tvec_swizzle<T, 2, 1, 3, 0> bgar;
				detail::tvec_swizzle<T, 2, 1, 3, 1> bgag;
				detail::tvec_swizzle<T, 2, 1, 3, 2> bgab;
				detail::tvec_swizzle<T, 2, 1, 3, 3> bgaa;
				detail::tvec_swizzle<T, 2, 2, 0, 0> bbrr;
				detail::tvec_swizzle<T, 2, 2, 0, 1> bbrg;
				detail::tvec_swizzle<T, 2, 2, 0, 2> bbrb;
				detail::tvec_swizzle<T, 2, 2, 0, 3> bbra;
				detail::tvec_swizzle<T, 2, 2, 1, 0> bbgr;
				detail::tvec_swizzle<T, 2, 2, 1, 1> bbgg;
				detail::tvec_swizzle<T, 2, 2, 1, 2> bbgb;
				detail::tvec_swizzle<T, 2, 2, 1, 3> bbga;
				detail::tvec_swizzle<T, 2, 2, 2, 0> bbbr;
				detail::tvec_swizzle<T, 2, 2, 2, 1> bbbg;
				detail::tvec_swizzle<T, 2, 2, 2, 2> bbbb;
				detail::tvec_swizzle<T, 2, 2, 2, 3> bbba;
				detail::tvec_swizzle<T, 2, 2, 3, 0> bbar;
				detail::tvec_swizzle<T, 2, 2, 3, 1> bbag;
				detail::tvec_swizzle<T, 2, 2, 3, 2> bbab;
				detail::tvec_swizzle<T, 2, 2, 3, 3> bbaa;
				detail::tvec_swizzle<T, 2, 3, 0, 0> barr;
				detail::tvec_swizzle<T, 2, 3, 0, 1> barg;
				detail::tvec_swizzle<T, 2, 3, 0, 2> barb;
				detail::tvec_swizzle<T, 2, 3, 0, 3> bara;
				detail::tvec_swizzle<T, 2, 3, 1, 0> bagr;
				detail::tvec_swizzle<T, 2, 3, 1, 1> bagg;
				detail::tvec_swizzle<T, 2, 3, 1, 2> bagb;
				detail::tvec_swizzle<T, 2, 3, 1, 3> baga;
				detail::tvec_swizzle<T, 2, 3, 2, 0> babr;
				detail::tvec_swizzle<T, 2, 3, 2, 1> babg;
				detail::tvec_swizzle<T, 2, 3, 2, 2> babb;
				detail::tvec_swizzle<T, 2, 3, 2, 3> baba;
				detail::tvec_swizzle<T, 2, 3, 3, 0> baar;
				detail::tvec_swizzle<T, 2, 3, 3, 1> baag;
				detail::tvec_swizzle<T, 2, 3, 3, 2> baab;
				detail::tvec_swizzle<T, 2, 3, 3, 3> baaa;
				detail::tvec_swizzle<T, 3, 0, 0, 0> arrr;
				detail::tvec_swizzle<T, 3, 0, 0, 1> arrg;
				detail::tvec_swizzle<T, 3, 0, 0, 2> arrb;
				detail::tvec_swizzle<T, 3, 0, 0, 3> arra;
				detail::tvec_swizzle<T, 3, 0, 1, 0> argr;
				detail::tvec_swizzle<T, 3, 0, 1, 1> argg;
				detail::tvec_swizzle<T, 3, 0, 1, 2> argb;
				detail::tvec_swizzle<T, 3, 0, 1, 3> arga;
				detail::tvec_swizzle<T, 3, 0, 2, 0> arbr;
				detail::tvec_swizzle<T, 3, 0, 2, 1> arbg;
				detail::tvec_swizzle<T, 3, 0, 2, 2> arbb;
				detail::tvec_swizzle<T, 3, 0, 2, 3> arba;
				detail::tvec_swizzle<T, 3, 0, 3, 0> arar;
				detail::tvec_swizzle<T, 3, 0, 3, 1> arag;
				detail::tvec_swizzle<T, 3, 0, 3, 2> arab;
				detail::tvec_swizzle<T, 3, 0, 3, 3> araa;
				detail::tvec_swizzle<T, 3, 1, 0, 0> agrr;
				detail::tvec_swizzle<T, 3, 1, 0, 1> agrg;
				detail::tvec_swizzle<T, 3, 1, 0, 2> agrb;
				detail::tvec_swizzle<T, 3, 1, 0, 3> agra;
				detail::tvec_swizzle<T, 3, 1, 1, 0> aggr;
				detail::tvec_swizzle<T, 3, 1, 1, 1> aggg;
				detail::tvec_swizzle<T, 3, 1, 1, 2> aggb;
				detail::tvec_swizzle<T, 3, 1, 1, 3> agga;
				detail::tvec_swizzle<T, 3, 1, 2, 0> agbr;
				detail::tvec_swizzle<T, 3, 1, 2, 1> agbg;
				detail::tvec_swizzle<T, 3, 1, 2, 2> agbb;
				detail::tvec_swizzle<T, 3, 1, 2, 3> agba;
				detail::tvec_swizzle<T, 3, 1, 3, 0> agar;
				detail::tvec_swizzle<T, 3, 1, 3, 1> agag;
				detail::tvec_swizzle<T, 3, 1, 3, 2> agab;
				detail::tvec_swizzle<T, 3, 1, 3, 3> agaa;
				detail::tvec_swizzle<T, 3, 2, 0, 0> abrr;
				detail::tvec_swizzle<T, 3, 2, 0, 1> abrg;
				detail::tvec_swizzle<T, 3, 2, 0, 2> abrb;
				detail::tvec_swizzle<T, 3, 2, 0, 3> abra;
				detail::tvec_swizzle<T, 3, 2, 1, 0> abgr;
				detail::tvec_swizzle<T, 3, 2, 1, 1> abgg;
				detail::tvec_swizzle<T, 3, 2, 1, 2> abgb;
				detail::tvec_swizzle<T, 3, 2, 1, 3> abga;
				detail::tvec_swizzle<T, 3, 2, 2, 0> abbr;
				detail::tvec_swizzle<T, 3, 2, 2, 1> abbg;
				detail::tvec_swizzle<T, 3, 2, 2, 2> abbb;
				detail::tvec_swizzle<T, 3, 2, 2, 3> abba;
				detail::tvec_swizzle<T, 3, 2, 3, 0> abar;
				detail::tvec_swizzle<T, 3, 2, 3, 1> abag;
				detail::tvec_swizzle<T, 3, 2, 3, 2> abab;
				detail::tvec_swizzle<T, 3, 2, 3, 3> abaa;
				detail::tvec_swizzle<T, 3, 3, 0, 0> aarr;
				detail::tvec_swizzle<T, 3, 3, 0, 1> aarg;
				detail::tvec_swizzle<T, 3, 3, 0, 2> aarb;
				detail::tvec_swizzle<T, 3, 3, 0, 3> aara;
				detail::tvec_swizzle<T, 3, 3, 1, 0> aagr;
				detail::tvec_swizzle<T, 3, 3, 1, 1> aagg;
				detail::tvec_swizzle<T, 3, 3, 1, 2> aagb;
				detail::tvec_swizzle<T, 3, 3, 1, 3> aaga;
				detail::tvec_swizzle<T, 3, 3, 2, 0> aabr;
				detail::tvec_swizzle<T, 3, 3, 2, 1> aabg;
				detail::tvec_swizzle<T, 3, 3, 2, 2> aabb;
				detail::tvec_swizzle<T, 3, 3, 2, 3> aaba;
				detail::tvec_swizzle<T, 3, 3, 3, 0> aaar;
				detail::tvec_swizzle<T, 3, 3, 3, 1> aaag;
				detail::tvec_swizzle<T, 3, 3, 3, 2> aaab;
				detail::tvec_swizzle<T, 3, 3, 3, 3> aaaa;
				detail::tvec_swizzle<T, 0, 0> ss;
				detail::tvec_swizzle<T, 0, 1> st;
				detail::tvec_swizzle<T, 0, 2> sp;
				detail::tvec_swizzle<T, 0, 3> sq;
				detail::tvec_swizzle<T, 1, 0> ts;
				detail::tvec_swizzle<T, 1, 1> tt;
				detail::tvec_swizzle<T, 1, 2> tp;
				detail::tvec_swizzle<T, 1, 3> tq;
				detail::tvec_swizzle<T, 2, 0> ps;
				detail::tvec_swizzle<T, 2, 1> pt;
				detail::tvec_swizzle<T, 2, 2> pp;
				detail::tvec_swizzle<T, 2, 3> pq;
				detail::tvec_swizzle<T, 3, 0> qs;
				detail::tvec_swizzle<T, 3, 1> qt;
				detail::tvec_swizzle<T, 3, 2> qp;
				detail::tvec_swizzle<T, 3, 3> qq;
				detail::tvec_swizzle<T, 0, 0, 0> sss;
				detail::tvec_swizzle<T, 0, 0, 1> sst;
				detail::tvec_swizzle<T, 0, 0, 2> ssp;
				detail::tvec_swizzle<T, 0, 0, 3> ssq;
				detail::tvec_swizzle<T, 0, 1, 0> sts;
				detail::tvec_swizzle<T, 0, 1, 1> stt;
				detail::tvec_swizzle<T, 0, 1, 2> stp;
				detail::tvec_swizzle<T, 0, 1, 3> stq;
				detail::tvec_swizzle<T, 0, 2, 0> sps;
				detail::tvec_swizzle<T, 0, 2, 1> spt;
				detail::tvec_swizzle<T, 0, 2, 2> spp;
				detail::tvec_swizzle<T, 0, 2, 3> spq;
				detail::tvec_swizzle<T, 0, 3, 0> sqs;
				detail::tvec_swizzle<T, 0, 3, 1> sqt;
				detail::tvec_swizzle<T, 0, 3, 2> sqp;
				detail::tvec_swizzle<T, 0, 3, 3> sqq;
				detail::tvec_swizzle<T, 1, 0, 0> tss;
				detail::tvec_swizzle<T, 1, 0, 1> tst;
				detail::tvec_swizzle<T, 1, 0, 2> tsp;
				detail::tvec_swizzle<T, 1, 0, 3> tsq;
				detail::tvec_swizzle<T, 1, 1, 0> tts;
				detail::tvec_swizzle<T, 1, 1, 1> ttt;
				detail::tvec_swizzle<T, 1, 1, 2> ttp;
				detail::tvec_swizzle<T, 1, 1, 3> ttq;
				detail::tvec_swizzle<T, 1, 2, 0> tps;
				detail::tvec_swizzle<T, 1, 2, 1> tpt;
				detail::tvec_swizzle<T, 1, 2, 2> tpp;
				detail::tvec_swizzle<T, 1, 2, 3> tpq;
				detail::tvec_swizzle<T, 1, 3, 0> tqs;
				detail::tvec_swizzle<T, 1, 3, 1> tqt;
				detail::tvec_swizzle<T, 1, 3, 2> tqp;
				detail::tvec_swizzle<T, 1, 3, 3> tqq;
				detail::tvec_swizzle<T, 2, 0, 0> pss;
				detail::tvec_swizzle<T, 2, 0, 1> pst;
				detail::tvec_swizzle<T, 2, 0, 2> psp;
				detail::tvec_swizzle<T, 2, 0, 3> psq;
				detail::tvec_swizzle<T, 2, 1, 0> pts;
				detail::tvec_swizzle<T, 2, 1, 1> ptt;
				detail::tvec_swizzle<T, 2, 1, 2> ptp;
				detail::tvec_swizzle<T, 2, 1, 3> ptq;
				detail::tvec_swizzle<T, 2, 2, 0> pps;
				detail::tvec_swizzle<T, 2, 2, 1> ppt;
				detail::tvec_swizzle<T, 2, 2, 2> ppp;
				detail::tvec_swizzle<T, 2, 2, 3> ppq;
				detail::tvec_swizzle<T, 2, 3, 0> pqs;
				detail::tvec_swizzle<T, 2, 3, 1> pqt;
				detail::tvec_swizzle<T, 2, 3, 2> pqp;
				detail::tvec_swizzle<T, 2, 3, 3> pqq;
				detail::tvec_swizzle<T, 3, 0, 0> qss;
				detail::tvec_swizzle<T, 3, 0, 1> qst;
				detail::tvec_swizzle<T, 3, 0, 2> qsp;
				detail::tvec_swizzle<T, 3, 0, 3> qsq;
				detail::tvec_swizzle<T, 3, 1, 0> qts;
				detail::tvec_swizzle<T, 3, 1, 1> qtt;
				detail::tvec_swizzle<T, 3, 1, 2> qtp;
				detail::tvec_swizzle<T, 3, 1, 3> qtq;
				detail::tvec_swizzle<T, 3, 2, 0> qps;
				detail::tvec_swizzle<T, 3, 2, 1> qpt;
				detail::tvec_swizzle<T, 3, 2, 2> qpp;
				detail::tvec_swizzle<T, 3, 2, 3> qpq;
				detail::tvec_swizzle<T, 3, 3, 0> qqs;
				detail::tvec_swizzle<T, 3, 3, 1> qqt;
				detail::tvec_swizzle<T, 3, 3, 2> qqp;
				detail::tvec_swizzle<T, 3, 3, 3> qqq;
				detail::tvec_swizzle<T, 0, 0, 0, 0> ssss;
				detail::tvec_swizzle<T, 0, 0, 0, 1> ssst;
				detail::tvec_swizzle<T, 0, 0, 0, 2> sssp;
				detail::tvec_swizzle<T, 0, 0, 0, 3> sssq;
				detail::tvec_swizzle<T, 0, 0, 1, 0> ssts;
				detail::tvec_swizzle<T, 0, 0, 1, 1> sstt;
				detail::tvec_swizzle<T, 0, 0, 1, 2> sstp;
				detail::tvec_swizzle<T, 0, 0, 1, 3> sstq;
				detail::tvec_swizzle<T, 0, 0, 2, 0> ssps;
				detail::tvec_swizzle<T, 0, 0, 2, 1> sspt;
				detail::tvec_swizzle<T, 0, 0, 2, 2> sspp;
				detail::tvec_swizzle<T, 0, 0, 2, 3> sspq;
				detail::tvec_swizzle<T, 0, 0, 3, 0> ssqs;
				detail::tvec_swizzle<T, 0, 0, 3, 1> ssqt;
				detail::tvec_swizzle<T, 0, 0, 3, 2> ssqp;
				detail::tvec_swizzle<T, 0, 0, 3, 3> ssqq;
				detail::tvec_swizzle<T, 0, 1, 0, 0> stss;
				detail::tvec_swizzle<T, 0, 1, 0, 1> stst;
				detail::tvec_swizzle<T, 0, 1, 0, 2> stsp;
				detail::tvec_swizzle<T, 0, 1, 0, 3> stsq;
				detail::tvec_swizzle<T, 0, 1, 1, 0> stts;
				detail::tvec_swizzle<T, 0, 1, 1, 1> sttt;
				detail::tvec_swizzle<T, 0, 1, 1, 2> sttp;
				detail::tvec_swizzle<T, 0, 1, 1, 3> sttq;
				detail::tvec_swizzle<T, 0, 1, 2, 0> stps;
				detail::tvec_swizzle<T, 0, 1, 2, 1> stpt;
				detail::tvec_swizzle<T, 0, 1, 2, 2> stpp;
				detail::tvec_swizzle<T, 0, 1, 2, 3> stpq;
				detail::tvec_swizzle<T, 0, 1, 3, 0> stqs;
				detail::tvec_swizzle<T, 0, 1, 3, 1> stqt;
				detail::tvec_swizzle<T, 0, 1, 3, 2> stqp;
				detail::tvec_swizzle<T, 0, 1, 3, 3> stqq;
				detail::tvec_swizzle<T, 0, 2, 0, 0> spss;
				detail::tvec_swizzle<T, 0, 2, 0, 1> spst;
				detail::tvec_swizzle<T, 0, 2, 0, 2> spsp;
				detail::tvec_swizzle<T, 0, 2, 0, 3> spsq;
				detail::tvec_swizzle<T, 0, 2, 1, 0> spts;
				detail::tvec_swizzle<T, 0, 2, 1, 1> sptt;
				detail::tvec_swizzle<T, 0, 2, 1, 2> sptp;
				detail::tvec_swizzle<T, 0, 2, 1, 3> sptq;
				detail::tvec_swizzle<T, 0, 2, 2, 0> spps;
				detail::tvec_swizzle<T, 0, 2, 2, 1> sppt;
				detail::tvec_swizzle<T, 0, 2, 2, 2> sppp;
				detail::tvec_swizzle<T, 0, 2, 2, 3> sppq;
				detail::tvec_swizzle<T, 0, 2, 3, 0> spqs;
				detail::tvec_swizzle<T, 0, 2, 3, 1> spqt;
				detail::tvec_swizzle<T, 0, 2, 3, 2> spqp;
				detail::tvec_swizzle<T, 0, 2, 3, 3> spqq;
				detail::tvec_swizzle<T, 0, 3, 0, 0> sqss;
				detail::tvec_swizzle<T, 0, 3, 0, 1> sqst;
				detail::tvec_swizzle<T, 0, 3, 0, 2> sqsp;
				detail::tvec_swizzle<T, 0, 3, 0, 3> sqsq;
				detail::tvec_swizzle<T, 0, 3, 1, 0> sqts;
				detail::tvec_swizzle<T, 0, 3, 1, 1> sqtt;
				detail::tvec_swizzle<T, 0, 3, 1, 2> sqtp;
				detail::tvec_swizzle<T, 0, 3, 1, 3> sqtq;
				detail::tvec_swizzle<T, 0, 3, 2, 0> sqps;
				detail::tvec_swizzle<T, 0, 3, 2, 1> sqpt;
				detail::tvec_swizzle<T, 0, 3, 2, 2> sqpp;
				detail::tvec_swizzle<T, 0, 3, 2, 3> sqpq;
				detail::tvec_swizzle<T, 0, 3, 3, 0> sqqs;
				detail::tvec_swizzle<T, 0, 3, 3, 1> sqqt;
				detail::tvec_swizzle<T, 0, 3, 3, 2> sqqp;
				detail::tvec_swizzle<T, 0, 3, 3, 3> sqqq;
				detail::tvec_swizzle<T, 1, 0, 0, 0> tsss;
				detail::tvec_swizzle<T, 1, 0, 0, 1> tsst;
				detail::tvec_swizzle<T, 1, 0, 0, 2> tssp;
				detail::tvec_swizzle<T, 1, 0, 0, 3> tssq;
				detail::tvec_swizzle<T, 1, 0, 1, 0> tsts;
				detail::tvec_swizzle<T, 1, 0, 1, 1> tstt;
				detail::tvec_swizzle<T, 1, 0, 1, 2> tstp;
				detail::tvec_swizzle<T, 1, 0, 1, 3> tstq;
				detail::tvec_swizzle<T, 1, 0, 2, 0> tsps;
				detail::tvec_swizzle<T, 1, 0, 2, 1> tspt;
				detail::tvec_swizzle<T, 1, 0, 2, 2> tspp;
				detail::tvec_swizzle<T, 1, 0, 2, 3> tspq;
				detail::tvec_swizzle<T, 1, 0, 3, 0> tsqs;
				detail::tvec_swizzle<T, 1, 0, 3, 1> tsqt;
				detail::tvec_swizzle<T, 1, 0, 3, 2> tsqp;
				detail::tvec_swizzle<T, 1, 0, 3, 3> tsqq;
				detail::tvec_swizzle<T, 1, 1, 0, 0> ttss;
				detail::tvec_swizzle<T, 1, 1, 0, 1> ttst;
				detail::tvec_swizzle<T, 1, 1, 0, 2> ttsp;
				detail::tvec_swizzle<T, 1, 1, 0, 3> ttsq;
				detail::tvec_swizzle<T, 1, 1, 1, 0> ttts;
				detail::tvec_swizzle<T, 1, 1, 1, 1> tttt;
				detail::tvec_swizzle<T, 1, 1, 1, 2> tttp;
				detail::tvec_swizzle<T, 1, 1, 1, 3> tttq;
				detail::tvec_swizzle<T, 1, 1, 2, 0> ttps;
				detail::tvec_swizzle<T, 1, 1, 2, 1> ttpt;
				detail::tvec_swizzle<T, 1, 1, 2, 2> ttpp;
				detail::tvec_swizzle<T, 1, 1, 2, 3> ttpq;
				detail::tvec_swizzle<T, 1, 1, 3, 0> ttqs;
				detail::tvec_swizzle<T, 1, 1, 3, 1> ttqt;
				detail::tvec_swizzle<T, 1, 1, 3, 2> ttqp;
				detail::tvec_swizzle<T, 1, 1, 3, 3> ttqq;
				detail::tvec_swizzle<T, 1, 2, 0, 0> tpss;
				detail::tvec_swizzle<T, 1, 2, 0, 1> tpst;
				detail::tvec_swizzle<T, 1, 2, 0, 2> tpsp;
				detail::tvec_swizzle<T, 1, 2, 0, 3> tpsq;
				detail::tvec_swizzle<T, 1, 2, 1, 0> tpts;
				detail::tvec_swizzle<T, 1, 2, 1, 1> tptt;
				detail::tvec_swizzle<T, 1, 2, 1, 2> tptp;
				detail::tvec_swizzle<T, 1, 2, 1, 3> tptq;
				detail::tvec_swizzle<T, 1, 2, 2, 0> tpps;
				detail::tvec_swizzle<T, 1, 2, 2, 1> tppt;
				detail::tvec_swizzle<T, 1, 2, 2, 2> tppp;
				detail::tvec_swizzle<T, 1, 2, 2, 3> tppq;
				detail::tvec_swizzle<T, 1, 2, 3, 0> tpqs;
				detail::tvec_swizzle<T, 1, 2, 3, 1> tpqt;
				detail::tvec_swizzle<T, 1, 2, 3, 2> tpqp;
				detail::tvec_swizzle<T, 1, 2, 3, 3> tpqq;
				detail::tvec_swizzle<T, 1, 3, 0, 0> tqss;
				detail::tvec_swizzle<T, 1, 3, 0, 1> tqst;
				detail::tvec_swizzle<T, 1, 3, 0, 2> tqsp;
				detail::tvec_swizzle<T, 1, 3, 0, 3> tqsq;
				detail::tvec_swizzle<T, 1, 3, 1, 0> tqts;
				detail::tvec_swizzle<T, 1, 3, 1, 1> tqtt;
				detail::tvec_swizzle<T, 1, 3, 1, 2> tqtp;
				detail::tvec_swizzle<T, 1, 3, 1, 3> tqtq;
				detail::tvec_swizzle<T, 1, 3, 2, 0> tqps;
				detail::tvec_swizzle<T, 1, 3, 2, 1> tqpt;
				detail::tvec_swizzle<T, 1, 3, 2, 2> tqpp;
				detail::tvec_swizzle<T, 1, 3, 2, 3> tqpq;
				detail::tvec_swizzle<T, 1, 3, 3, 0> tqqs;
				detail::tvec_swizzle<T, 1, 3, 3, 1> tqqt;
				detail::tvec_swizzle<T, 1, 3, 3, 2> tqqp;
				detail::tvec_swizzle<T, 1, 3, 3, 3> tqqq;
				detail::tvec_swizzle<T, 2, 0, 0, 0> psss;
				detail::tvec_swizzle<T, 2, 0, 0, 1> psst;
				detail::tvec_swizzle<T, 2, 0, 0, 2> pssp;
				detail::tvec_swizzle<T, 2, 0, 0, 3> pssq;
				detail::tvec_swizzle<T, 2, 0, 1, 0> psts;
				detail::tvec_swizzle<T, 2, 0, 1, 1> pstt;
				detail::tvec_swizzle<T, 2, 0, 1, 2> pstp;
				detail::tvec_swizzle<T, 2, 0, 1, 3> pstq;
				detail::tvec_swizzle<T, 2, 0, 2, 0> psps;
				detail::tvec_swizzle<T, 2, 0, 2, 1> pspt;
				detail::tvec_swizzle<T, 2, 0, 2, 2> pspp;
				detail::tvec_swizzle<T, 2, 0, 2, 3> pspq;
				detail::tvec_swizzle<T, 2, 0, 3, 0> psqs;
				detail::tvec_swizzle<T, 2, 0, 3, 1> psqt;
				detail::tvec_swizzle<T, 2, 0, 3, 2> psqp;
				detail::tvec_swizzle<T, 2, 0, 3, 3> psqq;
				detail::tvec_swizzle<T, 2, 1, 0, 0> ptss;
				detail::tvec_swizzle<T, 2, 1, 0, 1> ptst;
				detail::tvec_swizzle<T, 2, 1, 0, 2> ptsp;
				detail::tvec_swizzle<T, 2, 1, 0, 3> ptsq;
				detail::tvec_swizzle<T, 2, 1, 1, 0> ptts;
				detail::tvec_swizzle<T, 2, 1, 1, 1> pttt;
				detail::tvec_swizzle<T, 2, 1, 1, 2> pttp;
				detail::tvec_swizzle<T, 2, 1, 1, 3> pttq;
				detail::tvec_swizzle<T, 2, 1, 2, 0> ptps;
				detail::tvec_swizzle<T, 2, 1, 2, 1> ptpt;
				detail::tvec_swizzle<T, 2, 1, 2, 2> ptpp;
				detail::tvec_swizzle<T, 2, 1, 2, 3> ptpq;
				detail::tvec_swizzle<T, 2, 1, 3, 0> ptqs;
				detail::tvec_swizzle<T, 2, 1, 3, 1> ptqt;
				detail::tvec_swizzle<T, 2, 1, 3, 2> ptqp;
				detail::tvec_swizzle<T, 2, 1, 3, 3> ptqq;
				detail::tvec_swizzle<T, 2, 2, 0, 0> ppss;
				detail::tvec_swizzle<T, 2, 2, 0, 1> ppst;
				detail::tvec_swizzle<T, 2, 2, 0, 2> ppsp;
				detail::tvec_swizzle<T, 2, 2, 0, 3> ppsq;
				detail::tvec_swizzle<T, 2, 2, 1, 0> ppts;
				detail::tvec_swizzle<T, 2, 2, 1, 1> pptt;
				detail::tvec_swizzle<T, 2, 2, 1, 2> pptp;
				detail::tvec_swizzle<T, 2, 2, 1, 3> pptq;
				detail::tvec_swizzle<T, 2, 2, 2, 0> ppps;
				detail::tvec_swizzle<T, 2, 2, 2, 1> pppt;
				detail::tvec_swizzle<T, 2, 2, 2, 2> pppp;
				detail::tvec_swizzle<T, 2, 2, 2, 3> pppq;
				detail::tvec_swizzle<T, 2, 2, 3, 0> ppqs;
				detail::tvec_swizzle<T, 2, 2, 3, 1> ppqt;
				detail::tvec_swizzle<T, 2, 2, 3, 2> ppqp;
				detail::tvec_swizzle<T, 2, 2, 3, 3> ppqq;
				detail::tvec_swizzle<T, 2, 3, 0, 0> pqss;
				detail::tvec_swizzle<T, 2, 3, 0, 1> pqst;
				detail::tvec_swizzle<T, 2, 3, 0, 2> pqsp;
				detail::tvec_swizzle<T, 2, 3, 0, 3> pqsq;
				detail::tvec_swizzle<T, 2, 3, 1, 0> pqts;
				detail::tvec_swizzle<T, 2, 3, 1, 1> pqtt;
				detail::tvec_swizzle<T, 2, 3, 1, 2> pqtp;
				detail::tvec_swizzle<T, 2, 3, 1, 3> pqtq;
				detail::tvec_swizzle<T, 2, 3, 2, 0> pqps;
				detail::tvec_swizzle<T, 2, 3, 2, 1> pqpt;
				detail::tvec_swizzle<T, 2, 3, 2, 2> pqpp;
				detail::tvec_swizzle<T, 2, 3, 2, 3> pqpq;
				detail::tvec_swizzle<T, 2, 3, 3, 0> pqqs;
				detail::tvec_swizzle<T, 2, 3, 3, 1> pqqt;
				detail::tvec_swizzle<T, 2, 3, 3, 2> pqqp;
				detail::tvec_swizzle<T, 2, 3, 3, 3> pqqq;
				detail::tvec_swizzle<T, 3, 0, 0, 0> qsss;
				detail::tvec_swizzle<T, 3, 0, 0, 1> qsst;
				detail::tvec_swizzle<T, 3, 0, 0, 2> qssp;
				detail::tvec_swizzle<T, 3, 0, 0, 3> qssq;
				detail::tvec_swizzle<T, 3, 0, 1, 0> qsts;
				detail::tvec_swizzle<T, 3, 0, 1, 1> qstt;
				detail::tvec_swizzle<T, 3, 0, 1, 2> qstp;
				detail::tvec_swizzle<T, 3, 0, 1, 3> qstq;
				detail::tvec_swizzle<T, 3, 0, 2, 0> qsps;
				detail::tvec_swizzle<T, 3, 0, 2, 1> qspt;
				detail::tvec_swizzle<T, 3, 0, 2, 2> qspp;
				detail::tvec_swizzle<T, 3, 0, 2, 3> qspq;
				detail::tvec_swizzle<T, 3, 0, 3, 0> qsqs;
				detail::tvec_swizzle<T, 3, 0, 3, 1> qsqt;
				detail::tvec_swizzle<T, 3, 0, 3, 2> qsqp;
				detail::tvec_swizzle<T, 3, 0, 3, 3> qsqq;
				detail::tvec_swizzle<T, 3, 1, 0, 0> qtss;
				detail::tvec_swizzle<T, 3, 1, 0, 1> qtst;
				detail::tvec_swizzle<T, 3, 1, 0, 2> qtsp;
				detail::tvec_swizzle<T, 3, 1, 0, 3> qtsq;
				detail::tvec_swizzle<T, 3, 1, 1, 0> qtts;
				detail::tvec_swizzle<T, 3, 1, 1, 1> qttt;
				detail::tvec_swizzle<T, 3, 1, 1, 2> qttp;
				detail::tvec_swizzle<T, 3, 1, 1, 3> qttq;
				detail::tvec_swizzle<T, 3, 1, 2, 0> qtps;
				detail::tvec_swizzle<T, 3, 1, 2, 1> qtpt;
				detail::tvec_swizzle<T, 3, 1, 2, 2> qtpp;
				detail::tvec_swizzle<T, 3, 1, 2, 3> qtpq;
				detail::tvec_swizzle<T, 3, 1, 3, 0> qtqs;
				detail::tvec_swizzle<T, 3, 1, 3, 1> qtqt;
				detail::tvec_swizzle<T, 3, 1, 3, 2> qtqp;
				detail::tvec_swizzle<T, 3, 1, 3, 3> qtqq;
				detail::tvec_swizzle<T, 3, 2, 0, 0> qpss;
				detail::tvec_swizzle<T, 3, 2, 0, 1> qpst;
				detail::tvec_swizzle<T, 3, 2, 0, 2> qpsp;
				detail::tvec_swizzle<T, 3, 2, 0, 3> qpsq;
				detail::tvec_swizzle<T, 3, 2, 1, 0> qpts;
				detail::tvec_swizzle<T, 3, 2, 1, 1> qptt;
				detail::tvec_swizzle<T, 3, 2, 1, 2> qptp;
				detail::tvec_swizzle<T, 3, 2, 1, 3> qptq;
				detail::tvec_swizzle<T, 3, 2, 2, 0> qpps;
				detail::tvec_swizzle<T, 3, 2, 2, 1> qppt;
				detail::tvec_swizzle<T, 3, 2, 2, 2> qppp;
				detail::tvec_swizzle<T, 3, 2, 2, 3> qppq;
				detail::tvec_swizzle<T, 3, 2, 3, 0> qpqs;
				detail::tvec_swizzle<T, 3, 2, 3, 1> qpqt;
				detail::tvec_swizzle<T, 3, 2, 3, 2> qpqp;
				detail::tvec_swizzle<T, 3, 2, 3, 3> qpqq;
				detail::tvec_swizzle<T, 3, 3, 0, 0> qqss;
				detail::tvec_swizzle<T, 3, 3, 0, 1> qqst;
				detail::tvec_swizzle<T, 3, 3, 0, 2> qqsp;
				detail::tvec_swizzle<T, 3, 3, 0, 3> qqsq;
				detail::tvec_swizzle<T, 3, 3, 1, 0> qqts;
				detail::tvec_swizzle<T, 3, 3, 1, 1> qqtt;
				detail::tvec_swizzle<T, 3, 3, 1, 2> qqtp;
				detail::tvec_swizzle<T, 3, 3, 1, 3> qqtq;
				detail::tvec_swizzle<T, 3, 3, 2, 0> qqps;
				detail::tvec_swizzle<T, 3, 3, 2, 1> qqpt;
				detail::tvec_swizzle<T, 3, 3, 2, 2> qqpp;
				detail::tvec_swizzle<T, 3, 3, 2, 3> qqpq;
				detail::tvec_swizzle<T, 3, 3, 3, 0> qqqs;
				detail::tvec_swizzle<T, 3, 3, 3, 1> qqqt;
				detail::tvec_swizzle<T, 3, 3, 3, 2> qqqp;
				detail::tvec_swizzle<T, 3, 3, 3, 3> qqqq;
				detail::tvec_swizzle<T, 0, 0> uu;
				detail::tvec_swizzle<T, 0, 1> uv;
				detail::tvec_swizzle<T, 0, 2> ui;
				detail::tvec_swizzle<T, 0, 3> uj;
				detail::tvec_swizzle<T, 1, 0> vu;
				detail::tvec_swizzle<T, 1, 1> vv;
				detail::tvec_swizzle<T, 1, 2> vi;
				detail::tvec_swizzle<T, 1, 3> vj;
				detail::tvec_swizzle<T, 2, 0> iu;
				detail::tvec_swizzle<T, 2, 1> iv;
				detail::tvec_swizzle<T, 2, 2> ii;
				detail::tvec_swizzle<T, 2, 3> ij;
				detail::tvec_swizzle<T, 3, 0> ju;
				detail::tvec_swizzle<T, 3, 1> jv;
				detail::tvec_swizzle<T, 3, 2> ji;
				detail::tvec_swizzle<T, 3, 3> jj;
				detail::tvec_swizzle<T, 0, 0, 0> uuu;
				detail::tvec_swizzle<T, 0, 0, 1> uuv;
				detail::tvec_swizzle<T, 0, 0, 2> uui;
				detail::tvec_swizzle<T, 0, 0, 3> uuj;
				detail::tvec_swizzle<T, 0, 1, 0> uvu;
				detail::tvec_swizzle<T, 0, 1, 1> uvv;
				detail::tvec_swizzle<T, 0, 1, 2> uvi;
				detail::tvec_swizzle<T, 0, 1, 3> uvj;
				detail::tvec_swizzle<T, 0, 2, 0> uiu;
				detail::tvec_swizzle<T, 0, 2, 1> uiv;
				detail::tvec_swizzle<T, 0, 2, 2> uii;
				detail::tvec_swizzle<T, 0, 2, 3> uij;
				detail::tvec_swizzle<T, 0, 3, 0> uju;
				detail::tvec_swizzle<T, 0, 3, 1> ujv;
				detail::tvec_swizzle<T, 0, 3, 2> uji;
				detail::tvec_swizzle<T, 0, 3, 3> ujj;
				detail::tvec_swizzle<T, 1, 0, 0> vuu;
				detail::tvec_swizzle<T, 1, 0, 1> vuv;
				detail::tvec_swizzle<T, 1, 0, 2> vui;
				detail::tvec_swizzle<T, 1, 0, 3> vuj;
				detail::tvec_swizzle<T, 1, 1, 0> vvu;
				detail::tvec_swizzle<T, 1, 1, 1> vvv;
				detail::tvec_swizzle<T, 1, 1, 2> vvi;
				detail::tvec_swizzle<T, 1, 1, 3> vvj;
				detail::tvec_swizzle<T, 1, 2, 0> viu;
				detail::tvec_swizzle<T, 1, 2, 1> viv;
				detail::tvec_swizzle<T, 1, 2, 2> vii;
				detail::tvec_swizzle<T, 1, 2, 3> vij;
				detail::tvec_swizzle<T, 1, 3, 0> vju;
				detail::tvec_swizzle<T, 1, 3, 1> vjv;
				detail::tvec_swizzle<T, 1, 3, 2> vji;
				detail::tvec_swizzle<T, 1, 3, 3> vjj;
				detail::tvec_swizzle<T, 2, 0, 0> iuu;
				detail::tvec_swizzle<T, 2, 0, 1> iuv;
				detail::tvec_swizzle<T, 2, 0, 2> iui;
				detail::tvec_swizzle<T, 2, 0, 3> iuj;
				detail::tvec_swizzle<T, 2, 1, 0> ivu;
				detail::tvec_swizzle<T, 2, 1, 1> ivv;
				detail::tvec_swizzle<T, 2, 1, 2> ivi;
				detail::tvec_swizzle<T, 2, 1, 3> ivj;
				detail::tvec_swizzle<T, 2, 2, 0> iiu;
				detail::tvec_swizzle<T, 2, 2, 1> iiv;
				detail::tvec_swizzle<T, 2, 2, 2> iii;
				detail::tvec_swizzle<T, 2, 2, 3> iij;
				detail::tvec_swizzle<T, 2, 3, 0> iju;
				detail::tvec_swizzle<T, 2, 3, 1> ijv;
				detail::tvec_swizzle<T, 2, 3, 2> iji;
				detail::tvec_swizzle<T, 2, 3, 3> ijj;
				detail::tvec_swizzle<T, 3, 0, 0> juu;
				detail::tvec_swizzle<T, 3, 0, 1> juv;
				detail::tvec_swizzle<T, 3, 0, 2> jui;
				detail::tvec_swizzle<T, 3, 0, 3> juj;
				detail::tvec_swizzle<T, 3, 1, 0> jvu;
				detail::tvec_swizzle<T, 3, 1, 1> jvv;
				detail::tvec_swizzle<T, 3, 1, 2> jvi;
				detail::tvec_swizzle<T, 3, 1, 3> jvj;
				detail::tvec_swizzle<T, 3, 2, 0> jiu;
				detail::tvec_swizzle<T, 3, 2, 1> jiv;
				detail::tvec_swizzle<T, 3, 2, 2> jii;
				detail::tvec_swizzle<T, 3, 2, 3> jij;
				detail::tvec_swizzle<T, 3, 3, 0> jju;
				detail::tvec_swizzle<T, 3, 3, 1> jjv;
				detail::tvec_swizzle<T, 3, 3, 2> jji;
				detail::tvec_swizzle<T, 3, 3, 3> jjj;
				detail::tvec_swizzle<T, 0, 0, 0, 0> uuuu;
				detail::tvec_swizzle<T, 0, 0, 0, 1> uuuv;
				detail::tvec_swizzle<T, 0, 0, 0, 2> uuui;
				detail::tvec_swizzle<T, 0, 0, 0, 3> uuuj;
				detail::tvec_swizzle<T, 0, 0, 1, 0> uuvu;
				detail::tvec_swizzle<T, 0, 0, 1, 1> uuvv;
				detail::tvec_swizzle<T, 0, 0, 1, 2> uuvi;
				detail::tvec_swizzle<T, 0, 0, 1, 3> uuvj;
				detail::tvec_swizzle<T, 0, 0, 2, 0> uuiu;
				detail::tvec_swizzle<T, 0, 0, 2, 1> uuiv;
				detail::tvec_swizzle<T, 0, 0, 2, 2> uuii;
				detail::tvec_swizzle<T, 0, 0, 2, 3> uuij;
				detail::tvec_swizzle<T, 0, 0, 3, 0> uuju;
				detail::tvec_swizzle<T, 0, 0, 3, 1> uujv;
				detail::tvec_swizzle<T, 0, 0, 3, 2> uuji;
				detail::tvec_swizzle<T, 0, 0, 3, 3> uujj;
				detail::tvec_swizzle<T, 0, 1, 0, 0> uvuu;
				detail::tvec_swizzle<T, 0, 1, 0, 1> uvuv;
				detail::tvec_swizzle<T, 0, 1, 0, 2> uvui;
				detail::tvec_swizzle<T, 0, 1, 0, 3> uvuj;
				detail::tvec_swizzle<T, 0, 1, 1, 0> uvvu;
				detail::tvec_swizzle<T, 0, 1, 1, 1> uvvv;
				detail::tvec_swizzle<T, 0, 1, 1, 2> uvvi;
				detail::tvec_swizzle<T, 0, 1, 1, 3> uvvj;
				detail::tvec_swizzle<T, 0, 1, 2, 0> uviu;
				detail::tvec_swizzle<T, 0, 1, 2, 1> uviv;
				detail::tvec_swizzle<T, 0, 1, 2, 2> uvii;
				detail::tvec_swizzle<T, 0, 1, 2, 3> uvij;
				detail::tvec_swizzle<T, 0, 1, 3, 0> uvju;
				detail::tvec_swizzle<T, 0, 1, 3, 1> uvjv;
				detail::tvec_swizzle<T, 0, 1, 3, 2> uvji;
				detail::tvec_swizzle<T, 0, 1, 3, 3> uvjj;
				detail::tvec_swizzle<T, 0, 2, 0, 0> uiuu;
				detail::tvec_swizzle<T, 0, 2, 0, 1> uiuv;
				detail::tvec_swizzle<T, 0, 2, 0, 2> uiui;
				detail::tvec_swizzle<T, 0, 2, 0, 3> uiuj;
				detail::tvec_swizzle<T, 0, 2, 1, 0> uivu;
				detail::tvec_swizzle<T, 0, 2, 1, 1> uivv;
				detail::tvec_swizzle<T, 0, 2, 1, 2> uivi;
				detail::tvec_swizzle<T, 0, 2, 1, 3> uivj;
				detail::tvec_swizzle<T, 0, 2, 2, 0> uiiu;
				detail::tvec_swizzle<T, 0, 2, 2, 1> uiiv;
				detail::tvec_swizzle<T, 0, 2, 2, 2> uiii;
				detail::tvec_swizzle<T, 0, 2, 2, 3> uiij;
				detail::tvec_swizzle<T, 0, 2, 3, 0> uiju;
				detail::tvec_swizzle<T, 0, 2, 3, 1> uijv;
				detail::tvec_swizzle<T, 0, 2, 3, 2> uiji;
				detail::tvec_swizzle<T, 0, 2, 3, 3> uijj;
				detail::tvec_swizzle<T, 0, 3, 0, 0> ujuu;
				detail::tvec_swizzle<T, 0, 3, 0, 1> ujuv;
				detail::tvec_swizzle<T, 0, 3, 0, 2> ujui;
				detail::tvec_swizzle<T, 0, 3, 0, 3> ujuj;
				detail::tvec_swizzle<T, 0, 3, 1, 0> ujvu;
				detail::tvec_swizzle<T, 0, 3, 1, 1> ujvv;
				detail::tvec_swizzle<T, 0, 3, 1, 2> ujvi;
				detail::tvec_swizzle<T, 0, 3, 1, 3> ujvj;
				detail::tvec_swizzle<T, 0, 3, 2, 0> ujiu;
				detail::tvec_swizzle<T, 0, 3, 2, 1> ujiv;
				detail::tvec_swizzle<T, 0, 3, 2, 2> ujii;
				detail::tvec_swizzle<T, 0, 3, 2, 3> ujij;
				detail::tvec_swizzle<T, 0, 3, 3, 0> ujju;
				detail::tvec_swizzle<T, 0, 3, 3, 1> ujjv;
				detail::tvec_swizzle<T, 0, 3, 3, 2> ujji;
				detail::tvec_swizzle<T, 0, 3, 3, 3> ujjj;
				detail::tvec_swizzle<T, 1, 0, 0, 0> vuuu;
				detail::tvec_swizzle<T, 1, 0, 0, 1> vuuv;
				detail::tvec_swizzle<T, 1, 0, 0, 2> vuui;
				detail::tvec_swizzle<T, 1, 0, 0, 3> vuuj;
				detail::tvec_swizzle<T, 1, 0, 1, 0> vuvu;
				detail::tvec_swizzle<T, 1, 0, 1, 1> vuvv;
				detail::tvec_swizzle<T, 1, 0, 1, 2> vuvi;
				detail::tvec_swizzle<T, 1, 0, 1, 3> vuvj;
				detail::tvec_swizzle<T, 1, 0, 2, 0> vuiu;
				detail::tvec_swizzle<T, 1, 0, 2, 1> vuiv;
				detail::tvec_swizzle<T, 1, 0, 2, 2> vuii;
				detail::tvec_swizzle<T, 1, 0, 2, 3> vuij;
				detail::tvec_swizzle<T, 1, 0, 3, 0> vuju;
				detail::tvec_swizzle<T, 1, 0, 3, 1> vujv;
				detail::tvec_swizzle<T, 1, 0, 3, 2> vuji;
				detail::tvec_swizzle<T, 1, 0, 3, 3> vujj;
				detail::tvec_swizzle<T, 1, 1, 0, 0> vvuu;
				detail::tvec_swizzle<T, 1, 1, 0, 1> vvuv;
				detail::tvec_swizzle<T, 1, 1, 0, 2> vvui;
				detail::tvec_swizzle<T, 1, 1, 0, 3> vvuj;
				detail::tvec_swizzle<T, 1, 1, 1, 0> vvvu;
				detail::tvec_swizzle<T, 1, 1, 1, 1> vvvv;
				detail::tvec_swizzle<T, 1, 1, 1, 2> vvvi;
				detail::tvec_swizzle<T, 1, 1, 1, 3> vvvj;
				detail::tvec_swizzle<T, 1, 1, 2, 0> vviu;
				detail::tvec_swizzle<T, 1, 1, 2, 1> vviv;
				detail::tvec_swizzle<T, 1, 1, 2, 2> vvii;
				detail::tvec_swizzle<T, 1, 1, 2, 3> vvij;
				detail::tvec_swizzle<T, 1, 1, 3, 0> vvju;
				detail::tvec_swizzle<T, 1, 1, 3, 1> vvjv;
				detail::tvec_swizzle<T, 1, 1, 3, 2> vvji;
				detail::tvec_swizzle<T, 1, 1, 3, 3> vvjj;
				detail::tvec_swizzle<T, 1, 2, 0, 0> viuu;
				detail::tvec_swizzle<T, 1, 2, 0, 1> viuv;
				detail::tvec_swizzle<T, 1, 2, 0, 2> viui;
				detail::tvec_swizzle<T, 1, 2, 0, 3> viuj;
				detail::tvec_swizzle<T, 1, 2, 1, 0> vivu;
				detail::tvec_swizzle<T, 1, 2, 1, 1> vivv;
				detail::tvec_swizzle<T, 1, 2, 1, 2> vivi;
				detail::tvec_swizzle<T, 1, 2, 1, 3> vivj;
				detail::tvec_swizzle<T, 1, 2, 2, 0> viiu;
				detail::tvec_swizzle<T, 1, 2, 2, 1> viiv;
				detail::tvec_swizzle<T, 1, 2, 2, 2> viii;
				detail::tvec_swizzle<T, 1, 2, 2, 3> viij;
				detail::tvec_swizzle<T, 1, 2, 3, 0> viju;
				detail::tvec_swizzle<T, 1, 2, 3, 1> vijv;
				detail::tvec_swizzle<T, 1, 2, 3, 2> viji;
				detail::tvec_swizzle<T, 1, 2, 3, 3> vijj;
				detail::tvec_swizzle<T, 1, 3, 0, 0> vjuu;
				detail::tvec_swizzle<T, 1, 3, 0, 1> vjuv;
				detail::tvec_swizzle<T, 1, 3, 0, 2> vjui;
				detail::tvec_swizzle<T, 1, 3, 0, 3> vjuj;
				detail::tvec_swizzle<T, 1, 3, 1, 0> vjvu;
				detail::tvec_swizzle<T, 1, 3, 1, 1> vjvv;
				detail::tvec_swizzle<T, 1, 3, 1, 2> vjvi;
				detail::tvec_swizzle<T, 1, 3, 1, 3> vjvj;
				detail::tvec_swizzle<T, 1, 3, 2, 0> vjiu;
				detail::tvec_swizzle<T, 1, 3, 2, 1> vjiv;
				detail::tvec_swizzle<T, 1, 3, 2, 2> vjii;
				detail::tvec_swizzle<T, 1, 3, 2, 3> vjij;
				detail::tvec_swizzle<T, 1, 3, 3, 0> vjju;
				detail::tvec_swizzle<T, 1, 3, 3, 1> vjjv;
				detail::tvec_swizzle<T, 1, 3, 3, 2> vjji;
				detail::tvec_swizzle<T, 1, 3, 3, 3> vjjj;
				detail::tvec_swizzle<T, 2, 0, 0, 0> iuuu;
				detail::tvec_swizzle<T, 2, 0, 0, 1> iuuv;
				detail::tvec_swizzle<T, 2, 0, 0, 2> iuui;
				detail::tvec_swizzle<T, 2, 0, 0, 3> iuuj;
				detail::tvec_swizzle<T, 2, 0, 1, 0> iuvu;
				detail::tvec_swizzle<T, 2, 0, 1, 1> iuvv;
				detail::tvec_swizzle<T, 2, 0, 1, 2> iuvi;
				detail::tvec_swizzle<T, 2, 0, 1, 3> iuvj;
				detail::tvec_swizzle<T, 2, 0, 2, 0> iuiu;
				detail::tvec_swizzle<T, 2, 0, 2, 1> iuiv;
				detail::tvec_swizzle<T, 2, 0, 2, 2> iuii;
				detail::tvec_swizzle<T, 2, 0, 2, 3> iuij;
				detail::tvec_swizzle<T, 2, 0, 3, 0> iuju;
				detail::tvec_swizzle<T, 2, 0, 3, 1> iujv;
				detail::tvec_swizzle<T, 2, 0, 3, 2> iuji;
				detail::tvec_swizzle<T, 2, 0, 3, 3> iujj;
				detail::tvec_swizzle<T, 2, 1, 0, 0> ivuu;
				detail::tvec_swizzle<T, 2, 1, 0, 1> ivuv;
				detail::tvec_swizzle<T, 2, 1, 0, 2> ivui;
				detail::tvec_swizzle<T, 2, 1, 0, 3> ivuj;
				detail::tvec_swizzle<T, 2, 1, 1, 0> ivvu;
				detail::tvec_swizzle<T, 2, 1, 1, 1> ivvv;
				detail::tvec_swizzle<T, 2, 1, 1, 2> ivvi;
				detail::tvec_swizzle<T, 2, 1, 1, 3> ivvj;
				detail::tvec_swizzle<T, 2, 1, 2, 0> iviu;
				detail::tvec_swizzle<T, 2, 1, 2, 1> iviv;
				detail::tvec_swizzle<T, 2, 1, 2, 2> ivii;
				detail::tvec_swizzle<T, 2, 1, 2, 3> ivij;
				detail::tvec_swizzle<T, 2, 1, 3, 0> ivju;
				detail::tvec_swizzle<T, 2, 1, 3, 1> ivjv;
				detail::tvec_swizzle<T, 2, 1, 3, 2> ivji;
				detail::tvec_swizzle<T, 2, 1, 3, 3> ivjj;
				detail::tvec_swizzle<T, 2, 2, 0, 0> iiuu;
				detail::tvec_swizzle<T, 2, 2, 0, 1> iiuv;
				detail::tvec_swizzle<T, 2, 2, 0, 2> iiui;
				detail::tvec_swizzle<T, 2, 2, 0, 3> iiuj;
				detail::tvec_swizzle<T, 2, 2, 1, 0> iivu;
				detail::tvec_swizzle<T, 2, 2, 1, 1> iivv;
				detail::tvec_swizzle<T, 2, 2, 1, 2> iivi;
				detail::tvec_swizzle<T, 2, 2, 1, 3> iivj;
				detail::tvec_swizzle<T, 2, 2, 2, 0> iiiu;
				detail::tvec_swizzle<T, 2, 2, 2, 1> iiiv;
				detail::tvec_swizzle<T, 2, 2, 2, 2> iiii;
				detail::tvec_swizzle<T, 2, 2, 2, 3> iiij;
				detail::tvec_swizzle<T, 2, 2, 3, 0> iiju;
				detail::tvec_swizzle<T, 2, 2, 3, 1> iijv;
				detail::tvec_swizzle<T, 2, 2, 3, 2> iiji;
				detail::tvec_swizzle<T, 2, 2, 3, 3> iijj;
				detail::tvec_swizzle<T, 2, 3, 0, 0> ijuu;
				detail::tvec_swizzle<T, 2, 3, 0, 1> ijuv;
				detail::tvec_swizzle<T, 2, 3, 0, 2> ijui;
				detail::tvec_swizzle<T, 2, 3, 0, 3> ijuj;
				detail::tvec_swizzle<T, 2, 3, 1, 0> ijvu;
				detail::tvec_swizzle<T, 2, 3, 1, 1> ijvv;
				detail::tvec_swizzle<T, 2, 3, 1, 2> ijvi;
				detail::tvec_swizzle<T, 2, 3, 1, 3> ijvj;
				detail::tvec_swizzle<T, 2, 3, 2, 0> ijiu;
				detail::tvec_swizzle<T, 2, 3, 2, 1> ijiv;
				detail::tvec_swizzle<T, 2, 3, 2, 2> ijii;
				detail::tvec_swizzle<T, 2, 3, 2, 3> ijij;
				detail::tvec_swizzle<T, 2, 3, 3, 0> ijju;
				detail::tvec_swizzle<T, 2, 3, 3, 1> ijjv;
				detail::tvec_swizzle<T, 2, 3, 3, 2> ijji;
				detail::tvec_swizzle<T, 2, 3, 3, 3> ijjj;
				detail::tvec_swizzle<T, 3, 0, 0, 0> juuu;
				detail::tvec_swizzle<T, 3, 0, 0, 1> juuv;
				detail::tvec_swizzle<T, 3, 0, 0, 2> juui;
				detail::tvec_swizzle<T, 3, 0, 0, 3> juuj;
				detail::tvec_swizzle<T, 3, 0, 1, 0> juvu;
				detail::tvec_swizzle<T, 3, 0, 1, 1> juvv;
				detail::tvec_swizzle<T, 3, 0, 1, 2> juvi;
				detail::tvec_swizzle<T, 3, 0, 1, 3> juvj;
				detail::tvec_swizzle<T, 3, 0, 2, 0> juiu;
				detail::tvec_swizzle<T, 3, 0, 2, 1> juiv;
				detail::tvec_swizzle<T, 3, 0, 2, 2> juii;
				detail::tvec_swizzle<T, 3, 0, 2, 3> juij;
				detail::tvec_swizzle<T, 3, 0, 3, 0> juju;
				detail::tvec_swizzle<T, 3, 0, 3, 1> jujv;
				detail::tvec_swizzle<T, 3, 0, 3, 2> juji;
				detail::tvec_swizzle<T, 3, 0, 3, 3> jujj;
				detail::tvec_swizzle<T, 3, 1, 0, 0> jvuu;
				detail::tvec_swizzle<T, 3, 1, 0, 1> jvuv;
				detail::tvec_swizzle<T, 3, 1, 0, 2> jvui;
				detail::tvec_swizzle<T, 3, 1, 0, 3> jvuj;
				detail::tvec_swizzle<T, 3, 1, 1, 0> jvvu;
				detail::tvec_swizzle<T, 3, 1, 1, 1> jvvv;
				detail::tvec_swizzle<T, 3, 1, 1, 2> jvvi;
				detail::tvec_swizzle<T, 3, 1, 1, 3> jvvj;
				detail::tvec_swizzle<T, 3, 1, 2, 0> jviu;
				detail::tvec_swizzle<T, 3, 1, 2, 1> jviv;
				detail::tvec_swizzle<T, 3, 1, 2, 2> jvii;
				detail::tvec_swizzle<T, 3, 1, 2, 3> jvij;
				detail::tvec_swizzle<T, 3, 1, 3, 0> jvju;
				detail::tvec_swizzle<T, 3, 1, 3, 1> jvjv;
				detail::tvec_swizzle<T, 3, 1, 3, 2> jvji;
				detail::tvec_swizzle<T, 3, 1, 3, 3> jvjj;
				detail::tvec_swizzle<T, 3, 2, 0, 0> jiuu;
				detail::tvec_swizzle<T, 3, 2, 0, 1> jiuv;
				detail::tvec_swizzle<T, 3, 2, 0, 2> jiui;
				detail::tvec_swizzle<T, 3, 2, 0, 3> jiuj;
				detail::tvec_swizzle<T, 3, 2, 1, 0> jivu;
				detail::tvec_swizzle<T, 3, 2, 1, 1> jivv;
				detail::tvec_swizzle<T, 3, 2, 1, 2> jivi;
				detail::tvec_swizzle<T, 3, 2, 1, 3> jivj;
				detail::tvec_swizzle<T, 3, 2, 2, 0> jiiu;
				detail::tvec_swizzle<T, 3, 2, 2, 1> jiiv;
				detail::tvec_swizzle<T, 3, 2, 2, 2> jiii;
				detail::tvec_swizzle<T, 3, 2, 2, 3> jiij;
				detail::tvec_swizzle<T, 3, 2, 3, 0> jiju;
				detail::tvec_swizzle<T, 3, 2, 3, 1> jijv;
				detail::tvec_swizzle<T, 3, 2, 3, 2> jiji;
				detail::tvec_swizzle<T, 3, 2, 3, 3> jijj;
				detail::tvec_swizzle<T, 3, 3, 0, 0> jjuu;
				detail::tvec_swizzle<T, 3, 3, 0, 1> jjuv;
				detail::tvec_swizzle<T, 3, 3, 0, 2> jjui;
				detail::tvec_swizzle<T, 3, 3, 0, 3> jjuj;
				detail::tvec_swizzle<T, 3, 3, 1, 0> jjvu;
				detail::tvec_swizzle<T, 3, 3, 1, 1> jjvv;
				detail::tvec_swizzle<T, 3, 3, 1, 2> jjvi;
				detail::tvec_swizzle<T, 3, 3, 1, 3> jjvj;
				detail::tvec_swizzle<T, 3, 3, 2, 0> jjiu;
				detail::tvec_swizzle<T, 3, 3, 2, 1> jjiv;
				detail::tvec_swizzle<T, 3, 3, 2, 2> jjii;
				detail::tvec_swizzle<T, 3, 3, 2, 3> jjij;
				detail::tvec_swizzle<T, 3, 3, 3, 0> jjju;
				detail::tvec_swizzle<T, 3, 3, 3, 1> jjjv;
				detail::tvec_swizzle<T, 3, 3, 3, 2> jjji;
				detail::tvec_swizzle<T, 3, 3, 3, 3> jjjj;
			#endif
			};
			const T& operator [] (std::size_t i)const {
			#if _NEWTON_MATH_RANGE_CHECK			
				if (i >= size) throw std::out_of_range ("Vector subscript out of range.");
			#endif
				return reinterpret_cast<const T (&) [size]> (*this) [i];
			}
			 T& operator [] (std::size_t i) {
			#if _NEWTON_MATH_RANGE_CHECK			
				if (i >= size) throw std::out_of_range ("Vector subscript out of range.");
			#endif
				return reinterpret_cast< T (&) [size]> (*this) [i];
			}
			
			constexpr tvec (T v0, T v1, T v2, T v3) : 
				x (v0), y (v1), z (v2), w (v3)				 
			{}
			
			constexpr tvec (T in) : 
				tvec (in, in, in, in) 
			{}
			
			constexpr tvec ()  : tvec (T ()) {}
			
			//template <typename U, std::size_t... I>
			//tvec (const detail::tvec_swizzle<U, I...>& a) : tvec (a ()) {}
			
			template <typename I> 
			explicit tvec (const tvec<2, I>& in) : 
				tvec (in.x, in.y, T (), T ()) 
			{}
			
			template <typename I> 
			explicit tvec (const tvec<3, I>& in) : 
				tvec (in.x, in.y, in.z, T ()) 
			{}
			
			template <typename I, std::enable_if_t<!std::is_same<T, I>::value, int> = 0> 
			explicit tvec (const tvec<4, I>& in) : 
				tvec (in.x, in.y, in.z, in.w) 
			{}
			
			explicit tvec (T v0, T v1, const tvec<2, T>& v2) : tvec (v0, v1, v2.x, v2.y) {}
			explicit tvec (T v0, const tvec<2, T>& v1, T v2) : tvec (v0, v1.x, v1.y, v2) {}
			explicit tvec (T v0, const tvec<3, T>& v1) : tvec (v0, v1.x, v1.y, v1.z) {}
			explicit tvec (const tvec<2, T>& v0, T v1, T v2) : tvec (v0.x, v0.y, v1, v2) {}
			explicit tvec (const tvec<2, T>& v0, const tvec<2, T>& v1) : tvec (v0.x, v0.y, v1.x, v1.y) {}
			explicit tvec (const tvec<3, T>& v0, T v1) : tvec (v0.x, v0.y, v0.z, v1) {}
			tvec (const tvec<4, T>& v0) : tvec (v0.x, v0.y, v0.z, v0.w) {}
			tvec<4, T>& operator = (const tvec<4, T>& in) {
				(*this).x = in.x;
				(*this).y = in.y;
				(*this).z = in.z;
				(*this).w = in.w;
				return *this;
			};
		};
////////////////////////////////
// I/O Operators
////////////////////////////////
		template <typename T>
		std::istream& operator >> (std::istream& iss, tvec<4, T>& in) {			
			return iss >> in.x >> in.y >> in.z >> in.w;
		}		
		template <typename T>
		std::ostream& operator << (std::ostream& oss, const tvec<4, T>& in) {
			return oss << "{ " << "x: " << in.x << ", "<< "y: " << in.y << ", "<< "z: " << in.z << ", "<< "w: " << in.w << ", " << " }";
		}

////////////////////////////////
// Binary operations
////////////////////////////////
		template <typename A, typename B>
		inline auto operator + (const tvec<4, A>& a, const tvec<4, B>& b) {
			return tvec_make (a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator + (const tvec<4, A>& a, const B& b) {
			return a + tvec<4, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator + (const A& a, const tvec<4, B>& b) {
			return tvec<4, A> (a) + b;
		}
		
		template <typename A, typename B>
		inline auto operator - (const tvec<4, A>& a, const tvec<4, B>& b) {
			return tvec_make (a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator - (const tvec<4, A>& a, const B& b) {
			return a - tvec<4, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator - (const A& a, const tvec<4, B>& b) {
			return tvec<4, A> (a) - b;
		}
		
		template <typename A, typename B>
		inline auto operator * (const tvec<4, A>& a, const tvec<4, B>& b) {
			return tvec_make (a.x * b.x, a.y * b.y, a.z * b.z, a.w * b.w);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator * (const tvec<4, A>& a, const B& b) {
			return a * tvec<4, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator * (const A& a, const tvec<4, B>& b) {
			return tvec<4, A> (a) * b;
		}
		
		template <typename A, typename B>
		inline auto operator / (const tvec<4, A>& a, const tvec<4, B>& b) {
			return tvec_make (a.x / b.x, a.y / b.y, a.z / b.z, a.w / b.w);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator / (const tvec<4, A>& a, const B& b) {
			return a / tvec<4, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator / (const A& a, const tvec<4, B>& b) {
			return tvec<4, A> (a) / b;
		}
		
		template <typename A, typename B>
		inline auto operator % (const tvec<4, A>& a, const tvec<4, B>& b) {
			return tvec_make (modulus (a.x, b.x), modulus (a.y, b.y), modulus (a.z, b.z), modulus (a.w, b.w));			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator % (const tvec<4, A>& a, const B& b) {
			return a % tvec<4, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator % (const A& a, const tvec<4, B>& b) {
			return tvec<4, A> (a) % b;
		}
		
		template <typename A, typename B>
		inline auto operator & (const tvec<4, A>& a, const tvec<4, B>& b) {
			return tvec_make (a.x & b.x, a.y & b.y, a.z & b.z, a.w & b.w);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator & (const tvec<4, A>& a, const B& b) {
			return a & tvec<4, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator & (const A& a, const tvec<4, B>& b) {
			return tvec<4, A> (a) & b;
		}
		
		template <typename A, typename B>
		inline auto operator | (const tvec<4, A>& a, const tvec<4, B>& b) {
			return tvec_make (a.x | b.x, a.y | b.y, a.z | b.z, a.w | b.w);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator | (const tvec<4, A>& a, const B& b) {
			return a | tvec<4, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator | (const A& a, const tvec<4, B>& b) {
			return tvec<4, A> (a) | b;
		}
		
		template <typename A, typename B>
		inline auto operator ^ (const tvec<4, A>& a, const tvec<4, B>& b) {
			return tvec_make (a.x ^ b.x, a.y ^ b.y, a.z ^ b.z, a.w ^ b.w);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator ^ (const tvec<4, A>& a, const B& b) {
			return a ^ tvec<4, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator ^ (const A& a, const tvec<4, B>& b) {
			return tvec<4, A> (a) ^ b;
		}
		
		template <typename A, typename B>
		inline auto operator && (const tvec<4, A>& a, const tvec<4, B>& b) {
			return tvec_make (a.x && b.x, a.y && b.y, a.z && b.z, a.w && b.w);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator && (const tvec<4, A>& a, const B& b) {
			return a && tvec<4, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator && (const A& a, const tvec<4, B>& b) {
			return tvec<4, A> (a) && b;
		}
		
		template <typename A, typename B>
		inline auto operator || (const tvec<4, A>& a, const tvec<4, B>& b) {
			return tvec_make (a.x || b.x, a.y || b.y, a.z || b.z, a.w || b.w);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator || (const tvec<4, A>& a, const B& b) {
			return a || tvec<4, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator || (const A& a, const tvec<4, B>& b) {
			return tvec<4, A> (a) || b;
		}
		
////////////////////////////////
// Boolean operations
////////////////////////////////
		template <typename A, typename B>
		inline auto operator == (const tvec<4, A>& a, const tvec<4, B>& b) {
			return (a.x == b.x) && (a.y == b.y) && (a.z == b.z) && (a.w == b.w);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator == (const tvec<4, A>& a, const B& b) {
			return a == tvec<4, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator == (const A& a, const tvec<4, B>& b) {
			return tvec<4, A> (a) == b;
		}		
		
		template <typename A, typename B>
		inline auto operator != (const tvec<4, A>& a, const tvec<4, B>& b) {
			return (a.x != b.x) || (a.y != b.y) || (a.z != b.z) || (a.w != b.w);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator != (const tvec<4, A>& a, const B& b) {
			return a != tvec<4, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator != (const A& a, const tvec<4, B>& b) {
			return tvec<4, A> (a) != b;
		}		
		
		


////////////////////////////////
// Unary operations
////////////////////////////////
		template <typename T>
		inline auto operator + (const tvec<4, T>& a) {
			return tvec_make (+a.x, +a.y, +a.z, +a.w);			
		}
		
		template <typename T>
		inline auto operator - (const tvec<4, T>& a) {
			return tvec_make (-a.x, -a.y, -a.z, -a.w);			
		}
		
		template <typename T>
		inline auto operator ~ (const tvec<4, T>& a) {
			return tvec_make (~a.x, ~a.y, ~a.z, ~a.w);			
		}
		
		template <typename T>
		inline auto operator ! (const tvec<4, T>& a) {
			return tvec_make (!a.x, !a.y, !a.z, !a.w);			
		}
		
////////////////////////////////
// Misc operations
////////////////////////////////
		template <typename A, typename B>
		inline auto dot (const tvec<4, A>& a, const tvec<4, B>& b)  {			
			return a.x*b.x + a.y*b.y + a.z*b.z + a.w*b.w;
		}
		
		template <typename T>
		inline auto hypot2 (const tvec<4, T>& a)  {
			return dot (a, a);
		}
		
		template <typename T>
		inline auto hypot (const tvec<4, T>& a)  {
			return std::sqrt (hypot2 (a));
		}

		template <typename T>
		inline auto length2 (const tvec<4, T>& a)  {
			return hypot2 (a);
		}
		
		template <typename T>
		inline auto length (const tvec<4, T>& a)  {
			return hypot (a);
		}
		
		template <typename T>
		inline auto normalize (const tvec<4, T>& a)  {
			return a / hypot (a);
		}	
		
		template <typename T>
		inline auto distance (const tvec<4, T>& a, const tvec<4, T>& b)  {
			return hypot (b - a);
		}

		template <typename T>
		inline auto distance2 (const tvec<4, T>& a, const tvec<4, T>& b)  {
			return hypot2 (b - a);
		}
		

		template <typename T>
		auto abs (const tvec<4, T>& x)  {
			return tvec_make (std::abs (x.x), std::abs (x.y), std::abs (x.z), std::abs (x.w));
		}		
		
		template <typename T>
		auto min (const tvec<4, T>& x, const tvec<4, T>& y)  {
			return tvec_make (std::min (x.x, y.x), std::min (x.y, y.y), std::min (x.z, y.z), std::min (x.w, y.w));
		}		
		
		template <typename T>
		auto max (const tvec<4, T>& x, const tvec<4, T>& y)  {
			return tvec_make (std::max (x.x, y.x), std::max (x.y, y.y), std::max (x.z, y.z), std::max (x.w, y.w));
		}		
		
		template <typename T>
		auto sin (const tvec<4, T>& x)  {
			return tvec_make (std::sin (x.x), std::sin (x.y), std::sin (x.z), std::sin (x.w));
		}		
		
		template <typename T>
		auto cos (const tvec<4, T>& x)  {
			return tvec_make (std::cos (x.x), std::cos (x.y), std::cos (x.z), std::cos (x.w));
		}		
		
		template <typename T>
		auto tan (const tvec<4, T>& x)  {
			return tvec_make (std::tan (x.x), std::tan (x.y), std::tan (x.z), std::tan (x.w));
		}		
		
		template <typename T>
		auto sinh (const tvec<4, T>& x)  {
			return tvec_make (std::sinh (x.x), std::sinh (x.y), std::sinh (x.z), std::sinh (x.w));
		}		
		
		template <typename T>
		auto cosh (const tvec<4, T>& x)  {
			return tvec_make (std::cosh (x.x), std::cosh (x.y), std::cosh (x.z), std::cosh (x.w));
		}		
		
		template <typename T>
		auto tanh (const tvec<4, T>& x)  {
			return tvec_make (std::tanh (x.x), std::tanh (x.y), std::tanh (x.z), std::tanh (x.w));
		}		
		
		template <typename T>
		auto asin (const tvec<4, T>& x)  {
			return tvec_make (std::asin (x.x), std::asin (x.y), std::asin (x.z), std::asin (x.w));
		}		
		
		template <typename T>
		auto acos (const tvec<4, T>& x)  {
			return tvec_make (std::acos (x.x), std::acos (x.y), std::acos (x.z), std::acos (x.w));
		}		
		
		template <typename T>
		auto atan (const tvec<4, T>& x)  {
			return tvec_make (std::atan (x.x), std::atan (x.y), std::atan (x.z), std::atan (x.w));
		}		
		
		template <typename T>
		auto atan2 (const tvec<4, T>& x, const tvec<4, T>& y)  {
			return tvec_make (std::atan2 (x.x, y.x), std::atan2 (x.y, y.y), std::atan2 (x.z, y.z), std::atan2 (x.w, y.w));
		}		
		
		template <typename T>
		auto log (const tvec<4, T>& x)  {
			return tvec_make (std::log (x.x), std::log (x.y), std::log (x.z), std::log (x.w));
		}		
		
		template <typename T>
		auto log2 (const tvec<4, T>& x)  {
			return tvec_make (std::log2 (x.x), std::log2 (x.y), std::log2 (x.z), std::log2 (x.w));
		}		
		
		template <typename T>
		auto log10 (const tvec<4, T>& x)  {
			return tvec_make (std::log10 (x.x), std::log10 (x.y), std::log10 (x.z), std::log10 (x.w));
		}		
		
		template <typename T>
		auto log1p (const tvec<4, T>& x)  {
			return tvec_make (std::log1p (x.x), std::log1p (x.y), std::log1p (x.z), std::log1p (x.w));
		}		
		
		template <typename T>
		auto exp (const tvec<4, T>& x)  {
			return tvec_make (std::exp (x.x), std::exp (x.y), std::exp (x.z), std::exp (x.w));
		}		
		
		template <typename T>
		auto exp2 (const tvec<4, T>& x)  {
			return tvec_make (std::exp2 (x.x), std::exp2 (x.y), std::exp2 (x.z), std::exp2 (x.w));
		}		
		
		template <typename T>
		auto exp1m (const tvec<4, T>& x)  {
			return tvec_make (std::exp1m (x.x), std::exp1m (x.y), std::exp1m (x.z), std::exp1m (x.w));
		}		
		
		template <typename T>
		auto pow (const tvec<4, T>& x, const tvec<4, T>& n)  {
			return tvec_make (std::pow (x.x, n.x), std::pow (x.y, n.y), std::pow (x.z, n.z), std::pow (x.w, n.w));
		}		
		
		template <typename T>
		auto sqrt (const tvec<4, T>& x)  {
			return tvec_make (std::sqrt (x.x), std::sqrt (x.y), std::sqrt (x.z), std::sqrt (x.w));
		}		
		
		template <typename T>
		auto cbrt (const tvec<4, T>& x)  {
			return tvec_make (std::cbrt (x.x), std::cbrt (x.y), std::cbrt (x.z), std::cbrt (x.w));
		}		
		
		template <typename T>
		auto floor (const tvec<4, T>& x)  {
			return tvec_make (std::floor (x.x), std::floor (x.y), std::floor (x.z), std::floor (x.w));
		}		
		
		template <typename T>
		auto ceil (const tvec<4, T>& x)  {
			return tvec_make (std::ceil (x.x), std::ceil (x.y), std::ceil (x.z), std::ceil (x.w));
		}		
		
		template <typename T>
		auto round (const tvec<4, T>& x)  {
			return tvec_make (std::round (x.x), std::round (x.y), std::round (x.z), std::round (x.w));
		}		
		
		template <typename T>
		auto trunc (const tvec<4, T>& x)  {
			return tvec_make (std::trunc (x.x), std::trunc (x.y), std::trunc (x.z), std::trunc (x.w));
		}		
		
		template <typename T>
		auto copysign (const tvec<4, T>& x, const tvec<4, T>& y)  {
			return tvec_make (std::copysign (x.x, y.x), std::copysign (x.y, y.y), std::copysign (x.z, y.z), std::copysign (x.w, y.w));
		}		
		
		
	}
}
#pragma pack(pop)
#endif // __NEWTON_MATH_DETAIL_TVEC4_HPP__