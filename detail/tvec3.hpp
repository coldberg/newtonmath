#ifndef __NEWTON_MATH_DETAIL_TVEC3_HPP__ 
#define __NEWTON_MATH_DETAIL_TVEC3_HPP__ 

#include <cstddef>
#include <cstdint>

#include "basic_tvec.hpp"

#include <istream>
#include <ostream>
#include <cmath>
#include <type_traits>

#pragma pack(push, 1)
namespace Newton {
	namespace Math {
		namespace detail {
			template<typename T>
			struct tvec_landing<3, T> {				
				tvec_landing (T& x, T& y, T& z) : x (x), y (y), z (z) {}
				
				tvec_landing<3, T>& operator = (const tvec<3, T>& in)  {
					x = in.x;
					y = in.y;
					z = in.z;
					return *this;
				}
				
			private:
				T& x;
				T& y;
				T& z;
			};
		}
				
		template <typename T>
		struct tvec<3, T> {
			typedef T value_type;
			static const std::size_t size = 3;			

			union {				
				struct { value_type x, y, z; };
				struct { value_type r, g, b; };
				struct { value_type s, t, p; };
				struct { value_type u, v, i; };

			#if !_NEWTON_MATH_DISABLE_SWIZZLE			
				detail::tvec_swizzle<T, 0, 0> xx;
				detail::tvec_swizzle<T, 0, 1> xy;
				detail::tvec_swizzle<T, 0, 2> xz;
				detail::tvec_swizzle<T, 1, 0> yx;
				detail::tvec_swizzle<T, 1, 1> yy;
				detail::tvec_swizzle<T, 1, 2> yz;
				detail::tvec_swizzle<T, 2, 0> zx;
				detail::tvec_swizzle<T, 2, 1> zy;
				detail::tvec_swizzle<T, 2, 2> zz;
				detail::tvec_swizzle<T, 0, 0, 0> xxx;
				detail::tvec_swizzle<T, 0, 0, 1> xxy;
				detail::tvec_swizzle<T, 0, 0, 2> xxz;
				detail::tvec_swizzle<T, 0, 1, 0> xyx;
				detail::tvec_swizzle<T, 0, 1, 1> xyy;
				detail::tvec_swizzle<T, 0, 1, 2> xyz;
				detail::tvec_swizzle<T, 0, 2, 0> xzx;
				detail::tvec_swizzle<T, 0, 2, 1> xzy;
				detail::tvec_swizzle<T, 0, 2, 2> xzz;
				detail::tvec_swizzle<T, 1, 0, 0> yxx;
				detail::tvec_swizzle<T, 1, 0, 1> yxy;
				detail::tvec_swizzle<T, 1, 0, 2> yxz;
				detail::tvec_swizzle<T, 1, 1, 0> yyx;
				detail::tvec_swizzle<T, 1, 1, 1> yyy;
				detail::tvec_swizzle<T, 1, 1, 2> yyz;
				detail::tvec_swizzle<T, 1, 2, 0> yzx;
				detail::tvec_swizzle<T, 1, 2, 1> yzy;
				detail::tvec_swizzle<T, 1, 2, 2> yzz;
				detail::tvec_swizzle<T, 2, 0, 0> zxx;
				detail::tvec_swizzle<T, 2, 0, 1> zxy;
				detail::tvec_swizzle<T, 2, 0, 2> zxz;
				detail::tvec_swizzle<T, 2, 1, 0> zyx;
				detail::tvec_swizzle<T, 2, 1, 1> zyy;
				detail::tvec_swizzle<T, 2, 1, 2> zyz;
				detail::tvec_swizzle<T, 2, 2, 0> zzx;
				detail::tvec_swizzle<T, 2, 2, 1> zzy;
				detail::tvec_swizzle<T, 2, 2, 2> zzz;
				detail::tvec_swizzle<T, 0, 0, 0, 0> xxxx;
				detail::tvec_swizzle<T, 0, 0, 0, 1> xxxy;
				detail::tvec_swizzle<T, 0, 0, 0, 2> xxxz;
				detail::tvec_swizzle<T, 0, 0, 1, 0> xxyx;
				detail::tvec_swizzle<T, 0, 0, 1, 1> xxyy;
				detail::tvec_swizzle<T, 0, 0, 1, 2> xxyz;
				detail::tvec_swizzle<T, 0, 0, 2, 0> xxzx;
				detail::tvec_swizzle<T, 0, 0, 2, 1> xxzy;
				detail::tvec_swizzle<T, 0, 0, 2, 2> xxzz;
				detail::tvec_swizzle<T, 0, 1, 0, 0> xyxx;
				detail::tvec_swizzle<T, 0, 1, 0, 1> xyxy;
				detail::tvec_swizzle<T, 0, 1, 0, 2> xyxz;
				detail::tvec_swizzle<T, 0, 1, 1, 0> xyyx;
				detail::tvec_swizzle<T, 0, 1, 1, 1> xyyy;
				detail::tvec_swizzle<T, 0, 1, 1, 2> xyyz;
				detail::tvec_swizzle<T, 0, 1, 2, 0> xyzx;
				detail::tvec_swizzle<T, 0, 1, 2, 1> xyzy;
				detail::tvec_swizzle<T, 0, 1, 2, 2> xyzz;
				detail::tvec_swizzle<T, 0, 2, 0, 0> xzxx;
				detail::tvec_swizzle<T, 0, 2, 0, 1> xzxy;
				detail::tvec_swizzle<T, 0, 2, 0, 2> xzxz;
				detail::tvec_swizzle<T, 0, 2, 1, 0> xzyx;
				detail::tvec_swizzle<T, 0, 2, 1, 1> xzyy;
				detail::tvec_swizzle<T, 0, 2, 1, 2> xzyz;
				detail::tvec_swizzle<T, 0, 2, 2, 0> xzzx;
				detail::tvec_swizzle<T, 0, 2, 2, 1> xzzy;
				detail::tvec_swizzle<T, 0, 2, 2, 2> xzzz;
				detail::tvec_swizzle<T, 1, 0, 0, 0> yxxx;
				detail::tvec_swizzle<T, 1, 0, 0, 1> yxxy;
				detail::tvec_swizzle<T, 1, 0, 0, 2> yxxz;
				detail::tvec_swizzle<T, 1, 0, 1, 0> yxyx;
				detail::tvec_swizzle<T, 1, 0, 1, 1> yxyy;
				detail::tvec_swizzle<T, 1, 0, 1, 2> yxyz;
				detail::tvec_swizzle<T, 1, 0, 2, 0> yxzx;
				detail::tvec_swizzle<T, 1, 0, 2, 1> yxzy;
				detail::tvec_swizzle<T, 1, 0, 2, 2> yxzz;
				detail::tvec_swizzle<T, 1, 1, 0, 0> yyxx;
				detail::tvec_swizzle<T, 1, 1, 0, 1> yyxy;
				detail::tvec_swizzle<T, 1, 1, 0, 2> yyxz;
				detail::tvec_swizzle<T, 1, 1, 1, 0> yyyx;
				detail::tvec_swizzle<T, 1, 1, 1, 1> yyyy;
				detail::tvec_swizzle<T, 1, 1, 1, 2> yyyz;
				detail::tvec_swizzle<T, 1, 1, 2, 0> yyzx;
				detail::tvec_swizzle<T, 1, 1, 2, 1> yyzy;
				detail::tvec_swizzle<T, 1, 1, 2, 2> yyzz;
				detail::tvec_swizzle<T, 1, 2, 0, 0> yzxx;
				detail::tvec_swizzle<T, 1, 2, 0, 1> yzxy;
				detail::tvec_swizzle<T, 1, 2, 0, 2> yzxz;
				detail::tvec_swizzle<T, 1, 2, 1, 0> yzyx;
				detail::tvec_swizzle<T, 1, 2, 1, 1> yzyy;
				detail::tvec_swizzle<T, 1, 2, 1, 2> yzyz;
				detail::tvec_swizzle<T, 1, 2, 2, 0> yzzx;
				detail::tvec_swizzle<T, 1, 2, 2, 1> yzzy;
				detail::tvec_swizzle<T, 1, 2, 2, 2> yzzz;
				detail::tvec_swizzle<T, 2, 0, 0, 0> zxxx;
				detail::tvec_swizzle<T, 2, 0, 0, 1> zxxy;
				detail::tvec_swizzle<T, 2, 0, 0, 2> zxxz;
				detail::tvec_swizzle<T, 2, 0, 1, 0> zxyx;
				detail::tvec_swizzle<T, 2, 0, 1, 1> zxyy;
				detail::tvec_swizzle<T, 2, 0, 1, 2> zxyz;
				detail::tvec_swizzle<T, 2, 0, 2, 0> zxzx;
				detail::tvec_swizzle<T, 2, 0, 2, 1> zxzy;
				detail::tvec_swizzle<T, 2, 0, 2, 2> zxzz;
				detail::tvec_swizzle<T, 2, 1, 0, 0> zyxx;
				detail::tvec_swizzle<T, 2, 1, 0, 1> zyxy;
				detail::tvec_swizzle<T, 2, 1, 0, 2> zyxz;
				detail::tvec_swizzle<T, 2, 1, 1, 0> zyyx;
				detail::tvec_swizzle<T, 2, 1, 1, 1> zyyy;
				detail::tvec_swizzle<T, 2, 1, 1, 2> zyyz;
				detail::tvec_swizzle<T, 2, 1, 2, 0> zyzx;
				detail::tvec_swizzle<T, 2, 1, 2, 1> zyzy;
				detail::tvec_swizzle<T, 2, 1, 2, 2> zyzz;
				detail::tvec_swizzle<T, 2, 2, 0, 0> zzxx;
				detail::tvec_swizzle<T, 2, 2, 0, 1> zzxy;
				detail::tvec_swizzle<T, 2, 2, 0, 2> zzxz;
				detail::tvec_swizzle<T, 2, 2, 1, 0> zzyx;
				detail::tvec_swizzle<T, 2, 2, 1, 1> zzyy;
				detail::tvec_swizzle<T, 2, 2, 1, 2> zzyz;
				detail::tvec_swizzle<T, 2, 2, 2, 0> zzzx;
				detail::tvec_swizzle<T, 2, 2, 2, 1> zzzy;
				detail::tvec_swizzle<T, 2, 2, 2, 2> zzzz;
				detail::tvec_swizzle<T, 0, 0> rr;
				detail::tvec_swizzle<T, 0, 1> rg;
				detail::tvec_swizzle<T, 0, 2> rb;
				detail::tvec_swizzle<T, 1, 0> gr;
				detail::tvec_swizzle<T, 1, 1> gg;
				detail::tvec_swizzle<T, 1, 2> gb;
				detail::tvec_swizzle<T, 2, 0> br;
				detail::tvec_swizzle<T, 2, 1> bg;
				detail::tvec_swizzle<T, 2, 2> bb;
				detail::tvec_swizzle<T, 0, 0, 0> rrr;
				detail::tvec_swizzle<T, 0, 0, 1> rrg;
				detail::tvec_swizzle<T, 0, 0, 2> rrb;
				detail::tvec_swizzle<T, 0, 1, 0> rgr;
				detail::tvec_swizzle<T, 0, 1, 1> rgg;
				detail::tvec_swizzle<T, 0, 1, 2> rgb;
				detail::tvec_swizzle<T, 0, 2, 0> rbr;
				detail::tvec_swizzle<T, 0, 2, 1> rbg;
				detail::tvec_swizzle<T, 0, 2, 2> rbb;
				detail::tvec_swizzle<T, 1, 0, 0> grr;
				detail::tvec_swizzle<T, 1, 0, 1> grg;
				detail::tvec_swizzle<T, 1, 0, 2> grb;
				detail::tvec_swizzle<T, 1, 1, 0> ggr;
				detail::tvec_swizzle<T, 1, 1, 1> ggg;
				detail::tvec_swizzle<T, 1, 1, 2> ggb;
				detail::tvec_swizzle<T, 1, 2, 0> gbr;
				detail::tvec_swizzle<T, 1, 2, 1> gbg;
				detail::tvec_swizzle<T, 1, 2, 2> gbb;
				detail::tvec_swizzle<T, 2, 0, 0> brr;
				detail::tvec_swizzle<T, 2, 0, 1> brg;
				detail::tvec_swizzle<T, 2, 0, 2> brb;
				detail::tvec_swizzle<T, 2, 1, 0> bgr;
				detail::tvec_swizzle<T, 2, 1, 1> bgg;
				detail::tvec_swizzle<T, 2, 1, 2> bgb;
				detail::tvec_swizzle<T, 2, 2, 0> bbr;
				detail::tvec_swizzle<T, 2, 2, 1> bbg;
				detail::tvec_swizzle<T, 2, 2, 2> bbb;
				detail::tvec_swizzle<T, 0, 0, 0, 0> rrrr;
				detail::tvec_swizzle<T, 0, 0, 0, 1> rrrg;
				detail::tvec_swizzle<T, 0, 0, 0, 2> rrrb;
				detail::tvec_swizzle<T, 0, 0, 1, 0> rrgr;
				detail::tvec_swizzle<T, 0, 0, 1, 1> rrgg;
				detail::tvec_swizzle<T, 0, 0, 1, 2> rrgb;
				detail::tvec_swizzle<T, 0, 0, 2, 0> rrbr;
				detail::tvec_swizzle<T, 0, 0, 2, 1> rrbg;
				detail::tvec_swizzle<T, 0, 0, 2, 2> rrbb;
				detail::tvec_swizzle<T, 0, 1, 0, 0> rgrr;
				detail::tvec_swizzle<T, 0, 1, 0, 1> rgrg;
				detail::tvec_swizzle<T, 0, 1, 0, 2> rgrb;
				detail::tvec_swizzle<T, 0, 1, 1, 0> rggr;
				detail::tvec_swizzle<T, 0, 1, 1, 1> rggg;
				detail::tvec_swizzle<T, 0, 1, 1, 2> rggb;
				detail::tvec_swizzle<T, 0, 1, 2, 0> rgbr;
				detail::tvec_swizzle<T, 0, 1, 2, 1> rgbg;
				detail::tvec_swizzle<T, 0, 1, 2, 2> rgbb;
				detail::tvec_swizzle<T, 0, 2, 0, 0> rbrr;
				detail::tvec_swizzle<T, 0, 2, 0, 1> rbrg;
				detail::tvec_swizzle<T, 0, 2, 0, 2> rbrb;
				detail::tvec_swizzle<T, 0, 2, 1, 0> rbgr;
				detail::tvec_swizzle<T, 0, 2, 1, 1> rbgg;
				detail::tvec_swizzle<T, 0, 2, 1, 2> rbgb;
				detail::tvec_swizzle<T, 0, 2, 2, 0> rbbr;
				detail::tvec_swizzle<T, 0, 2, 2, 1> rbbg;
				detail::tvec_swizzle<T, 0, 2, 2, 2> rbbb;
				detail::tvec_swizzle<T, 1, 0, 0, 0> grrr;
				detail::tvec_swizzle<T, 1, 0, 0, 1> grrg;
				detail::tvec_swizzle<T, 1, 0, 0, 2> grrb;
				detail::tvec_swizzle<T, 1, 0, 1, 0> grgr;
				detail::tvec_swizzle<T, 1, 0, 1, 1> grgg;
				detail::tvec_swizzle<T, 1, 0, 1, 2> grgb;
				detail::tvec_swizzle<T, 1, 0, 2, 0> grbr;
				detail::tvec_swizzle<T, 1, 0, 2, 1> grbg;
				detail::tvec_swizzle<T, 1, 0, 2, 2> grbb;
				detail::tvec_swizzle<T, 1, 1, 0, 0> ggrr;
				detail::tvec_swizzle<T, 1, 1, 0, 1> ggrg;
				detail::tvec_swizzle<T, 1, 1, 0, 2> ggrb;
				detail::tvec_swizzle<T, 1, 1, 1, 0> gggr;
				detail::tvec_swizzle<T, 1, 1, 1, 1> gggg;
				detail::tvec_swizzle<T, 1, 1, 1, 2> gggb;
				detail::tvec_swizzle<T, 1, 1, 2, 0> ggbr;
				detail::tvec_swizzle<T, 1, 1, 2, 1> ggbg;
				detail::tvec_swizzle<T, 1, 1, 2, 2> ggbb;
				detail::tvec_swizzle<T, 1, 2, 0, 0> gbrr;
				detail::tvec_swizzle<T, 1, 2, 0, 1> gbrg;
				detail::tvec_swizzle<T, 1, 2, 0, 2> gbrb;
				detail::tvec_swizzle<T, 1, 2, 1, 0> gbgr;
				detail::tvec_swizzle<T, 1, 2, 1, 1> gbgg;
				detail::tvec_swizzle<T, 1, 2, 1, 2> gbgb;
				detail::tvec_swizzle<T, 1, 2, 2, 0> gbbr;
				detail::tvec_swizzle<T, 1, 2, 2, 1> gbbg;
				detail::tvec_swizzle<T, 1, 2, 2, 2> gbbb;
				detail::tvec_swizzle<T, 2, 0, 0, 0> brrr;
				detail::tvec_swizzle<T, 2, 0, 0, 1> brrg;
				detail::tvec_swizzle<T, 2, 0, 0, 2> brrb;
				detail::tvec_swizzle<T, 2, 0, 1, 0> brgr;
				detail::tvec_swizzle<T, 2, 0, 1, 1> brgg;
				detail::tvec_swizzle<T, 2, 0, 1, 2> brgb;
				detail::tvec_swizzle<T, 2, 0, 2, 0> brbr;
				detail::tvec_swizzle<T, 2, 0, 2, 1> brbg;
				detail::tvec_swizzle<T, 2, 0, 2, 2> brbb;
				detail::tvec_swizzle<T, 2, 1, 0, 0> bgrr;
				detail::tvec_swizzle<T, 2, 1, 0, 1> bgrg;
				detail::tvec_swizzle<T, 2, 1, 0, 2> bgrb;
				detail::tvec_swizzle<T, 2, 1, 1, 0> bggr;
				detail::tvec_swizzle<T, 2, 1, 1, 1> bggg;
				detail::tvec_swizzle<T, 2, 1, 1, 2> bggb;
				detail::tvec_swizzle<T, 2, 1, 2, 0> bgbr;
				detail::tvec_swizzle<T, 2, 1, 2, 1> bgbg;
				detail::tvec_swizzle<T, 2, 1, 2, 2> bgbb;
				detail::tvec_swizzle<T, 2, 2, 0, 0> bbrr;
				detail::tvec_swizzle<T, 2, 2, 0, 1> bbrg;
				detail::tvec_swizzle<T, 2, 2, 0, 2> bbrb;
				detail::tvec_swizzle<T, 2, 2, 1, 0> bbgr;
				detail::tvec_swizzle<T, 2, 2, 1, 1> bbgg;
				detail::tvec_swizzle<T, 2, 2, 1, 2> bbgb;
				detail::tvec_swizzle<T, 2, 2, 2, 0> bbbr;
				detail::tvec_swizzle<T, 2, 2, 2, 1> bbbg;
				detail::tvec_swizzle<T, 2, 2, 2, 2> bbbb;
				detail::tvec_swizzle<T, 0, 0> ss;
				detail::tvec_swizzle<T, 0, 1> st;
				detail::tvec_swizzle<T, 0, 2> sp;
				detail::tvec_swizzle<T, 1, 0> ts;
				detail::tvec_swizzle<T, 1, 1> tt;
				detail::tvec_swizzle<T, 1, 2> tp;
				detail::tvec_swizzle<T, 2, 0> ps;
				detail::tvec_swizzle<T, 2, 1> pt;
				detail::tvec_swizzle<T, 2, 2> pp;
				detail::tvec_swizzle<T, 0, 0, 0> sss;
				detail::tvec_swizzle<T, 0, 0, 1> sst;
				detail::tvec_swizzle<T, 0, 0, 2> ssp;
				detail::tvec_swizzle<T, 0, 1, 0> sts;
				detail::tvec_swizzle<T, 0, 1, 1> stt;
				detail::tvec_swizzle<T, 0, 1, 2> stp;
				detail::tvec_swizzle<T, 0, 2, 0> sps;
				detail::tvec_swizzle<T, 0, 2, 1> spt;
				detail::tvec_swizzle<T, 0, 2, 2> spp;
				detail::tvec_swizzle<T, 1, 0, 0> tss;
				detail::tvec_swizzle<T, 1, 0, 1> tst;
				detail::tvec_swizzle<T, 1, 0, 2> tsp;
				detail::tvec_swizzle<T, 1, 1, 0> tts;
				detail::tvec_swizzle<T, 1, 1, 1> ttt;
				detail::tvec_swizzle<T, 1, 1, 2> ttp;
				detail::tvec_swizzle<T, 1, 2, 0> tps;
				detail::tvec_swizzle<T, 1, 2, 1> tpt;
				detail::tvec_swizzle<T, 1, 2, 2> tpp;
				detail::tvec_swizzle<T, 2, 0, 0> pss;
				detail::tvec_swizzle<T, 2, 0, 1> pst;
				detail::tvec_swizzle<T, 2, 0, 2> psp;
				detail::tvec_swizzle<T, 2, 1, 0> pts;
				detail::tvec_swizzle<T, 2, 1, 1> ptt;
				detail::tvec_swizzle<T, 2, 1, 2> ptp;
				detail::tvec_swizzle<T, 2, 2, 0> pps;
				detail::tvec_swizzle<T, 2, 2, 1> ppt;
				detail::tvec_swizzle<T, 2, 2, 2> ppp;
				detail::tvec_swizzle<T, 0, 0, 0, 0> ssss;
				detail::tvec_swizzle<T, 0, 0, 0, 1> ssst;
				detail::tvec_swizzle<T, 0, 0, 0, 2> sssp;
				detail::tvec_swizzle<T, 0, 0, 1, 0> ssts;
				detail::tvec_swizzle<T, 0, 0, 1, 1> sstt;
				detail::tvec_swizzle<T, 0, 0, 1, 2> sstp;
				detail::tvec_swizzle<T, 0, 0, 2, 0> ssps;
				detail::tvec_swizzle<T, 0, 0, 2, 1> sspt;
				detail::tvec_swizzle<T, 0, 0, 2, 2> sspp;
				detail::tvec_swizzle<T, 0, 1, 0, 0> stss;
				detail::tvec_swizzle<T, 0, 1, 0, 1> stst;
				detail::tvec_swizzle<T, 0, 1, 0, 2> stsp;
				detail::tvec_swizzle<T, 0, 1, 1, 0> stts;
				detail::tvec_swizzle<T, 0, 1, 1, 1> sttt;
				detail::tvec_swizzle<T, 0, 1, 1, 2> sttp;
				detail::tvec_swizzle<T, 0, 1, 2, 0> stps;
				detail::tvec_swizzle<T, 0, 1, 2, 1> stpt;
				detail::tvec_swizzle<T, 0, 1, 2, 2> stpp;
				detail::tvec_swizzle<T, 0, 2, 0, 0> spss;
				detail::tvec_swizzle<T, 0, 2, 0, 1> spst;
				detail::tvec_swizzle<T, 0, 2, 0, 2> spsp;
				detail::tvec_swizzle<T, 0, 2, 1, 0> spts;
				detail::tvec_swizzle<T, 0, 2, 1, 1> sptt;
				detail::tvec_swizzle<T, 0, 2, 1, 2> sptp;
				detail::tvec_swizzle<T, 0, 2, 2, 0> spps;
				detail::tvec_swizzle<T, 0, 2, 2, 1> sppt;
				detail::tvec_swizzle<T, 0, 2, 2, 2> sppp;
				detail::tvec_swizzle<T, 1, 0, 0, 0> tsss;
				detail::tvec_swizzle<T, 1, 0, 0, 1> tsst;
				detail::tvec_swizzle<T, 1, 0, 0, 2> tssp;
				detail::tvec_swizzle<T, 1, 0, 1, 0> tsts;
				detail::tvec_swizzle<T, 1, 0, 1, 1> tstt;
				detail::tvec_swizzle<T, 1, 0, 1, 2> tstp;
				detail::tvec_swizzle<T, 1, 0, 2, 0> tsps;
				detail::tvec_swizzle<T, 1, 0, 2, 1> tspt;
				detail::tvec_swizzle<T, 1, 0, 2, 2> tspp;
				detail::tvec_swizzle<T, 1, 1, 0, 0> ttss;
				detail::tvec_swizzle<T, 1, 1, 0, 1> ttst;
				detail::tvec_swizzle<T, 1, 1, 0, 2> ttsp;
				detail::tvec_swizzle<T, 1, 1, 1, 0> ttts;
				detail::tvec_swizzle<T, 1, 1, 1, 1> tttt;
				detail::tvec_swizzle<T, 1, 1, 1, 2> tttp;
				detail::tvec_swizzle<T, 1, 1, 2, 0> ttps;
				detail::tvec_swizzle<T, 1, 1, 2, 1> ttpt;
				detail::tvec_swizzle<T, 1, 1, 2, 2> ttpp;
				detail::tvec_swizzle<T, 1, 2, 0, 0> tpss;
				detail::tvec_swizzle<T, 1, 2, 0, 1> tpst;
				detail::tvec_swizzle<T, 1, 2, 0, 2> tpsp;
				detail::tvec_swizzle<T, 1, 2, 1, 0> tpts;
				detail::tvec_swizzle<T, 1, 2, 1, 1> tptt;
				detail::tvec_swizzle<T, 1, 2, 1, 2> tptp;
				detail::tvec_swizzle<T, 1, 2, 2, 0> tpps;
				detail::tvec_swizzle<T, 1, 2, 2, 1> tppt;
				detail::tvec_swizzle<T, 1, 2, 2, 2> tppp;
				detail::tvec_swizzle<T, 2, 0, 0, 0> psss;
				detail::tvec_swizzle<T, 2, 0, 0, 1> psst;
				detail::tvec_swizzle<T, 2, 0, 0, 2> pssp;
				detail::tvec_swizzle<T, 2, 0, 1, 0> psts;
				detail::tvec_swizzle<T, 2, 0, 1, 1> pstt;
				detail::tvec_swizzle<T, 2, 0, 1, 2> pstp;
				detail::tvec_swizzle<T, 2, 0, 2, 0> psps;
				detail::tvec_swizzle<T, 2, 0, 2, 1> pspt;
				detail::tvec_swizzle<T, 2, 0, 2, 2> pspp;
				detail::tvec_swizzle<T, 2, 1, 0, 0> ptss;
				detail::tvec_swizzle<T, 2, 1, 0, 1> ptst;
				detail::tvec_swizzle<T, 2, 1, 0, 2> ptsp;
				detail::tvec_swizzle<T, 2, 1, 1, 0> ptts;
				detail::tvec_swizzle<T, 2, 1, 1, 1> pttt;
				detail::tvec_swizzle<T, 2, 1, 1, 2> pttp;
				detail::tvec_swizzle<T, 2, 1, 2, 0> ptps;
				detail::tvec_swizzle<T, 2, 1, 2, 1> ptpt;
				detail::tvec_swizzle<T, 2, 1, 2, 2> ptpp;
				detail::tvec_swizzle<T, 2, 2, 0, 0> ppss;
				detail::tvec_swizzle<T, 2, 2, 0, 1> ppst;
				detail::tvec_swizzle<T, 2, 2, 0, 2> ppsp;
				detail::tvec_swizzle<T, 2, 2, 1, 0> ppts;
				detail::tvec_swizzle<T, 2, 2, 1, 1> pptt;
				detail::tvec_swizzle<T, 2, 2, 1, 2> pptp;
				detail::tvec_swizzle<T, 2, 2, 2, 0> ppps;
				detail::tvec_swizzle<T, 2, 2, 2, 1> pppt;
				detail::tvec_swizzle<T, 2, 2, 2, 2> pppp;
				detail::tvec_swizzle<T, 0, 0> uu;
				detail::tvec_swizzle<T, 0, 1> uv;
				detail::tvec_swizzle<T, 0, 2> ui;
				detail::tvec_swizzle<T, 1, 0> vu;
				detail::tvec_swizzle<T, 1, 1> vv;
				detail::tvec_swizzle<T, 1, 2> vi;
				detail::tvec_swizzle<T, 2, 0> iu;
				detail::tvec_swizzle<T, 2, 1> iv;
				detail::tvec_swizzle<T, 2, 2> ii;
				detail::tvec_swizzle<T, 0, 0, 0> uuu;
				detail::tvec_swizzle<T, 0, 0, 1> uuv;
				detail::tvec_swizzle<T, 0, 0, 2> uui;
				detail::tvec_swizzle<T, 0, 1, 0> uvu;
				detail::tvec_swizzle<T, 0, 1, 1> uvv;
				detail::tvec_swizzle<T, 0, 1, 2> uvi;
				detail::tvec_swizzle<T, 0, 2, 0> uiu;
				detail::tvec_swizzle<T, 0, 2, 1> uiv;
				detail::tvec_swizzle<T, 0, 2, 2> uii;
				detail::tvec_swizzle<T, 1, 0, 0> vuu;
				detail::tvec_swizzle<T, 1, 0, 1> vuv;
				detail::tvec_swizzle<T, 1, 0, 2> vui;
				detail::tvec_swizzle<T, 1, 1, 0> vvu;
				detail::tvec_swizzle<T, 1, 1, 1> vvv;
				detail::tvec_swizzle<T, 1, 1, 2> vvi;
				detail::tvec_swizzle<T, 1, 2, 0> viu;
				detail::tvec_swizzle<T, 1, 2, 1> viv;
				detail::tvec_swizzle<T, 1, 2, 2> vii;
				detail::tvec_swizzle<T, 2, 0, 0> iuu;
				detail::tvec_swizzle<T, 2, 0, 1> iuv;
				detail::tvec_swizzle<T, 2, 0, 2> iui;
				detail::tvec_swizzle<T, 2, 1, 0> ivu;
				detail::tvec_swizzle<T, 2, 1, 1> ivv;
				detail::tvec_swizzle<T, 2, 1, 2> ivi;
				detail::tvec_swizzle<T, 2, 2, 0> iiu;
				detail::tvec_swizzle<T, 2, 2, 1> iiv;
				detail::tvec_swizzle<T, 2, 2, 2> iii;
				detail::tvec_swizzle<T, 0, 0, 0, 0> uuuu;
				detail::tvec_swizzle<T, 0, 0, 0, 1> uuuv;
				detail::tvec_swizzle<T, 0, 0, 0, 2> uuui;
				detail::tvec_swizzle<T, 0, 0, 1, 0> uuvu;
				detail::tvec_swizzle<T, 0, 0, 1, 1> uuvv;
				detail::tvec_swizzle<T, 0, 0, 1, 2> uuvi;
				detail::tvec_swizzle<T, 0, 0, 2, 0> uuiu;
				detail::tvec_swizzle<T, 0, 0, 2, 1> uuiv;
				detail::tvec_swizzle<T, 0, 0, 2, 2> uuii;
				detail::tvec_swizzle<T, 0, 1, 0, 0> uvuu;
				detail::tvec_swizzle<T, 0, 1, 0, 1> uvuv;
				detail::tvec_swizzle<T, 0, 1, 0, 2> uvui;
				detail::tvec_swizzle<T, 0, 1, 1, 0> uvvu;
				detail::tvec_swizzle<T, 0, 1, 1, 1> uvvv;
				detail::tvec_swizzle<T, 0, 1, 1, 2> uvvi;
				detail::tvec_swizzle<T, 0, 1, 2, 0> uviu;
				detail::tvec_swizzle<T, 0, 1, 2, 1> uviv;
				detail::tvec_swizzle<T, 0, 1, 2, 2> uvii;
				detail::tvec_swizzle<T, 0, 2, 0, 0> uiuu;
				detail::tvec_swizzle<T, 0, 2, 0, 1> uiuv;
				detail::tvec_swizzle<T, 0, 2, 0, 2> uiui;
				detail::tvec_swizzle<T, 0, 2, 1, 0> uivu;
				detail::tvec_swizzle<T, 0, 2, 1, 1> uivv;
				detail::tvec_swizzle<T, 0, 2, 1, 2> uivi;
				detail::tvec_swizzle<T, 0, 2, 2, 0> uiiu;
				detail::tvec_swizzle<T, 0, 2, 2, 1> uiiv;
				detail::tvec_swizzle<T, 0, 2, 2, 2> uiii;
				detail::tvec_swizzle<T, 1, 0, 0, 0> vuuu;
				detail::tvec_swizzle<T, 1, 0, 0, 1> vuuv;
				detail::tvec_swizzle<T, 1, 0, 0, 2> vuui;
				detail::tvec_swizzle<T, 1, 0, 1, 0> vuvu;
				detail::tvec_swizzle<T, 1, 0, 1, 1> vuvv;
				detail::tvec_swizzle<T, 1, 0, 1, 2> vuvi;
				detail::tvec_swizzle<T, 1, 0, 2, 0> vuiu;
				detail::tvec_swizzle<T, 1, 0, 2, 1> vuiv;
				detail::tvec_swizzle<T, 1, 0, 2, 2> vuii;
				detail::tvec_swizzle<T, 1, 1, 0, 0> vvuu;
				detail::tvec_swizzle<T, 1, 1, 0, 1> vvuv;
				detail::tvec_swizzle<T, 1, 1, 0, 2> vvui;
				detail::tvec_swizzle<T, 1, 1, 1, 0> vvvu;
				detail::tvec_swizzle<T, 1, 1, 1, 1> vvvv;
				detail::tvec_swizzle<T, 1, 1, 1, 2> vvvi;
				detail::tvec_swizzle<T, 1, 1, 2, 0> vviu;
				detail::tvec_swizzle<T, 1, 1, 2, 1> vviv;
				detail::tvec_swizzle<T, 1, 1, 2, 2> vvii;
				detail::tvec_swizzle<T, 1, 2, 0, 0> viuu;
				detail::tvec_swizzle<T, 1, 2, 0, 1> viuv;
				detail::tvec_swizzle<T, 1, 2, 0, 2> viui;
				detail::tvec_swizzle<T, 1, 2, 1, 0> vivu;
				detail::tvec_swizzle<T, 1, 2, 1, 1> vivv;
				detail::tvec_swizzle<T, 1, 2, 1, 2> vivi;
				detail::tvec_swizzle<T, 1, 2, 2, 0> viiu;
				detail::tvec_swizzle<T, 1, 2, 2, 1> viiv;
				detail::tvec_swizzle<T, 1, 2, 2, 2> viii;
				detail::tvec_swizzle<T, 2, 0, 0, 0> iuuu;
				detail::tvec_swizzle<T, 2, 0, 0, 1> iuuv;
				detail::tvec_swizzle<T, 2, 0, 0, 2> iuui;
				detail::tvec_swizzle<T, 2, 0, 1, 0> iuvu;
				detail::tvec_swizzle<T, 2, 0, 1, 1> iuvv;
				detail::tvec_swizzle<T, 2, 0, 1, 2> iuvi;
				detail::tvec_swizzle<T, 2, 0, 2, 0> iuiu;
				detail::tvec_swizzle<T, 2, 0, 2, 1> iuiv;
				detail::tvec_swizzle<T, 2, 0, 2, 2> iuii;
				detail::tvec_swizzle<T, 2, 1, 0, 0> ivuu;
				detail::tvec_swizzle<T, 2, 1, 0, 1> ivuv;
				detail::tvec_swizzle<T, 2, 1, 0, 2> ivui;
				detail::tvec_swizzle<T, 2, 1, 1, 0> ivvu;
				detail::tvec_swizzle<T, 2, 1, 1, 1> ivvv;
				detail::tvec_swizzle<T, 2, 1, 1, 2> ivvi;
				detail::tvec_swizzle<T, 2, 1, 2, 0> iviu;
				detail::tvec_swizzle<T, 2, 1, 2, 1> iviv;
				detail::tvec_swizzle<T, 2, 1, 2, 2> ivii;
				detail::tvec_swizzle<T, 2, 2, 0, 0> iiuu;
				detail::tvec_swizzle<T, 2, 2, 0, 1> iiuv;
				detail::tvec_swizzle<T, 2, 2, 0, 2> iiui;
				detail::tvec_swizzle<T, 2, 2, 1, 0> iivu;
				detail::tvec_swizzle<T, 2, 2, 1, 1> iivv;
				detail::tvec_swizzle<T, 2, 2, 1, 2> iivi;
				detail::tvec_swizzle<T, 2, 2, 2, 0> iiiu;
				detail::tvec_swizzle<T, 2, 2, 2, 1> iiiv;
				detail::tvec_swizzle<T, 2, 2, 2, 2> iiii;
			#endif
			};
			const T& operator [] (std::size_t i)const {
			#if _NEWTON_MATH_RANGE_CHECK			
				if (i >= size) throw std::out_of_range ("Vector subscript out of range.");
			#endif
				return reinterpret_cast<const T (&) [size]> (*this) [i];
			}
			 T& operator [] (std::size_t i) {
			#if _NEWTON_MATH_RANGE_CHECK			
				if (i >= size) throw std::out_of_range ("Vector subscript out of range.");
			#endif
				return reinterpret_cast< T (&) [size]> (*this) [i];
			}
			
			constexpr tvec (T v0, T v1, T v2) : 
				x (v0), y (v1), z (v2)				 
			{}
			
			constexpr tvec (T in) : 
				tvec (in, in, in) 
			{}
			
			constexpr tvec ()  : tvec (T ()) {}
			
			//template <typename U, std::size_t... I>
			//tvec (const detail::tvec_swizzle<U, I...>& a) : tvec (a ()) {}
			
			template <typename I> 
			explicit tvec (const tvec<2, I>& in) : 
				tvec (in.x, in.y, T ()) 
			{}
			
			template <typename I, std::enable_if_t<!std::is_same<T, I>::value, int> = 0> 
			explicit tvec (const tvec<3, I>& in) : 
				tvec (in.x, in.y, in.z) 
			{}
			
			template <typename I> 
			explicit tvec (const tvec<4, I>& in) : 
				tvec (in.x, in.y, in.z) 
			{}
			
			explicit tvec (T v0, const tvec<2, T>& v1) : tvec (v0, v1.x, v1.y) {}
			explicit tvec (const tvec<2, T>& v0, T v1) : tvec (v0.x, v0.y, v1) {}
			tvec (const tvec<3, T>& v0) : tvec (v0.x, v0.y, v0.z) {}
			tvec<3, T>& operator = (const tvec<3, T>& in) {
				(*this).x = in.x;
				(*this).y = in.y;
				(*this).z = in.z;
				return *this;
			};
		};
////////////////////////////////
// I/O Operators
////////////////////////////////
		template <typename T>
		std::istream& operator >> (std::istream& iss, tvec<3, T>& in) {			
			return iss >> in.x >> in.y >> in.z;
		}		
		template <typename T>
		std::ostream& operator << (std::ostream& oss, const tvec<3, T>& in) {
			return oss << "{ " << "x: " << in.x << ", "<< "y: " << in.y << ", "<< "z: " << in.z << ", " << " }";
		}

////////////////////////////////
// Binary operations
////////////////////////////////
		template <typename A, typename B>
		inline auto operator + (const tvec<3, A>& a, const tvec<3, B>& b) {
			return tvec_make (a.x + b.x, a.y + b.y, a.z + b.z);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator + (const tvec<3, A>& a, const B& b) {
			return a + tvec<3, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator + (const A& a, const tvec<3, B>& b) {
			return tvec<3, A> (a) + b;
		}
		
		template <typename A, typename B>
		inline auto operator - (const tvec<3, A>& a, const tvec<3, B>& b) {
			return tvec_make (a.x - b.x, a.y - b.y, a.z - b.z);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator - (const tvec<3, A>& a, const B& b) {
			return a - tvec<3, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator - (const A& a, const tvec<3, B>& b) {
			return tvec<3, A> (a) - b;
		}
		
		template <typename A, typename B>
		inline auto operator * (const tvec<3, A>& a, const tvec<3, B>& b) {
			return tvec_make (a.x * b.x, a.y * b.y, a.z * b.z);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator * (const tvec<3, A>& a, const B& b) {
			return a * tvec<3, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator * (const A& a, const tvec<3, B>& b) {
			return tvec<3, A> (a) * b;
		}
		
		template <typename A, typename B>
		inline auto operator / (const tvec<3, A>& a, const tvec<3, B>& b) {
			return tvec_make (a.x / b.x, a.y / b.y, a.z / b.z);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator / (const tvec<3, A>& a, const B& b) {
			return a / tvec<3, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator / (const A& a, const tvec<3, B>& b) {
			return tvec<3, A> (a) / b;
		}
		
		template <typename A, typename B>
		inline auto operator % (const tvec<3, A>& a, const tvec<3, B>& b) {
			return tvec_make (modulus (a.x, b.x), modulus (a.y, b.y), modulus (a.z, b.z));			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator % (const tvec<3, A>& a, const B& b) {
			return a % tvec<3, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator % (const A& a, const tvec<3, B>& b) {
			return tvec<3, A> (a) % b;
		}
		
		template <typename A, typename B>
		inline auto operator & (const tvec<3, A>& a, const tvec<3, B>& b) {
			return tvec_make (a.x & b.x, a.y & b.y, a.z & b.z);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator & (const tvec<3, A>& a, const B& b) {
			return a & tvec<3, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator & (const A& a, const tvec<3, B>& b) {
			return tvec<3, A> (a) & b;
		}
		
		template <typename A, typename B>
		inline auto operator | (const tvec<3, A>& a, const tvec<3, B>& b) {
			return tvec_make (a.x | b.x, a.y | b.y, a.z | b.z);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator | (const tvec<3, A>& a, const B& b) {
			return a | tvec<3, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator | (const A& a, const tvec<3, B>& b) {
			return tvec<3, A> (a) | b;
		}
		
		template <typename A, typename B>
		inline auto operator ^ (const tvec<3, A>& a, const tvec<3, B>& b) {
			return tvec_make (a.x ^ b.x, a.y ^ b.y, a.z ^ b.z);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator ^ (const tvec<3, A>& a, const B& b) {
			return a ^ tvec<3, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator ^ (const A& a, const tvec<3, B>& b) {
			return tvec<3, A> (a) ^ b;
		}
		
		template <typename A, typename B>
		inline auto operator && (const tvec<3, A>& a, const tvec<3, B>& b) {
			return tvec_make (a.x && b.x, a.y && b.y, a.z && b.z);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator && (const tvec<3, A>& a, const B& b) {
			return a && tvec<3, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator && (const A& a, const tvec<3, B>& b) {
			return tvec<3, A> (a) && b;
		}
		
		template <typename A, typename B>
		inline auto operator || (const tvec<3, A>& a, const tvec<3, B>& b) {
			return tvec_make (a.x || b.x, a.y || b.y, a.z || b.z);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator || (const tvec<3, A>& a, const B& b) {
			return a || tvec<3, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator || (const A& a, const tvec<3, B>& b) {
			return tvec<3, A> (a) || b;
		}
		
////////////////////////////////
// Boolean operations
////////////////////////////////
		template <typename A, typename B>
		inline auto operator == (const tvec<3, A>& a, const tvec<3, B>& b) {
			return (a.x == b.x) && (a.y == b.y) && (a.z == b.z);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator == (const tvec<3, A>& a, const B& b) {
			return a == tvec<3, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator == (const A& a, const tvec<3, B>& b) {
			return tvec<3, A> (a) == b;
		}		
		
		template <typename A, typename B>
		inline auto operator != (const tvec<3, A>& a, const tvec<3, B>& b) {
			return (a.x != b.x) || (a.y != b.y) || (a.z != b.z);			
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<B>::value, int> = 0>
		inline auto operator != (const tvec<3, A>& a, const B& b) {
			return a != tvec<3, B> (b);
		}
		
		template <typename A, typename B, std::enable_if_t<std::is_scalar<A>::value, int> = 0>
		inline auto operator != (const A& a, const tvec<3, B>& b) {
			return tvec<3, A> (a) != b;
		}		
		
		


////////////////////////////////
// Unary operations
////////////////////////////////
		template <typename T>
		inline auto operator + (const tvec<3, T>& a) {
			return tvec_make (+a.x, +a.y, +a.z);			
		}
		
		template <typename T>
		inline auto operator - (const tvec<3, T>& a) {
			return tvec_make (-a.x, -a.y, -a.z);			
		}
		
		template <typename T>
		inline auto operator ~ (const tvec<3, T>& a) {
			return tvec_make (~a.x, ~a.y, ~a.z);			
		}
		
		template <typename T>
		inline auto operator ! (const tvec<3, T>& a) {
			return tvec_make (!a.x, !a.y, !a.z);			
		}
		
////////////////////////////////
// Misc operations
////////////////////////////////
		template <typename A, typename B>
		inline auto dot (const tvec<3, A>& a, const tvec<3, B>& b)  {			
			return a.x*b.x + a.y*b.y + a.z*b.z;
		}
		
		template <typename T>
		inline auto hypot2 (const tvec<3, T>& a)  {
			return dot (a, a);
		}
		
		template <typename T>
		inline auto hypot (const tvec<3, T>& a)  {
			return std::sqrt (hypot2 (a));
		}

		template <typename T>
		inline auto length2 (const tvec<3, T>& a)  {
			return hypot2 (a);
		}
		
		template <typename T>
		inline auto length (const tvec<3, T>& a)  {
			return hypot (a);
		}
		
		template <typename T>
		inline auto normalize (const tvec<3, T>& a)  {
			return a / hypot (a);
		}	
		
		template <typename T>
		inline auto distance (const tvec<3, T>& a, const tvec<3, T>& b)  {
			return hypot (b - a);
		}

		template <typename T>
		inline auto distance2 (const tvec<3, T>& a, const tvec<3, T>& b)  {
			return hypot2 (b - a);
		}
		
		template <typename T, std::size_t N>
		inline auto cross (const tvec<N, T>& a, const tvec<N, T>& b)  {			
			static_assert (N == 3, "Crossproduct only defined for 3 dimentional vectors.");
			return a.yzx * b.zxy - a.zxy * b.yzx;
		}		

		template <typename T>
		auto abs (const tvec<3, T>& x)  {
			return tvec_make (std::abs (x.x), std::abs (x.y), std::abs (x.z));
		}		
		
		template <typename T>
		auto min (const tvec<3, T>& x, const tvec<3, T>& y)  {
			return tvec_make (std::min (x.x, y.x), std::min (x.y, y.y), std::min (x.z, y.z));
		}		
		
		template <typename T>
		auto max (const tvec<3, T>& x, const tvec<3, T>& y)  {
			return tvec_make (std::max (x.x, y.x), std::max (x.y, y.y), std::max (x.z, y.z));
		}		
		
		template <typename T>
		auto sin (const tvec<3, T>& x)  {
			return tvec_make (std::sin (x.x), std::sin (x.y), std::sin (x.z));
		}		
		
		template <typename T>
		auto cos (const tvec<3, T>& x)  {
			return tvec_make (std::cos (x.x), std::cos (x.y), std::cos (x.z));
		}		
		
		template <typename T>
		auto tan (const tvec<3, T>& x)  {
			return tvec_make (std::tan (x.x), std::tan (x.y), std::tan (x.z));
		}		
		
		template <typename T>
		auto sinh (const tvec<3, T>& x)  {
			return tvec_make (std::sinh (x.x), std::sinh (x.y), std::sinh (x.z));
		}		
		
		template <typename T>
		auto cosh (const tvec<3, T>& x)  {
			return tvec_make (std::cosh (x.x), std::cosh (x.y), std::cosh (x.z));
		}		
		
		template <typename T>
		auto tanh (const tvec<3, T>& x)  {
			return tvec_make (std::tanh (x.x), std::tanh (x.y), std::tanh (x.z));
		}		
		
		template <typename T>
		auto asin (const tvec<3, T>& x)  {
			return tvec_make (std::asin (x.x), std::asin (x.y), std::asin (x.z));
		}		
		
		template <typename T>
		auto acos (const tvec<3, T>& x)  {
			return tvec_make (std::acos (x.x), std::acos (x.y), std::acos (x.z));
		}		
		
		template <typename T>
		auto atan (const tvec<3, T>& x)  {
			return tvec_make (std::atan (x.x), std::atan (x.y), std::atan (x.z));
		}		
		
		template <typename T>
		auto atan2 (const tvec<3, T>& x, const tvec<3, T>& y)  {
			return tvec_make (std::atan2 (x.x, y.x), std::atan2 (x.y, y.y), std::atan2 (x.z, y.z));
		}		
		
		template <typename T>
		auto log (const tvec<3, T>& x)  {
			return tvec_make (std::log (x.x), std::log (x.y), std::log (x.z));
		}		
		
		template <typename T>
		auto log2 (const tvec<3, T>& x)  {
			return tvec_make (std::log2 (x.x), std::log2 (x.y), std::log2 (x.z));
		}		
		
		template <typename T>
		auto log10 (const tvec<3, T>& x)  {
			return tvec_make (std::log10 (x.x), std::log10 (x.y), std::log10 (x.z));
		}		
		
		template <typename T>
		auto log1p (const tvec<3, T>& x)  {
			return tvec_make (std::log1p (x.x), std::log1p (x.y), std::log1p (x.z));
		}		
		
		template <typename T>
		auto exp (const tvec<3, T>& x)  {
			return tvec_make (std::exp (x.x), std::exp (x.y), std::exp (x.z));
		}		
		
		template <typename T>
		auto exp2 (const tvec<3, T>& x)  {
			return tvec_make (std::exp2 (x.x), std::exp2 (x.y), std::exp2 (x.z));
		}		
		
		template <typename T>
		auto exp1m (const tvec<3, T>& x)  {
			return tvec_make (std::exp1m (x.x), std::exp1m (x.y), std::exp1m (x.z));
		}		
		
		template <typename T>
		auto pow (const tvec<3, T>& x, const tvec<3, T>& n)  {
			return tvec_make (std::pow (x.x, n.x), std::pow (x.y, n.y), std::pow (x.z, n.z));
		}		
		
		template <typename T>
		auto sqrt (const tvec<3, T>& x)  {
			return tvec_make (std::sqrt (x.x), std::sqrt (x.y), std::sqrt (x.z));
		}		
		
		template <typename T>
		auto cbrt (const tvec<3, T>& x)  {
			return tvec_make (std::cbrt (x.x), std::cbrt (x.y), std::cbrt (x.z));
		}		
		
		template <typename T>
		auto floor (const tvec<3, T>& x)  {
			return tvec_make (std::floor (x.x), std::floor (x.y), std::floor (x.z));
		}		
		
		template <typename T>
		auto ceil (const tvec<3, T>& x)  {
			return tvec_make (std::ceil (x.x), std::ceil (x.y), std::ceil (x.z));
		}		
		
		template <typename T>
		auto round (const tvec<3, T>& x)  {
			return tvec_make (std::round (x.x), std::round (x.y), std::round (x.z));
		}		
		
		template <typename T>
		auto trunc (const tvec<3, T>& x)  {
			return tvec_make (std::trunc (x.x), std::trunc (x.y), std::trunc (x.z));
		}		
		
		template <typename T>
		auto copysign (const tvec<3, T>& x, const tvec<3, T>& y)  {
			return tvec_make (std::copysign (x.x, y.x), std::copysign (x.y, y.y), std::copysign (x.z, y.z));
		}		
		
		
	}
}
#pragma pack(pop)
#endif // __NEWTON_MATH_DETAIL_TVEC3_HPP__