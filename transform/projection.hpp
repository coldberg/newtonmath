#ifndef __NEWTON_MATH_TRANSFORM_PROJECTION_HPP__
#define __NEWTON_MATH_TRANSFORM_PROJECTION_HPP__

#include "../vector.hpp"
#include "../matrix.hpp"
#include "units.hpp"

namespace Newton {
    namespace Math {
		namespace row_major {
			#include "projection.inl"			
		}
		namespace col_major {
			#include "projection.inl"
		}
    }
}

#endif // !__NEWTON_MATH_TRANSFORMS_PROJECTION_HPP__
