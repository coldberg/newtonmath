#ifndef __NEWTON_MATH_TRANSFORM_UINITS_HPP__
#define __NEWTON_MATH_TRANSFORM_UINITS_HPP__

#include "../vector.hpp"
#include "../matrix.hpp"

namespace Newton {
    namespace Math {
        template <typename T = double>
        inline auto pi () noexcept {
            return T (3.1415926535897932384626433832795028841971693993751058209749445923078164062);
        }

        template <typename T>
        inline auto radians_from_degrees (const T& degrees) {
            static const auto d2r = pi<T> () / T (180.0);
            return d2r * degrees;
        }

        template <typename T>
        inline auto degrees_from_radians (const T& radians) {
            static const auto r2d = T (180.0) / pi<T> ();
            return r2d * radians;
        }
    }
}

#endif // !__NEWTON_MATH_TRANSFORM_UNITS_HPP__