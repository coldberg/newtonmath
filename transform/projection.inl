template <typename value_type, typename mat_type = tmat4<value_type>>
static auto perspective (value_type fov, value_type ratio, value_type near, value_type far) {
    /*
    mat_type out;
    auto thfy = std::tan (radians_from_degrees (fov) / value_type (2));
    auto Q = far / (far - near);
    out (0, 0) = value_type (1) / (thfy * ratio);
    out (1, 1) = value_type (1) / (thfy);
    out (2, 2) = Q;
    out (3, 2) = -Q*near;
    out (2, 3) = value_type (1);        
    */

    /*
    assert(abs(aspect - std::numeric_limits<T>::epsilon()) > static_cast<T>(0));

    T const tanHalfFovy = tan(fovy / static_cast<T>(2));

    tmat4x4<T, defaultp> Result(static_cast<T>(0));
    Result[0][0] = static_cast<T>(1) / (aspect * tanHalfFovy);
    Result[1][1] = static_cast<T>(1) / (tanHalfFovy);
    Result[2][2] = - (zFar + zNear) / (zFar - zNear);
    Result[2][3] = - static_cast<T>(1);
    Result[3][2] = - (static_cast<T>(2) * zFar * zNear) / (zFar - zNear);
    return Result;
    */

    mat_type result (0);
    value_type thfov = std::tan (fov / value_type (2));
    result (0, 0) = +value_type (1) / (ratio * thfov);
    result (1, 1) = +value_type (1) / (thfov);
    result (2, 2) = -(far + near)/(far - near);
    result (2, 3) = -value_type (1);
    result (3, 2) = -value_type (2) * far * near / (far - near);
    return result;
};