#ifndef __NEWTON_MATH_TRANSFORM_VIEW_HPP__
#define __NEWTON_MATH_TRANSFORM_VIEW_HPP__

#include "../vector.hpp"
#include "../matrix.hpp"

namespace Newton {
    namespace Math {
		namespace col_major {
			#include "view.inl"
		}
		namespace row_major {
			#include "view.inl"
		}
    }
}

#endif // !__NEWTON_MATH_TRANSFORM_UNITS_HPP__