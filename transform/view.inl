template <typename value_type, typename mat_type = tmat4<value_type>>
auto lookAt (const tvec3<value_type>& eye, 
             const tvec3<value_type>& center, 
             const tvec3<value_type>& up) 
{
    mat_type result (1);

    auto f = normalize (center - eye);
    auto s = normalize (cross (f, up));
    auto u = cross (s, f);

    result (0, 0) = +s.x;
    result (1, 0) = +s.y;
    result (2, 0) = +s.z;
    result (0, 1) = +u.x;
    result (1, 1) = +u.y;
    result (2, 1) = +u.z;
    result (0, 2) = -f.x;
    result (1, 2) = -f.y;
    result (2, 2) = -f.z;
    result (3, 0) = -dot (s, eye);
    result (3, 1) = -dot (u, eye);
    result (3, 2) = +dot (f, eye);
    return result;
}
